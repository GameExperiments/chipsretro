# README #

code is free to use!
app art related to Rommy produced by Tessa Porteous
art related to Tile World - Brian Raiter
sounds from Tile world too.
The level reader code in Java - a slightly modified version of the code written by written by Patrik Weibull.

Levels come directly from the community. I suggest only including CC1 as the whole LevelSet is known to work.
Missing is music playing, credit file reading and known bad level marking.
Also missing is a level creator and the ability to share.

Code is written in Java using LibGdx.
Know to run on Android and Windows. Should not be difficult to make a working version for all Libgdx Supported platforms,
including iOS

Font removed - see commits. Easily obtainable for commercial use from the Author for a small donation, but rights to share I did not get and that would have to be negotiated.
