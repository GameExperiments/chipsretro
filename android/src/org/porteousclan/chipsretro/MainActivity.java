package org.porteousclan.chipsretro;

import android.app.Activity;
import android.app.Application;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.ActivityCompat.OnRequestPermissionsResultCallback;
import android.support.v4.content.ContextCompat;
import android.util.DebugUtils;
import android.widget.Toast;

import java.io.Console;

public class MainActivity extends Activity implements OnRequestPermissionsResultCallback {
//temporary standby to test permissions
    private static final int MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL = 0x11;
    String permission = "android.permission.WRITE_EXTERNAL_STORAGE";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        askForPermission(permission,MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL);
    }

    public void onRequestPermissionsResult (int requestCode,String[] permissions,int[] grantResults) {
        //askForPermission(permission,REQUEST_CODE);
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL: {

                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    switchToGame();

                } else {
                    warnWriteNeeded();
                    switchToGame();
                }
                return;
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
//        System.exit(0);
    }

    private void askForPermission(String permission, Integer requestCode) {
        if (ContextCompat.checkSelfPermission(MainActivity.this, permission) != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(MainActivity.this, permission)) {
                //This is called if user has denied the permission before
                warnWriteNeeded();
                switchToGame();
            } else {
                warnWriteNeeded();
                ActivityCompat.requestPermissions(MainActivity.this, new String[]{permission}, requestCode);
                //wait for response??
            }
        } else {
            switchToGame();
        }
    }
    private void warnWriteNeeded() {
        Toast.makeText(this, "" + permission + " needs to be Allowed for import to work correctly ", Toast.LENGTH_LONG).show();
    }
    private void switchToGame() {
        Intent to_libGdx = new Intent(this.getApplicationContext(),AndroidLauncher.class);
        startActivity(to_libGdx);
        //wait a few seconds??
        finish(); //closes this activity//ummm
        //next code closes but toast does not show as the activity is gone
//        Intent intent = new Intent(this.getApplicationContext(),AndroidLauncher.class);//Intent.ACTION_MAIN);
//        intent.addCategory(Intent.CATEGORY_HOME);
//        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
//        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//        startActivity(intent);
//        System.exit(1);
    }
}
