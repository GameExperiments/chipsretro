package org.porteousclan.chipsretro;

import com.badlogic.gdx.Game;

import org.porteousclan.chipsretro.game.data.GameData;
import org.porteousclan.chipsretro.game.data.ImageCache;
import org.porteousclan.chipsretro.game.data.Preferences;
import org.porteousclan.chipsretro.screens.AbstractScreen;

public class DirectedGame extends Game {

	public GameData gameData;
    public AbstractScreen screen;


	public float screenWidth = 0;
	public float screenHeight = 0;
	public float screenHUDWidth = 0;
	public float screenHUDHeight = 0;
	public static final int GAME_STATE_PLAY = 0;
	public static final int GAME_STATE_PAUSE = 1;
	public static final int GAME_STATE_ANIMATE = 2;
	public static final int GAME_STATE_NEWLEVEL = 3;

    public DirectedGame() {
	}

	@Override
	public void create() {
        gameData = new GameData(this);
		Preferences.loadSettings(gameData);
        ImageCache.load (this);
//		default-font: { file: default.fnt },
//		ImageCache.uiSkin.add("default-font",gameData.font2);
		ImageCache.uiSkin.add("font1",gameData.font1);
		ImageCache.uiSkin.add("font2",gameData.font2);
		ImageCache.uiSkin.add("font3",gameData.font3);
		ImageCache.uiSkin.add("font4",gameData.font4);

	}
	
	@Override
	public void pause() {
		gameData.gameMode = DirectedGame.GAME_STATE_PAUSE;
        if (gameData._player != null)
            gameData._player.hadFirstMove = false;
        super.pause(); //tells screen to pause
	}


	@Override
	public void dispose() {
        super.dispose(); //tells screen to hide
//        ImageCache.uiSkin.dispose(); //pixmap complains its already disposed
        ImageCache.skinAtlas.dispose();
        ImageCache.atlas.dispose();

	}

}
