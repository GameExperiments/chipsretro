package org.porteousclan.chipsretro.util;

import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.CheckBox;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.utils.Scaling;

import org.porteousclan.chipsretro.game.data.ImageCache;

/**
 * Created by Richard on 2017-10-09.
 */

public class GuiHelper {

    public static CheckBox createCheckBox(String label, float fontscale, float size, boolean checked) {
        CheckBox checkbox = new CheckBox(label, ImageCache.uiSkin);
        //checkbox.setSkin(ImageCache.uiSkin);
        checkbox.getLabel().setFontScale(fontscale);
        checkbox.getImageCell().width(size).height(size);
        checkbox.getImage().setScaling(Scaling.stretch);
        checkbox.setChecked( checked );
        return checkbox;
    }
    public static TextButton createTextButton(String label) {
        TextButton textbutton = new TextButton(label, ImageCache.uiSkin);
        //textbutton.getLabel().setFontScale(fontscale);
        textbutton.defaults().fill();
        textbutton.pad(2,3,2,3);
        return textbutton;
    }
    public static boolean contains(Actor test, float x, float y) {
        Rectangle bb = new Rectangle(test.getX(), test.getY(), test.getWidth(), test.getHeight());
        return bb.contains(x,y);
    }

}
