package org.porteousclan.chipsretro.util;

//package com.siondream.freegemas;
//this is useless garbage
// change after
//  screen changes, etc.
// use Json or whatever format is used for language files.


import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.util.HashMap;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

public class LanguagesManager {
        private final static LanguagesManager _instance = new LanguagesManager();
        
        private static final String LANGUAGES_FILE = "data/languages/languages.xml";
        private static final String DEFAULT_LANGUAGE = "en";
        
        //private HashMap<String, HashMap<String, String>> _strings = null;
        private HashMap<String, String> _language = null;
        private String _languageName = null;
        
        private LanguagesManager() {
                // Create language map
                _language = new HashMap<String, String>();
                // Try to load specific system language
                _languageName = java.util.Locale.getDefault().toString();
                if (!loadLanguage(_languageName)) {
                        // Try to load generic system language
                        _languageName = java.util.Locale.getDefault().getLanguage();
                        //_languageName = "es";
                        if (!loadLanguage(_languageName)) {
                                // If it fails, fallback to default language
                                _languageName = DEFAULT_LANGUAGE;
                                loadLanguage(DEFAULT_LANGUAGE);
                        }
                }
        }
        
        public static LanguagesManager getInstance() {
                return _instance;
        }
        
        public static String getLanguage() {
                return getInstance()._languageName;
        }
        public static String getFontName() {
                return getInstance().getString("font");
        }

        public String getString(String key) {
                String string;
                
                if (_language != null) {
                        // Look for string in selected language
                        string = _language.get(key);
                        
                        if (string != null) {
                                return string;
                        }
                }
        
                // Key not found, return the key itself
                return key;
        }
        
        public String getString(String key, Object... args) {
                return String.format(getString(key), args);
        }
        
        public boolean loadLanguage(String languageName) {
                try {
                        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
                        DocumentBuilder db = dbf.newDocumentBuilder();
                        FileHandle fileHandle = Gdx.files.internal(LANGUAGES_FILE);
                        Document doc = db.parse(fileHandle.read());
                        
                        Element root = doc.getDocumentElement();
                        
                        NodeList languages = root.getElementsByTagName("language");
                        int numLanguages = languages.getLength();
                        
                        for (int i = 0; i < numLanguages; ++i) {
                                Node language = languages.item(i);
                                
                                if (language.getAttributes().getNamedItem("name").getTextContent().equals(languageName)) {
                                        _language.clear();
                                        Element languageElement = (Element)language;
                                        NodeList strings = languageElement.getElementsByTagName("string");
                                        int numStrings = strings.getLength();
                                        
                                        for (int j = 0; j < numStrings; ++j) {
                                                NamedNodeMap attributes = strings.item(j).getAttributes();
                                                String key = attributes.getNamedItem("key").getTextContent();
                                                String value = attributes.getNamedItem("value").getTextContent();
                                                System.out.println(value);
                                                value = value.replace("<br />", "\n");
                                                _language.put(key, value);
                                        }
                                        
                                        return true;
                                }
                        }
                }
                catch (Exception e) {
                        System.out.println("Error loading my languages file " + LANGUAGES_FILE);
                        return false;
                }
                
                return false;
        }

}