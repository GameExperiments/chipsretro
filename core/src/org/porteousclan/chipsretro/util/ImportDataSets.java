package org.porteousclan.chipsretro.util;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;

import org.porteousclan.chipsretro.game.data.GameData;
import org.porteousclan.chipsretro.game.gameLevels.InputStreamFileLevelReader;
import org.porteousclan.chipsretro.game.gameLevels.MicrosoftLevelFactory;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

/**
 * Created by richard on 23/08/16.
 */

public class ImportDataSets {
    //Some folder listing done here
    public static void searchForDownLoadedDataSets(GameData data) {
        Gdx.files.external(data.dataFolder+"games/").mkdirs();
        FileHandle dirHandle = Gdx.files.external("/");
        int i = 0;
        //don't forget to close handles if possible
        findFiles(data, dirHandle, i);
        dirHandle = null;
    }

    private static void findFiles(GameData data, FileHandle begin, int depth)
    {

        if (depth > 3) return;
        try {
            FileHandle[] newHandles = begin.list();
            for (FileHandle f : newHandles)
            {
                if (f.isDirectory() && f.name() != "porteousclan")
                {
                    if (f.name().equalsIgnoreCase("download") ||f.name().equalsIgnoreCase("downloads")) {
//		        		Gdx.app.log("Loop", "isFolder=" + f.name());
                        findFiles(data, f, depth + 1);
                    }
                }
                else
                {
                    FileHandle fho;
                    if (f.extension().equalsIgnoreCase("zip")) {
                        if (!Gdx.files.external(data.dataFolder+"games/" + f.nameWithoutExtension().toLowerCase() +".dat").exists()) {
                            fho = Gdx.files.external(data.dataFolder+"games/" +f.nameWithoutExtension().toLowerCase() +".dat");
                            unzip(f.file(),fho.file());
                            f = null;
                            f = fho;
                        }
                        fho = null;
                    }

                    @SuppressWarnings("unused")
                            /* we want to make sure the file is read correctly */
                    MicrosoftLevelFactory mslf = null;
                    if (f.extension().equalsIgnoreCase("dat") || f.extension().equalsIgnoreCase("ccl")) {
                        try {
                            mslf = new MicrosoftLevelFactory(InputStreamFileLevelReader.create(f)); //throws IOException
                            mslf = null;
                            if (!Gdx.files.external(data.dataFolder+"games/" + f.name().toLowerCase()).exists()) {
                                f.moveTo(Gdx.files.external(data.dataFolder+"games/" + f.nameWithoutExtension().toLowerCase() + ".dat"));
                            }
                        } catch (Exception e) { //file could not be read as CC
                            mslf = null;
                            try {
                                f.delete();
                            } catch (Exception ex) {}
                        }
                        f = null;
                    }
                }
            }
        } catch (Exception e) {
            Gdx.app.log("catch exception", e.getMessage());
        }
    }

    private static void unzip(File fi, File fo) {

        FileInputStream fis;
        //buffer for read and write data to file
        byte[] buffer = new byte[1024];
        try {
            fis = new FileInputStream(fi);
            ZipInputStream zis = new ZipInputStream(fis);

            ZipEntry ze = zis.getNextEntry();
            while(ze != null){
                String fileName = ze.getName();
                if (fileName.endsWith("dat") || fileName.endsWith("ccl")) {
                    FileOutputStream fos = new FileOutputStream(fo);
                    //            	OutputStreamWriter fos = new OutputStreamWriter(fo.write(false));
                    BufferedOutputStream out = new BufferedOutputStream(fos); //(new OutputStreamWriter(Gdx.files.external(foName).write(false)));
                    int len;
                    while ((len = zis.read(buffer)) > 0) {
                        out.write(buffer, 0, len);
                    }
                    out.flush();
                    out.close();
                    fos.close();
                    break;
                }
                //close this ZipEntry
                zis.closeEntry();
                ze = zis.getNextEntry();
            }
            //close last ZipEntry
            zis.closeEntry();
            zis.close();
            fis.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }


}
