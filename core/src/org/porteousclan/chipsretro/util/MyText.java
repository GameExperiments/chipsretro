package org.porteousclan.chipsretro.util;

/**
 * Created by richp on 2017-01-12.
 */


import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
//import com.badlogic.gdx.graphics.g2d.SpriteBatch;
//import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.scenes.scene2d.Actor;

public class MyText extends Actor {
    CharSequence str;
    BitmapFont font;

    public MyText(CharSequence str, BitmapFont font){
        this.font = font;
        this.str = str;
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        //font.draw(batch, "Score: 0" + myScore.getCurrent(), 0, 0);
        font.draw(batch,str,getX(),getY());
    }

    @Override
    public Actor hit(float x, float y, boolean touchable) {
        return null;
    }

}