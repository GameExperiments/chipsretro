package org.porteousclan.chipsretro.util;

/**
 * Created by Richard on 2017-10-10.
 */

public class AutoButtonSpacer {
    float numButtons;
    float buttonWidth;
    float buttonSpacer;
    public AutoButtonSpacer(float screenWidth, float numButtons) {
        this.numButtons = numButtons;
        buttonWidth = screenWidth/(numButtons+1);
        buttonSpacer = buttonWidth/(numButtons+1);
    }

    public float getwidth(){
        return buttonWidth;
    }

    public float getPos(float position) {

        return buttonWidth*(position-1) + (buttonSpacer)*position;

    }
}
