/*
 * Copyright (c) 30 Dec. 2015.
 * If you wish to use this code (except for menus)learning libgdx , please contact the author.
 * Rommy's v1.0 was a mashup of various code bits found freely on the internet mixed with my own underderstanding of the Game and the needs I had to make the code work. v1.5 and above is a rewrite based mostly of new things learnt from learning libgdx 2nd ed, libgdx cross-platform game dev cookbook, and game programming patterns which i used to help solidify my own patterns developed over the years. still ive visited the internet many times. Using the internet is like going to speak to the professor. So where i found the way I was misusing somethings I may have alreay thanked them in the forums. I dont think I need to thank them again.
 */


package org.porteousclan.chipsretro.util;

import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;

import org.porteousclan.chipsretro.game.elements.TileSprite;


public class CameraHelper {

	private static final String TAG = CameraHelper.class.getName();
//    private GameData _sharedData;
	private final float MAX_ZOOM_IN = 0.5f;
	private final float MAX_ZOOM_OUT = 3.0f;
	private float FOLLOW_SPEED = 3.0f;

	private Vector2 position;
	private float zoom;
	private TileSprite target;
    private static CameraHelper ourInstance = new CameraHelper(); //initializes itself on first use
    public static CameraHelper getInstance() {
        return ourInstance;
    }

	private CameraHelper () {
//        _sharedData = GameData.getInstance();
		position = new Vector2();
		zoom = 1.0f;
	}

	public void update (float deltaTime) {
		if (!hasTarget()) return;
        Vector2 player_position = new Vector2(target.getX(),target.getY());

        //if (_sharedData.gameMode != Constants.GAME_STATE.PLAY) {
		position = player_position;
//       	position.lerp(player_position, FOLLOW_SPEED * deltaTime);
//
//        if (position.dst(player_position) > 4.0f)
//            FOLLOW_SPEED = 5;
//        else
//            FOLLOW_SPEED = 3;

		//prevent going (too far) past edge of world
        position.x = Math.min(30f, position.x);
        position.x = Math.max(3f, position.x);
        position.y = Math.min(30f, position.y);
        position.y = Math.max(3f, position.y);

	}

    public void moveCamera (float x, float y) {
        x += getPosition().x;
        y += getPosition().y;
        setPosition(x, y);
    }

	public void setPosition (float x, float y) {
        this.position.set(x, y);
	}

	public Vector2 getPosition () {
		return position;
	}

	public void addZoom (float amount) {
		setZoom(zoom + amount);
	}

	public void setZoom (float zoom) {
		this.zoom = MathUtils.clamp(zoom, MAX_ZOOM_IN, MAX_ZOOM_OUT);
	}

	public float getZoom () {
		return zoom;
	}

	public void setTarget (TileSprite target) {
		this.target = target;
	}

	public TileSprite getTarget () {
		return target;
	}

	public boolean hasTarget () {
		return target != null;
	}

	public boolean hasTarget (TileSprite target) {
		return hasTarget() && this.target.equals(target);
	}

	public void applyTo (OrthographicCamera camera) {
        //need cameraMenu to be right and down from player
		camera.position.x = position.x+2.5f;
		camera.position.y = position.y+1;
		camera.zoom = zoom;
		camera.update();
	}

}
