package org.porteousclan.chipsretro.game.gui;

import com.badlogic.gdx.graphics.g2d.Sprite;
import org.porteousclan.chipsretro.game.data.ImageCache;

public class InventorySprite extends Sprite {
    protected int items = 0;
    public int type;
    public boolean visible;

    public InventorySprite(String skinName, float x, float y, int type) {
        super(ImageCache.getTexture(skinName));
        setScale(0.8f);
        setPosition(x,y);
        this.type = type;
    }

    public void show () { visible = true; }
    public void hide () { visible = false; }

    public void reset() {
        items = 0;
    }


    public void update(float dt) {
        if (items > 0) {
            show();
        } else {
            items = 0;
            hide();
        }
    }

    public boolean addInventory(int value) {
        items += value;
        if (items < 0) {
            items = 0;
            return false;
        }
        return true;
    }

    public boolean hasInventory() {
        if (items > 0) return true;
        return false;
    }

}
