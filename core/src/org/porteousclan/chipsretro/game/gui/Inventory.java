package org.porteousclan.chipsretro.game.gui;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector3;

import org.porteousclan.chipsretro.DirectedGame;
import org.porteousclan.chipsretro.game.data.Type;

import java.util.ArrayList;
import java.util.List;

public class Inventory  {


	protected List<InventorySprite> _elements;
	protected DirectedGame _game;
	public Vector3 position = new Vector3(0,0,0);
	public boolean active;
	public boolean visible;

	public Inventory(DirectedGame game, float x, float y) {
		_game = game;
		position.x = x;
		position.y = y;
		_elements = new ArrayList<InventorySprite>();
		createElements ();
	}
	
	protected void createElements () {
		InventorySprite sprite;
		float element_x, element_y;
		float spacing_x, spacing_y;

		element_x = this.position.x;
		element_y = this.position.y;

		
		sprite = new InventorySprite("fire_boots", element_x, element_y, Type.FIREBOOTS);
		_elements.add(sprite);
		spacing_y = sprite.getHeight()*0.8f;
		spacing_x = sprite.getWidth()*0.8f;

		sprite = new InventorySprite("suction_boots", element_x, element_y-spacing_y, Type.SUCTIONBOOTS);
		_elements.add(sprite);

		sprite = new InventorySprite("flippers", element_x , element_y-spacing_y*2, Type.FLIPPERS);
		_elements.add(sprite);

		sprite = new InventorySprite("ice_skates", element_x , element_y-spacing_y*3, Type.ICESKATES);
		_elements.add(sprite);
		
		sprite = new InventorySprite("blue_key", element_x+spacing_x, element_y, Type.BLUEKEY);
		_elements.add(sprite);
		
		sprite = new InventorySprite("green_key", element_x+spacing_x, element_y-spacing_y, Type.GREENKEY);
		_elements.add(sprite);
		
		sprite = new InventorySprite("red_key", element_x+spacing_x, element_y-spacing_y*2, Type.REDKEY);
		_elements.add(sprite);
		
		sprite = new InventorySprite("yellow_key", element_x+spacing_x, element_y-spacing_y*3, Type.YELLOWKEY);
		_elements.add(sprite);	
	}
	

	public void reset () {
		InventorySprite sprite;
		int len = _elements.size();
		for (int i=0; i<len; i++) {
			sprite = _elements.get(i);
			sprite.reset();
		}
	}
	public void resetboots () {
		InventorySprite sprite;
		int len = _elements.size();
		for (int i=0; i<len; i++) {
			sprite = _elements.get(i);
			if (isboots(sprite.type))
				sprite.reset();
		}
	}
	private boolean isboots (int type) {
		switch (type) {
		case Type.FLIPPERS:
		case Type.FIREBOOTS :
		case Type.SUCTIONBOOTS :
		case Type.ICESKATES :
			return true;
		default:
			return false;
		}
	}

	public void update (float dt) {
		InventorySprite sprite;
		int len = _elements.size();
		for (int i = 0; i < len; i++) {
			sprite = _elements.get(i);
			sprite.update(dt);
		}
	}
	

	public void draw(SpriteBatch spriteBatch) {

		InventorySprite sprite;
		int len = _elements.size();
			for (int i = 0; i < len; i++) {
				sprite = _elements.get(i);
				if (sprite.visible)
				sprite.draw(spriteBatch);
			}

	}
	
	/** Boots, Keys and thief */
	public boolean addInventory(int type) {
		InventorySprite sprite;
		sprite = null;

		switch (type) {
		case Type.FIREBOOTS :
			sprite = _elements.get(0);
			sprite.addInventory(1);
			return true;
		case Type.SUCTIONBOOTS :
			sprite = _elements.get(1);
			sprite.addInventory(1);
			return true;
		case Type.FLIPPERS :
			sprite = _elements.get(2);
			sprite.addInventory(1);
			return true;
		case Type.ICESKATES :
			sprite = _elements.get(3);
			sprite.addInventory(1);
			return true;
		case Type.BLUEKEY :
			sprite = _elements.get(4);
			sprite.addInventory(1);
			return true;
		case Type.GREENKEY :
			sprite = _elements.get(5);
			sprite.addInventory(1);
			return true;
		case Type.REDKEY :
			sprite = _elements.get(6);
			sprite.addInventory(1);
			return true;
		case Type.YELLOWKEY :
			sprite = _elements.get(7);
			sprite.addInventory(1);
			return true;
		case Type.THIEF :
			resetboots();
			return true;

		default:
			break;
		}
		
		return false;

	}

	/** locks and special floor tiles 
	 * returns true if inventory used
	 * false if not
	 * */
	public boolean useInventory(int type) {
		InventorySprite sprite;
		sprite = null;

		switch (type) {
		case Type.BLUELOCK :
			sprite = _elements.get(4);
//			Gdx.app.log("BlueLock", "Use Inventory");
			if (sprite.addInventory(-1)) {
				return true;
			}
			break;
		case Type.GREENLOCK :
			/** Green Key doesn't get used */
			sprite = _elements.get(5);
			if (sprite.hasInventory()) {
				return true;
			}
			break;
		case Type.REDLOCK :
			sprite = _elements.get(6);
			if (sprite.addInventory(-1)) {
				return true;
			}
			break;
		case Type.YELLOWLOCK :
			sprite = _elements.get(7);
			if (sprite.addInventory(-1)) {
				return true;
			}
			break;
		case Type.WATER :
			sprite = _elements.get(2);
			if (sprite.hasInventory()) {
				return true;
			}
			break;
		case Type.ICE :
		case Type.ICEWALL_NE :
		case Type.ICEWALL_NW :
		case Type.ICEWALL_SW :
		case Type.ICEWALL_SE :
			sprite = _elements.get(3);
			if (sprite.hasInventory()) {
				return true;
			}
			break;
		case Type.FIRE :
			sprite = _elements.get(0);
			if (sprite.hasInventory()) {
				return true;
			}
			break;
		case Type.FORCEFLOOR :
		case Type.RANDOMFORCEFLOOR :
			sprite = _elements.get(1);
			if (sprite.hasInventory()) {
				return true;
			}
			break;
		}
		
		return false;

	}
	public boolean hasInventory(int type) {
		InventorySprite sprite;
		sprite = null;

		switch (type) {
		case Type.BLUELOCK :
			sprite = _elements.get(4);
			if (sprite.hasInventory()) {
				return true;
			}
			break;
		case Type.GREENLOCK :
			/** Green Key doesn't get used */
			sprite = _elements.get(5);
			if (sprite.hasInventory()) {
				return true;
			}
			break;
		case Type.REDLOCK :
			sprite = _elements.get(6);
			if (sprite.hasInventory()) {
				return true;
			}
			break;
		case Type.YELLOWLOCK :
			sprite = _elements.get(7);
			if (sprite.hasInventory()) {
				return true;
			}
			break;
		case Type.WATER :
			sprite = _elements.get(2);
			if (sprite.hasInventory()) {
				return true;
			}
			break;
		case Type.ICE :
		case Type.ICEWALL_NE :
		case Type.ICEWALL_NW :
		case Type.ICEWALL_SW :
		case Type.ICEWALL_SE :
			sprite = _elements.get(3);
			if (sprite.hasInventory()) {
				return true;
			}
			break;
		case Type.FIRE :
			sprite = _elements.get(0);
			if (sprite.hasInventory()) {
				return true;
			}
			break;
		case Type.FORCEFLOOR :
		case Type.RANDOMFORCEFLOOR :
			sprite = _elements.get(1);
			if (sprite.hasInventory()) {
				return true;
			}
			break;
		}
		
		return false;

	}
}
