package org.porteousclan.chipsretro.game.gui;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector3;

import org.porteousclan.chipsretro.DirectedGame;
import org.porteousclan.chipsretro.game.data.ImageCache;

import java.util.ArrayList;
import java.util.List;

public class NumberSprite {
	
	public int value;
	public String prefix = null;
	private List<TextureRegion> _textures;
	private List<Sprite> _numbers;
	BitmapFont font;
    protected DirectedGame _game;
    public Vector3 position = new Vector3(0,0,0);
    public boolean active;
    public boolean visible;


	public NumberSprite(DirectedGame game, float x, float y, String nameRoot) {
		_game = game;
        position.x = x;
        position.y = y;
		//super(null,game, x, y);
		
//		skin = null;
		value = 0;
		_textures = new ArrayList<TextureRegion>();
		_numbers = new ArrayList<Sprite>();
		font = new BitmapFont(Gdx.files.internal(_game.gameData.skinsfolder + "chips_med.fnt"),Gdx.files.internal(_game.gameData.skinsfolder + "chips_med_0.png"), false);

		int i;
		for (i = 0; i < 12; i++) {
			_textures.add(ImageCache.getFrame(nameRoot, i));
//			Gdx.app.log("numbers", "image " + nameRoot + "_" + i);
//			_textures.add(ImageCache.getFrame(ImageCache.numberatlas, nameRoot, i));
		}
		for (i = 0; i < 12; i++) {
			_textures.add(ImageCache.getFrame(nameRoot + "y", i));
//			Gdx.app.log("numbers", "image " + nameRoot + "y_" + i);
//			_textures.add(ImageCache.getFrame(ImageCache.numberatlas, nameRoot, i));
		}
		
		Sprite sprite;
		for (i = 0; i < 10; i++) {
			//create Sprite with TextureRegion
			sprite = new Sprite(_textures.get(i));
			sprite.setPosition(x + i * (sprite.getRegionWidth() + 2), y);
			_numbers.add(sprite);
		}
	}
	
	public void reset () {
//		super.reset();
		value = 0;
	}
    public void show () { visible = true; }
    public void hide () { visible = false; }

    public void draw (SpriteBatch spriteBatch) {
		int _value;
		String string, stringN;
		if (value > 999) {
			string = "0000999";
		} else {
			string = "0000" + value;
		}
		if (value < 0) string = "0000---";
		//if (value < 0) value = 0;
	
		string = string.substring(string.length() - 3);
		//int len = string.length();
		//3 real digits max
		Sprite sprite;
		font.setColor(0.1f, 0.1f, 0.1f, 1f);
		
		if (prefix != null) {
			font.draw(spriteBatch, prefix, this.position.x - _game.screenHUDWidth*0.07f, this.position.y + _game.screenHUDHeight *0.06f);
		}

		if (value > 15 || value < 0) { //if (Integer.parseInt(string) > 15) {
			for (int i = 0; i < 3 ; i++) {
				sprite = _numbers.get(i);
				stringN = string.substring(i,i+1);
				if (stringN.equals("-")) 
					stringN = "10";
				_value = Integer.parseInt(stringN); 
	
				if (value > 999 && i==0) {
					sprite.setRegion(_textures.get(_value+12));		//first character is yellow			
				} else {
					sprite.setRegion(_textures.get(_value));
				}
				sprite.draw(spriteBatch);
			}
		} else {
			for (int i = 0; i < 3 ; i++) {
				sprite = _numbers.get(i); //+10?
				stringN = string.substring(i,i+1);
				if (stringN.equals("-")) 
					stringN = "10";
				_value = Integer.parseInt(stringN); 
	
				sprite.setRegion(_textures.get(_value+12));
				sprite.draw(spriteBatch);
			}
			
		}
	}
}
