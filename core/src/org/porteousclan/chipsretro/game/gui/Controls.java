package org.porteousclan.chipsretro.game.gui;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector3;

import org.porteousclan.chipsretro.DirectedGame;
import org.porteousclan.chipsretro.game.data.GameData;
import org.porteousclan.chipsretro.game.data.ImageCache;

public class Controls extends Sprite {

	//private Vector3 _center;
	protected DirectedGame _game;
//	public Vector3 position = new Vector3(0,0,0);
	public boolean active;
	public boolean visible;
    private Sprite slapDot;


	public Controls(DirectedGame game, float x, float y) {
		super(ImageCache.getTexture("control"));
		super.setPosition(x,y);
		_game = game;
		//_center = new Vector3 (x, y , 0f);
        slapDot = new Sprite(new Texture("data/skins/touchKnob.png")); //,(int)preferredHeight/4,(int)preferredHeight/4);


    }
//    public void setSlapOff() {
//        slapDot.setColor(Color.GREEN);
//        _game.gameData.slapOnly = false;
//    }

//    public void setSlapOn() {
//        slapDot.setColor(Color.YELLOW);
//        _game.gameData.slapOnly = true;
//    }

    public void resetSize (float width, float height) {
        super.setSize(width, height);
        slapDot.setSize(width/4,width/4);
        slapDot.setPosition(this.getX() + width/2 - slapDot.getWidth()/2, this.getY() + height/2 - slapDot.getHeight()/2);
        //setSlapOff();
        //_game.gameData.slapOnly = false;
    }

	public int getDirection (Vector3 p) {
		//setOriginCenter();
		//float x = getX();
		//float y = getY();
		//float width = getWidth();
		//float height = getHeight();
		double diffx = p.x - (getX() + getWidth()/2);
		double diffy = p.y - (getY() + getHeight()/2);

        //Centre
		if (Math.abs(diffx) < getHeight()/10 && Math.abs(diffy) < getWidth()/10) { //dead zone
            //setSlapOn();
            if (!_game.gameData.msStyleMoveConditions)
                _game.gameData.slapOnly = true;
            return GameData.NONE;
        }
        //slapDot.setColor(Color.GREEN);
		double rad = Math.atan2(diffy, diffx);
		
		int angle = (int) (180 * rad / Math.PI);
		if (angle < 360) angle += 360;
		if (angle > 360) angle -=  360;

		//USE 6 degree gaps
		
		if (angle > 318 || angle < 42) {
			//Gdx.app.log("CONTROL CLICK:", "RIGHT");
			return GameData.EAST;
		} else if (angle > 48 && angle < 132) {
			//Gdx.app.log("CONTROL CLICK:", "TOP");
			return GameData.NORTH;
		} else if (angle > 138 && angle < 222) {
			//Gdx.app.log("CONTROL CLICK:", "LEFT");
			return GameData.WEST;
		} else if (angle > 228 && angle < 312) {
			//Gdx.app.log("CONTROL CLICK:", "DOWN");
			return GameData.SOUTH;
		} else {
			//Gdx.app.log("CONTROL CLICK:", "MISSED");
			return GameData.NONE;
		}
	}

	public Boolean contains(Vector3 tp) {
		if (visible && bounds().contains(tp.x,tp.y))
			return true;
		else
			return false;
	}

	public Rectangle bounds () {
		float extraTouchArea = getWidth()/7;
		return new Rectangle(getX()-extraTouchArea, getY()-extraTouchArea, getWidth()+(extraTouchArea*2), getHeight()+(extraTouchArea*2));
	}

	@Override
    public void draw (Batch batch) {
        if (_game.gameData.slapOnly)
            slapDot.setColor(Color.YELLOW);
        else
            slapDot.setColor(Color.GREEN);

        super.draw(batch);

        if (!_game.gameData.msStyleMoveConditions)
            slapDot.draw(batch);
    }

}
