package org.porteousclan.chipsretro.game.gui;

import org.porteousclan.chipsretro.DirectedGame;
import org.porteousclan.chipsretro.game.data.GameData;
import org.porteousclan.chipsretro.game.data.Sounds;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;


public class TimeBar extends NumberSprite {
	public int seconds;
	private float _timer;
	public static int _timeleft;
	private float _timeDecrement;
	public static int _timeallocated = 0;

	public TimeBar(DirectedGame game, float x, float y, String nameRoot, int timeallocated) {
		super(game, x, y, nameRoot);
		seconds = 0;
			_timeallocated = timeallocated;
		_timeleft = _timeallocated;
		_timeDecrement = 0.001f; //1f; 
		
	}
	
	@Override 
	public void draw(SpriteBatch spriteBatch) {
		value = _timeleft;
		prefix =  GameData.getLanguageString("TimeBar");
		super.draw(spriteBatch);
	}
	

	@Override 
	public void reset () {
		_timeallocated = _game.gameData.timeleft;
		_timeleft = _timeallocated;
		show();
		seconds = 0;
		_timer = 0;
	}

	public void update (float dt) {
       	if (_game.gameData.gameMode == DirectedGame.GAME_STATE_PLAY && _game.gameData._player.hadFirstMove) {
			_timer += dt * 855; //1000		should be 1s -> 1.17s
			if (_timer >= 1000 && _timer < 10000) {
				_timer = 0;
				if (visible) {
					seconds++;
					if (_timeallocated < 1) {
						_timeleft = -1;
					} else {
						if (_timeleft - _timeDecrement <= 0) {
							_game.gameData.timeleft = _timeleft = 0;
							visible = false;
							//_game.gameData._player.kill();
							_game.gameData.gsEvent = GameData.GameScreenEventMessage.TIMEOUTMESSAGE;

						} else {
							_timeleft -= _timeDecrement;
							_game.gameData.timeleft = _timeleft;
							if (_timeleft <= 15) Sounds.play(Sounds.tick);
						}
					}
				}
			}
       	}
	}

}
