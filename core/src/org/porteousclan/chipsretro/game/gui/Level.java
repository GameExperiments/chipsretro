package org.porteousclan.chipsretro.game.gui;

import org.porteousclan.chipsretro.DirectedGame;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public class Level extends NumberSprite {
	public int  currentLevel = 0;

	public Level(DirectedGame game, float x, float y, String nameRoot) {
		super(game, x, y, nameRoot);
		currentLevel = _game.gameData.level;
	}
	
	@Override 
	public void draw (SpriteBatch spriteBatch) {
//		value = _game.gameData.level;
		value = currentLevel;
		prefix = _game.gameData.getLanguageString("LevelBar");
		super.draw(spriteBatch);
	}

}
