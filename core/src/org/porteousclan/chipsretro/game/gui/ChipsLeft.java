package org.porteousclan.chipsretro.game.gui;

import org.porteousclan.chipsretro.DirectedGame;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public class ChipsLeft extends NumberSprite {

	public ChipsLeft(DirectedGame game, float x, float y, String nameRoot) {
		super(game, x, y, nameRoot);

	}
	@Override
	public void reset () {
		super.reset();
		
	}
	@Override 
	public void draw (SpriteBatch spriteBatch) {
		value = _game.gameData.chips;
		prefix = _game.gameData.getLanguageString("ChipsLeftBar");
		super.draw(spriteBatch);
	}

}
