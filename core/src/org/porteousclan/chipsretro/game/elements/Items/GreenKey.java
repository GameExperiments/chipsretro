package org.porteousclan.chipsretro.game.elements.Items;

import org.porteousclan.chipsretro.DirectedGame;
import org.porteousclan.chipsretro.game.data.Type;
import org.porteousclan.chipsretro.game.elements.TileSprite;

public class GreenKey extends InventoryItem {

	public GreenKey(DirectedGame game, int x, int y, int _type, int layer, int direction) {
		super( game, x, y, _type, layer, direction);
	}
	@Override
	protected void initialize() {
		animNorth = "green_key";
		animSouth = "green_key";
		animEast = "green_key";
		animWest = "green_key";
		super.initialize();
	}
	@Override
	public boolean canMoveToMe(TileSprite sprite, int moveDirection) {

		if (sprite.type == Type.PLAYER || sprite.type == Type.BLOCK || sprite.type == Type.ICEBLOCK || sprite.type == Type.CLONEBLOCK
				|| (!_game.gameData.lynxStyleMoveConditions && sprite.isCreature())) {
			return true;
		}
		return false;
	}

}
