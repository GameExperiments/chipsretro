package org.porteousclan.chipsretro.game.elements.Items;

import org.porteousclan.chipsretro.DirectedGame;
import org.porteousclan.chipsretro.game.data.Type;
import org.porteousclan.chipsretro.game.elements.TileSprite;

public class Flippers extends InventoryItem {

	public Flippers(DirectedGame game, int x, int y, int _type, int layer, int direction) {
		super( game, x, y, _type, layer, direction);
	}
	@Override
	protected void initialize() {
		animNorth = "flippers";
		animSouth = "flippers";
		animEast = "flippers";
		animWest = "flippers";
		super.initialize();
	}

	@Override
	public boolean canMoveToMe(TileSprite sprite, int moveDirection) {
		if (sprite.type == Type.PLAYER) {
			return true;
		}
		if (!_game.gameData.lynxStyleMoveConditions && (sprite.type == Type.BLOCK || sprite.type == Type.ICEBLOCK || sprite.type == Type.CLONEBLOCK))
			return true;

		return false;
	}

	@Override
	public boolean stepToTiles(TileSprite sprite, boolean dontKill) {
		if (sprite.type == Type.PLAYER) {
			stoodon = sprite;
			return true;
		}
		return false;
	}

}