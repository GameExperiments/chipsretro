package org.porteousclan.chipsretro.game.elements.Items;

import org.porteousclan.chipsretro.DirectedGame;
import org.porteousclan.chipsretro.game.data.GroupType;
import org.porteousclan.chipsretro.game.data.Sounds;
import org.porteousclan.chipsretro.game.data.Type;
import org.porteousclan.chipsretro.game.elements.TileSprite;

public class InventoryItem extends TileSprite {

	public InventoryItem( DirectedGame game, int x, int y, int _type, int layer, int direction) {
		super(null, game, x, y, direction);
		_layer = layer;
		_originalType = type = _type;
		groupType = GroupType.ITEM;
		initialize();
        transparency = true;
	}

	@Override
	public void reset() {
		super.reset();
		_joinedElements.clear();
	}

	@Override
	public boolean canMoveFrom(TileSprite sprite, int moveDirection) {
		return true;
	}
	
	@Override
	public boolean canMoveToMe(TileSprite sprite, int moveDirection) {
		return true;
	}
	
	@Override
	public boolean stoodOn(TileSprite sprite, boolean onORoff) {
		if (stoodon == sprite && sprite.type == Type.PLAYER) {
			_game.gameData._inventory.addInventory(type);
			Sounds.play(Sounds.ting);
		}
		return true; 
	}	

	@Override
	public boolean stepToTiles(TileSprite sprite, boolean dontKill) {
		if (sprite.type == Type.PLAYER) {
			stoodon = sprite;
			return true;
		}
		return true;
	}
	@Override
	public boolean stepOffTiles(TileSprite sprite, boolean clearAhead) {
		//first was stepped to - second it was under a tile or we started on it
		if (sprite.type == Type.PLAYER)  {
			if (stoodon == sprite) {
				kill();
				return true;
			}
			_game.gameData._inventory.addInventory(type);
			Sounds.play(Sounds.ting);
			kill();
		}
		return true;
	}
	@Override
	public boolean isItem() {
		return true;
	}

}
