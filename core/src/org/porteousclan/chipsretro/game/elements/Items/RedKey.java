package org.porteousclan.chipsretro.game.elements.Items;

import org.porteousclan.chipsretro.DirectedGame;

public class RedKey extends InventoryItem {

	public RedKey(DirectedGame game, int x, int y, int _type, int layer, int direction) {
		super( game, x, y, _type, layer, direction);
	}
	@Override
	protected void initialize() {
		animNorth = "red_key";
		animSouth = "red_key";
		animEast = "red_key";
		animWest = "red_key";
		super.initialize();
	}

}
