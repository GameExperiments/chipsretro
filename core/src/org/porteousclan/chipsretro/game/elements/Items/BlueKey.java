package org.porteousclan.chipsretro.game.elements.Items;

import org.porteousclan.chipsretro.DirectedGame;
import org.porteousclan.chipsretro.game.data.Type;
import org.porteousclan.chipsretro.game.elements.TileSprite;

public class BlueKey extends InventoryItem {

	public BlueKey(DirectedGame game, int x, int y, int _type, int layer, int direction) {
		super( game, x, y, _type, layer, direction);
	}

	@Override
	protected void initialize() {
		animNorth = "blue_key";
		animSouth = "blue_key";
		animEast = "blue_key";
		animWest = "blue_key";
		super.initialize();
	}

	@Override
	public boolean canMoveToMe(TileSprite sprite, int moveDirection) {
		return true;
	}
    @Override
    public boolean stepToTiles(TileSprite sprite, boolean dontKill) {
        stoodon = sprite;
        return true;
    }
    @Override
    public boolean stepOffTiles(TileSprite sprite, boolean clearAhead) {
        //in lynx monsters can pick up blue key
        if (_game.gameData.lynxStyleMoveConditions && (sprite.type != Type.PLAYER && sprite.isCreature())) { //is player creature?
            if (stoodon == sprite) {
                kill();
                return true;
            }
        }
        return super.stepOffTiles(sprite, clearAhead);
    }

}
