package org.porteousclan.chipsretro.game.elements;

import org.porteousclan.chipsretro.DirectedGame;
import org.porteousclan.chipsretro.game.data.DirectionStack;
import org.porteousclan.chipsretro.game.data.GameData;
import org.porteousclan.chipsretro.game.data.Type;

/**
 * Created by Richard on 2017-04-17.
 */

public class CheckMove {
    private DirectedGame _game;
    private TileSprite spriteBeingChecked;

    public CheckMove(DirectedGame game, TileSprite spriteToCheck) {
        spriteBeingChecked = spriteToCheck;
        _game = game;
    }

    public boolean attemptMove() {
        //start with move;
        if (_game.gameData.msStyleMovement) {
            spriteBeingChecked.moveWanted();
        }
        spriteBeingChecked.direction = spriteBeingChecked.movePop();
        spriteBeingChecked.direction = directionAffected(spriteBeingChecked.direction);

        //needed for ice/forcefloor under dirt
        if (!spriteBeingChecked.moving) {
            return false;
        }

        boolean attemptSuccess = checkCanMove();

        //needed for ??
        if (!spriteBeingChecked.moving) {
            return false;
        }

        if (spriteBeingChecked.direction == GameData.NONE) {
            return false;
        }
//*****//

        //boolean trySuccess = false;
        if (attemptSuccess) {
            int lowerId = DirectionStack.getLowerId(_game, spriteBeingChecked, spriteBeingChecked.direction);
            TileSprite lowerSprite = DirectionStack.getElement(_game, lowerId);
            int[] bag = DirectionStack.getDirectionStack(spriteBeingChecked.direction, spriteBeingChecked.currentGridX, spriteBeingChecked.currentGridY);
            if (checkTryMove(bag, lowerSprite)) {
                boolean ret =  advance(spriteBeingChecked, spriteBeingChecked.direction, spriteBeingChecked.currentGridX, spriteBeingChecked.currentGridY);
                if (spriteBeingChecked == _game.gameData._player)
                    _game.gameData.slapOnly = false;
                return ret;
            }
        }
        //_game.gameData.slapOnly = false;
        return false;
    }



    public boolean advance(TileSprite spriteToCheck, int direction, int currentGridX, int currentGridY) {
        TileSprite itemSprite = null, topUpperLowerSprite = null, topIncludingVisitorSprite = null, bottomSprite = null;

            //here it is
            spriteToCheck.goAhead(direction, currentGridX, currentGridY);

            spriteToCheck.obstructed = false;
            spriteToCheck.directionIndex = 0;
            spriteToCheck.place();

            //set Stood Ons

            if (DirectionStack.getTopUpperLowerITEMid(_game, spriteToCheck, GameData.NONE) > Type.EMPTY) {
                itemSprite = DirectionStack.getElement(_game,DirectionStack.getTopUpperLowerITEMid(_game, spriteToCheck, GameData.NONE));
            }
            if (DirectionStack.getTopUpperLowerId(_game, spriteToCheck, GameData.NONE) > Type.EMPTY) {
                topUpperLowerSprite = DirectionStack.getElement(_game,DirectionStack.getTopUpperLowerId(_game, spriteToCheck, GameData.NONE));
            }
            if (DirectionStack.getTopAllLayersId(_game, spriteToCheck, GameData.NONE) > Type.EMPTY) {
                topIncludingVisitorSprite = DirectionStack.getElement(_game,DirectionStack.getTopAllLayersId(_game, spriteToCheck, GameData.NONE));
            }
            if (DirectionStack.getLowerId(_game, spriteToCheck, GameData.NONE) > Type.EMPTY) {
                bottomSprite = DirectionStack.getElement(_game,DirectionStack.getLowerId(_game, spriteToCheck, GameData.NONE));
            }

            if (itemSprite != null) {
                if (topUpperLowerSprite != null) {
                    if (spriteToCheck.active && topUpperLowerSprite._layer < itemSprite._layer)
                        itemSprite.stoodOn(spriteToCheck, true);//not covered
                }
                else
                    if(spriteToCheck.active)
                        itemSprite.stoodOn(spriteToCheck, true);//onTiles(sprite1);
            }
            if (spriteToCheck.active && topUpperLowerSprite != null) {
                topUpperLowerSprite.stoodOn(spriteToCheck, true);
            }

            if (topIncludingVisitorSprite == topUpperLowerSprite || topIncludingVisitorSprite == itemSprite)
                topIncludingVisitorSprite = null;
            if (topIncludingVisitorSprite != null) {
                topIncludingVisitorSprite.stoodOn(spriteToCheck, true);
            }
            if (spriteToCheck.active && bottomSprite != null && bottomSprite.type == Type.EXIT) {
                bottomSprite.stoodOn(spriteToCheck, true);
            }
            return true;


    }


    public boolean checkCanMove() {


//move to front - want to get direction affected first
        if (obstructedMoveFrom(spriteBeingChecked, spriteBeingChecked.direction)) {
            canWeBumpBlock(true);
            spriteBeingChecked.obstructed = true;
            return false;
        }


        //check if obstructedOrEmptyMoveTo and try new direction
        if (obstructedMoveTo(spriteBeingChecked, spriteBeingChecked.direction)) {
            spriteBeingChecked.obstructed = true;
            stepOffTile(false);
            canWeBumpBlock(true);
            spriteBeingChecked.obstructed = true;
            return false;
        }
        return true;
    }

    public void canWeBumpBlock(boolean checkGameMode) {
        int lowerId = DirectionStack.getLowerId(_game, spriteBeingChecked, spriteBeingChecked.direction);
        TileSprite lowerSprite = DirectionStack.getElement(_game, lowerId);
        int upperId = DirectionStack.getUpperId(_game, spriteBeingChecked, spriteBeingChecked.direction);
        TileSprite upperSprite = DirectionStack.getElement(_game, upperId);
        int visitorId = DirectionStack.getVisitorId(_game, spriteBeingChecked, spriteBeingChecked.direction);
        TileSprite visitorSprite = DirectionStack.getElement(_game, visitorId);

        TileSprite[] bag = {visitorSprite, upperSprite, lowerSprite};
        //try push block (even if we cant move)
        if (spriteBeingChecked.type == Type.PLAYER) {
            //            Sounds.play(Sounds.oof); //not if block
            for (TileSprite sprite : bag) {
                if (sprite != null) {
                    //can't bump through thin walls in lynx but can in MS
                    if (checkGameMode) {
                        if (!_game.gameData.lynxStyleMoveConditions && (sprite.type == Type.BLOCK || sprite.type == Type.ICEBLOCK || sprite.type == Type.CLONEBLOCK)) { //can we bump it
                            sprite.stepToTiles(spriteBeingChecked, false);
                        }
                    } else {
                        if (sprite.type == Type.BLOCK || sprite.type == Type.ICEBLOCK || sprite.type == Type.CLONEBLOCK) { //can we bump it
                            sprite.stepToTiles(spriteBeingChecked, false);
                        }
                    }
                    if(!sprite.transparency) break;
                }
            }
        }

    }

    public boolean checkTryMove(int[] bag, TileSprite lowerSprite) {
        //int[] bag = DirectionStack.getDirectionStack(spriteBeingChecked.direction, spriteBeingChecked.currentGridX, spriteBeingChecked.currentGridY);

        if(obstructedFromSteppingToTiles(spriteBeingChecked, bag, spriteBeingChecked.direction)){
            canWeBumpBlock(false); //new
            spriteBeingChecked.obstructed = true;
            stepOffTile(false);
            return false;
        }

        if ( lowerSprite != null ) {
            //adjusted to play cclp2 level 76 fire bugs
//            if (lowerSprite.type == Type.PLAYER || (lowerSprite.type == Type.EXIT && spriteBeingChecked.type == Type.PLAYER) )
            if (lowerSprite.type == Type.EXIT && spriteBeingChecked.type == Type.PLAYER)
                lowerSprite.stepToTiles(spriteBeingChecked, false);
        }
        stepOffTile(true);
        return true;
    }

    public int directionAffected(int _direction) {
        int id4;
        TileSprite sprite4=null;

        id4 = DirectionStack.getTopUpperLowerId(_game, spriteBeingChecked, GameData.NONE);

        if (id4 > Type.EMPTY) {
            sprite4 = DirectionStack.getElement(_game,id4);
            if (sprite4.transparency) {
                id4 = DirectionStack.getLowerId(_game, spriteBeingChecked, GameData.NONE);
                if (id4 > Type.EMPTY)
                    sprite4 = DirectionStack.getElement(_game,id4);
            }

        }
        if (id4 > Type.EMPTY) {
            return sprite4.yourDirectionAffected(spriteBeingChecked,_direction);
        }
        return _direction;
    }


    private boolean stepOffTile(boolean clearAhead){
        int topItemType,underspriteBeingCheckedType;
        TileSprite spriteToStepOff, topItemSprite, underspriteBeingCheckedSprite;
        //we step off whatever is directly under and perform actions

        topItemType = DirectionStack.getTopUpperLowerITEMid(_game, spriteBeingChecked, GameData.NONE);
        topItemSprite = DirectionStack.getElement(_game, topItemType);
        underspriteBeingCheckedType = DirectionStack.getTopUpperLowerId(_game, spriteBeingChecked, GameData.NONE);
        underspriteBeingCheckedSprite = DirectionStack.getElement(_game,underspriteBeingCheckedType); //_game.gameData._elements.get(underspriteBeingCheckedType);

        if (topItemSprite != null) {
            if (underspriteBeingCheckedSprite != null) {
                if (topItemSprite._layer > underspriteBeingCheckedSprite._layer) {
                    spriteToStepOff = topItemSprite;
                } else {
                    spriteToStepOff = underspriteBeingCheckedSprite;
                }
            } else {
                spriteToStepOff = topItemSprite;
            }

        } else {
            spriteToStepOff = underspriteBeingCheckedSprite;
        }

        if (spriteToStepOff != null)
            spriteToStepOff.stepOffTiles(spriteBeingChecked,clearAhead);

        return clearAhead;
    }

    public boolean obstructedMoveFrom(TileSprite sprite, int moveDirection) {
//        if (spriteBeingChecked.type == Type.BUG)
//            Gdx.app.log("Debug", "choose me, I'm just a bug");

        int[] bag = DirectionStack.getDirectionStack(GameData.NONE, sprite.currentGridX, sprite.currentGridY);
//        TileSprite sprite1 = GameData.getElement(_game, bag[GameData.VISITOR_LAYER]);
        TileSprite sprite2 = DirectionStack.getElement(_game, bag[GameData.UPPER_LAYER]);
        TileSprite sprite3 = DirectionStack.getElement(_game, bag[GameData.LOWER_LAYER]);
        if(sprite3 != null && sprite2 != null) {
            if (!sprite2.transparency ) { //&& lowerSprite.isItem()) {
                bag[GameData.LOWER_LAYER]=Type.EMPTY;
                sprite3 = null;
            }
        }

        TileSprite[] sbag = {sprite2, sprite3};
        for (TileSprite usprite : sbag) {
            if (usprite != null && usprite != sprite) {
                if (!usprite.canMoveFrom(sprite, moveDirection)) {
                    return true;
                }
            }
        }
        return false;
    }

    public boolean obstructedFromSteppingToTiles(TileSprite sprite, int direction) {
        int[] bag = DirectionStack.getDirectionStack(direction, sprite.currentGridX, sprite.currentGridY);
        return obstructedFromSteppingToTiles(spriteBeingChecked, bag, direction);
    }

    public boolean obstructedFromSteppingToTiles(TileSprite spriteToCheck, int[] bag, int direction) {

        if (bag == null)
            return true;
        //if border
        if (bag[0] < Type.EMPTY || bag[1] < Type.EMPTY || bag[2] < Type.EMPTY)
            return true;

        TileSprite sprite1 = null, sprite2 = null, sprite3 = null;

        sprite1 = DirectionStack.getElement(_game, bag[GameData.VISITOR_LAYER]);
        sprite2 = DirectionStack.getElement(_game, bag[GameData.UPPER_LAYER]);
        sprite3 = DirectionStack.getElement(_game, bag[GameData.LOWER_LAYER]);
        if(sprite3 != null && (sprite2 != null || sprite1 != null)) {
            //problem with block ... but should it be transparent
            if (!((sprite2 != null && sprite2.transparency) || (sprite1 != null && sprite1.transparency)) ) { //&& lowerSprite.isItem()) {
                bag[GameData.LOWER_LAYER]=Type.EMPTY;
                sprite3 = null;
            }
        }
//Anyone that stops us is enough to stop us .... no not if hidden
        if (bag[GameData.LOWER_LAYER] > Type.EMPTY) {
            //this is for things under items
            if (sprite2 != null && sprite2.transparency && _game.gameData.enableTransparencyGlitch) {
                if (!sprite3.stepToTiles(spriteToCheck, true))
                    return true;
            } else {
                if (!sprite3.stepToTiles(spriteToCheck, false))
                    return true;
            }
        }
        if (bag[GameData.UPPER_LAYER] > Type.EMPTY)
            if (spriteToCheck.active && !sprite2.stepToTiles(spriteToCheck, false))
                return true;
        if (bag[GameData.VISITOR_LAYER] > Type.EMPTY)
            if (spriteToCheck.active && !sprite1.stepToTiles(spriteToCheck, false))
                return true;

        return false;
    }

    public boolean obstructedMoveTo(TileSprite sprite, int direction) {
        int[] bag = DirectionStack.getDirectionStack(direction, sprite.currentGridX, sprite.currentGridY);
        return obstructedMoveTo( bag , direction);
    }
    public boolean obstructedMoveTo(int[] bag, int direction) {
        //if border
        if (bag[0] < Type.EMPTY || bag[1] < Type.EMPTY || bag[2] < Type.EMPTY)
            return true;

        TileSprite sprite1 = null, sprite2 = null, sprite3 = null;

        sprite1 = DirectionStack.getElement(_game, bag[GameData.VISITOR_LAYER]);
        sprite2 = DirectionStack.getElement(_game, bag[GameData.UPPER_LAYER]);
        sprite3 = DirectionStack.getElement(_game, bag[GameData.LOWER_LAYER]);

        if(sprite3 != null && sprite2 != null) {
            if (!(sprite2 != null && sprite2.transparency) ) {
                bag[GameData.LOWER_LAYER]=Type.EMPTY;
                sprite3 = null;
            }
        }
        if(sprite3 != null && sprite1 != null) {
            if (!(sprite1 != null && sprite1.transparency) ) {
                bag[GameData.LOWER_LAYER]=Type.EMPTY;
                sprite3 = null;
            }
        }

        //also called from clone machine
        if (sprite1 != null) //bag[GameData.VISITOR_LAYER] > Type.EMPTY)
            if (spriteBeingChecked != sprite1 && !sprite1.canMoveToMe(spriteBeingChecked, direction))
                return true;
        if (sprite2 != null) //bag[GameData.UPPER_LAYER] > Type.EMPTY)
            if (spriteBeingChecked != sprite2 && !sprite2.canMoveToMe(spriteBeingChecked, direction))
                return true;
        if (sprite3 != null) //bag[GameData.LOWER_LAYER] > Type.EMPTY)
            if (spriteBeingChecked != sprite3 && !sprite3.canMoveToMe(spriteBeingChecked, direction))
                return true;

        return false;
    }


}
