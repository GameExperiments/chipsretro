package org.porteousclan.chipsretro.game.elements.tiles;

import org.porteousclan.chipsretro.DirectedGame;
import org.porteousclan.chipsretro.game.data.DirectionStack;
import org.porteousclan.chipsretro.game.data.Sounds;
import org.porteousclan.chipsretro.game.data.Type;
import org.porteousclan.chipsretro.game.elements.TileSprite;

import java.util.ArrayList;

public class Teleport extends TileSprite {
	

	public Teleport(DirectedGame game, int x, int y, int _type, int layer, int direction) {
		super(null, game, x, y, direction);

		_layer = layer;
		_originalType = type = _type;
		_joinedElements = new ArrayList<JoinedTile>();
		initialize();

	}

	@Override
	protected void initialize() {
		animNorth = "teleport";
		animSouth = "teleport";
		animEast = "teleport";
		animWest = "teleport";
		super.initialize();
	}

	@Override
	public void reset() {
		super.reset();
		_joinedElements.clear();
	}
	
	@Override
	public boolean stepToTiles(TileSprite sprite, boolean dontKill) {
        if (!dontKill) {
            direction = sprite.direction;
            //sprite.teleloop = this;
            stoodon = sprite;
        }
        return true;
	}

	@Override
	public boolean stoodOn(TileSprite sprite, boolean onORoff) {

        //TODO: if only teleport valid to step off it steps back instead of forward

        //Move me to the last where I can step off next turn
        int[][] keepTrack = new int[33][33];
        Sounds.play(Sounds.derezz);
        JoinedTile teleportConnector = findJoined(direction);
        TileSprite connectedSprite = teleportConnector.joinedSprite;

        while (teleportConnector != null && connectedSprite != this) {
            keepTrack[connectedSprite.currentGridX][connectedSprite.currentGridY] = 1;
            sprite.direction = teleportConnector.directionOut; // not necessary at the mo - but if the portals were to be made in different directions  this would be needed
            int[] bag = DirectionStack.getDirectionStack(teleportConnector.directionOut, connectedSprite.currentGridX, connectedSprite.currentGridY);
            //Can we step off?
            if (sprite.checkMove.obstructedMoveTo(bag, teleportConnector.directionOut)) {
                teleportConnector = connectedSprite.findJoined(teleportConnector.directionOut);
                connectedSprite = teleportConnector.joinedSprite;
                continue;
            }
//                {
            //is the next Teleport - if yes loop again
            int upperId = DirectionStack.getTopUpperLowerId(_game, connectedSprite, teleportConnector.directionOut);
            if (upperId > Type.EMPTY) {
                TileSprite upperSprite = DirectionStack.getElement(_game, upperId);

                if (upperSprite.type == Type.TELEPORT) {
                    if (keepTrack[upperSprite.currentGridX][upperSprite.currentGridY] > 0) {
                        //turn around and go back to where we started
                        stepOffTiles(sprite, false);
                        connectedSprite = this;
                        connectedSprite.direction = sprite.direction;
                        break;
                    }

                    teleportConnector = upperSprite.findJoined(teleportConnector.directionOut);
                    connectedSprite = teleportConnector.joinedSprite;
                    continue;
                }
            }
            int lowerId = DirectionStack.getLowerId(_game, connectedSprite, teleportConnector.directionOut);
            //if (lowerId > Type.EMPTY) {
            TileSprite lowerSprite = DirectionStack.getElement(_game, lowerId);
            if (sprite.checkMove.checkTryMove(bag, lowerSprite)) {
                //Step off point
                break;
            }
                //}
//                }
            //No check next
            teleportConnector = connectedSprite.findJoined(teleportConnector.directionOut);
            connectedSprite = teleportConnector.joinedSprite;
            //}
        }
        sprite.direction = direction;

        int[] bag = DirectionStack.getDirectionStack(sprite.direction, connectedSprite.currentGridX, connectedSprite.currentGridY);
        if (sprite.checkMove.obstructedFromSteppingToTiles(sprite,bag, sprite.direction)) {
            stepOffTiles(sprite, false); //turn around
            direction = sprite.direction;
        }

//        sprite.goAhead(GameData.NONE,connectedSprite.currentGridX,connectedSprite.currentGridY);
//        sprite.lastdirection = direction;
//        sprite.drawDirection = direction;
//
//        sprite.moveDir(direction);
//        sprite.moving = false;
        sprite.checkMove.advance(sprite,direction, connectedSprite.currentGridX, connectedSprite.currentGridY);
		return true;
	}
	
	@Override
	public boolean canMoveToMe(TileSprite sprite, int moveDirection) {
		return true;
	}

	@Override
	public boolean stepOffTiles(TileSprite sprite, boolean clearAhead) {
        if (!clearAhead)
            sprite.lastdirection = sprite.direction = sprite.getReversed(sprite.direction);
        return true;
	}

	@Override
	public void joinTiles(TileSprite sprite, int directionIn, int directionOut) {
		JoinedTile tile = new JoinedTile();
		tile.joinedSprite = sprite;
		tile.directionIncoming = directionIn;
		tile.directionOut = directionOut;
		_joinedElements.add(tile);
	}

	@Override
    public int yourDirectionAffected(TileSprite sprite, int direction) {
        return sprite.direction;
    }

    @Override
    public boolean slippery(TileSprite sprite) {
        if (stoodon == sprite && !sprite.moving )
            sprite.slide(sprite.direction, type);
        return true;
    }

}
