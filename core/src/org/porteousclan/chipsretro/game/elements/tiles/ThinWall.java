package org.porteousclan.chipsretro.game.elements.tiles;

import org.porteousclan.chipsretro.DirectedGame;
import org.porteousclan.chipsretro.game.data.GameData;
import org.porteousclan.chipsretro.game.data.ImageCache;
import org.porteousclan.chipsretro.game.data.Sounds;
import org.porteousclan.chipsretro.game.data.Type;
import org.porteousclan.chipsretro.game.elements.TileSprite;

import java.util.ArrayList;

public class ThinWall extends TileSprite {

	public ThinWall(DirectedGame game, int x, int y, int _type, int layer, int direction) {
		super(null, game, x, y, direction);

		_layer = layer;
		_originalType = type = _type;
		_joinedElements = new ArrayList<JoinedTile>();
		initialize();

	}

	@Override
	protected void initialize() {
		animNorth = "thin_wall_n";
		animSouth = "thin_wall_s";
		animEast = "thin_wall_e";
		animWest = "thin_wall_w";
		TEXTURE_4 = ImageCache.getFrame("thin_wall_se", -1);
		super.initialize();
		switch (type) {
			case Type.WALL_SE:
				_restFrame = TEXTURE_4;
				break;
		}
	}

	@Override
	public void reset() {
		super.reset();
		_joinedElements.clear();
	}


	@Override
	public boolean canMoveFrom(TileSprite sprite, int moveDirection) {
        boolean returnVal = true;
		switch (type) {
		case Type.WALL_N:
			if (moveDirection == GameData.NORTH) {
				returnVal =  false;
			}
			break;
		case Type.WALL_S:
			if (moveDirection == GameData.SOUTH) {
				returnVal =  false;
			}
			break;
		case Type.WALL_E:
			if (moveDirection == GameData.EAST) {
				returnVal =  false;
			}
			break;
		case Type.WALL_W:
			if (moveDirection == GameData.WEST) {
				returnVal =  false;
			}
			break;
		case Type.WALL_SE:
			if ((moveDirection == GameData.SOUTH)
					|| (moveDirection == GameData.EAST)) {
				returnVal =  false;
			}
			break;
		}
		if (returnVal == false) {
			sprite.obstructed = true;
			if (sprite.type == Type.PLAYER) {
				Sounds.play(Sounds.oof);
			}

		}
		return returnVal;
	}

	@Override
	public boolean canMoveToMe(TileSprite sprite, int moveDirection) {
		boolean returnVal = true;
		switch (type) {
		case Type.WALL_N:
			if (moveDirection == GameData.SOUTH) {
				returnVal = false;
			}
			break;
		case Type.WALL_S:
			if (moveDirection == GameData.NORTH) {
				returnVal = false;
			}
			break;
		case Type.WALL_E:
			if (moveDirection == GameData.WEST) {
				returnVal = false;
			}
			break;
		case Type.WALL_W:
			if (moveDirection == GameData.EAST) {
				returnVal = false;
			}
			break;
		case Type.WALL_SE:
			if ((moveDirection == GameData.NORTH)
					|| (moveDirection == GameData.WEST)) {
				returnVal = false;
			}
			break;
		}
		if (returnVal == false) {
			if (sprite.type == Type.PLAYER) {
				Sounds.play(Sounds.oof);
			}

		}
		return returnVal;
	}

	@Override
	public boolean stepToTiles(TileSprite sprite, boolean dontKill) {
		switch (type) {
		case Type.WALL_N:
			if (sprite.direction != GameData.SOUTH) {
				stoodon = sprite;
				return true;
			}
			break;
		case Type.WALL_S:
			if (sprite.direction != GameData.NORTH) {
				stoodon = sprite;
				return true;
			}
			break;
		case Type.WALL_E:
			if (sprite.direction != GameData.WEST) {
				stoodon = sprite;
				return true;
			}
			break;
		case Type.WALL_W:
			if (sprite.direction != GameData.EAST) {
				stoodon = sprite;
				return true;
			}
			break;
		case Type.WALL_SE:
			if ((sprite.direction != GameData.NORTH) && (sprite.direction != GameData.WEST)) {
				stoodon = sprite;
				return true;
			}
			break;
		}
		return false;
	}

}
