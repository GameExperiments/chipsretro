package org.porteousclan.chipsretro.game.elements.tiles;

import org.porteousclan.chipsretro.DirectedGame;
import org.porteousclan.chipsretro.game.data.Sounds;
import org.porteousclan.chipsretro.game.data.Type;
import org.porteousclan.chipsretro.game.elements.TileSprite;

public class Dirt extends TileSprite {

	public Dirt(DirectedGame game, int x, int y, int _type, int layer, int direction) {
		super(null, game, x, y, direction);
		_layer = layer;
		_originalType = type = _type;
		initialize();
	}

	@Override
	protected void initialize() {
		animNorth = "dirt";
		animSouth = "dirt";
		animEast = "dirt";
		animWest = "dirt";
		super.initialize();
	}

	@Override
	public void reset() {
		super.reset();
		_joinedElements.clear();
	}
	
	@Override
	public boolean canMoveFrom(TileSprite sprite, int moveDirection) {
		return true;
	}
	
	@Override
	public boolean canMoveToMe(TileSprite sprite, int moveDirection) {
		if (sprite.type == Type.PLAYER) {
			return true;
		}
		return false;
	}

	@Override
	public boolean stoodOn(TileSprite sprite, boolean onORoff) {
		Sounds.play(Sounds.whisk);
		kill();
		return true;
	}
	
	@Override
	public boolean stepToTiles(TileSprite sprite, boolean dontKill) {
		if (sprite.type == Type.PLAYER) {
			stoodon = sprite;
			return true;				
		}
		return false;
	}


}
