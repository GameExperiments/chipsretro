package org.porteousclan.chipsretro.game.elements.tiles;

import org.porteousclan.chipsretro.DirectedGame;
import org.porteousclan.chipsretro.game.data.GameData;
import org.porteousclan.chipsretro.game.data.Sounds;
import org.porteousclan.chipsretro.game.data.Type;
import org.porteousclan.chipsretro.game.elements.TileSprite;

import java.util.ArrayList;

public class ToggleWall extends TileSprite {
	

	public ToggleWall(DirectedGame game, int x, int y, int _type, int layer, int direction) {
		super(null, game, x, y, direction);
		_layer = layer;
		_originalType = type = _type;
		_joinedElements = new ArrayList<JoinedTile>();
		initialize();

	}

	@Override
	protected void initialize() {
		animNorth = "toggle_wall_closed";
		animSouth = "toggle_wall_open";
		animEast = "toggle_wall_closed";
		animWest = "toggle_wall_open";
		super.initialize();
	}

	@Override
	public void reset() {
		super.reset();
		_joinedElements.clear();
	}
	
	@Override
	public boolean canMoveToMe(TileSprite sprite, int moveDirection) {
		if (this.direction == GameData.SOUTH){
			return true;
		}
		return false;
	}
	
	@Override
	public boolean stepToTiles(TileSprite sprite, boolean dontKill) {
		if (direction == GameData.SOUTH) {
			stoodon = sprite;
			return true;
		}
		if (sprite.type == Type.PLAYER)
			Sounds.play(Sounds.oof);
		return false;

	}
	
	@Override
	public void trigger(boolean release) {
		if (type == Type.TOGGLEWALL) {
			Sounds.play(Sounds.whisk);
			if (direction == GameData.NORTH) {
				direction= GameData.SOUTH;
				setSkin(TEXTURE_2);
			} else {
				direction= GameData.NORTH;
				setSkin(TEXTURE_0);
			}
		}				
	}

	@Override
    public boolean canMoveFrom(TileSprite sprite, int moveDirection) {
        if (_game.gameData.lynxStyleMoveConditions && this.direction == GameData.NORTH && sprite.type != Type.PLAYER)
            return false;
        return true;
    }

}
