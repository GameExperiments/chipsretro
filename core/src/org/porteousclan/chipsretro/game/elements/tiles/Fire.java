package org.porteousclan.chipsretro.game.elements.tiles;

import org.porteousclan.chipsretro.DirectedGame;
import org.porteousclan.chipsretro.game.data.GameData;
import org.porteousclan.chipsretro.game.data.Sounds;
import org.porteousclan.chipsretro.game.data.Type;
import org.porteousclan.chipsretro.game.elements.TileSprite;

public class Fire extends TileSprite {

	public Fire(DirectedGame game, int x, int y, int _type, int layer, int direction) {
		super(null, game, x, y, direction);

		_layer = layer; //clone machine does not follow this
		_originalType = type = _type;
		initialize();


	}

	@Override
	protected void initialize() {
		animNorth = "fire";
		animSouth = "fire";
		animEast  = "fire";
		animWest  = "fire";
		super.initialize();
	}

	@Override
	public void reset() {
		super.reset();
		_joinedElements.clear();
	}

	@Override
	public boolean canMoveToMe(TileSprite sprite, int moveDirection) {
        if (sprite.type == Type.PLAYER) return true;
		if (_game.gameData.msStyleMoveConditions && !( sprite.type == Type.BUG || sprite.type == Type.WALKER)) {
			return true;
		}
        if (_game.gameData.lynxStyleMoveConditions && (sprite.type == Type.FIREBALL || sprite.type == Type.BLOCK || sprite.type == Type.ICEBLOCK)) {
            return true;
        }
        if (_game.gameData.rommysStyleMoveConditions && (sprite.type != Type.BUG && sprite.type != Type.WALKER)) {
            return true;
        }
        return false;
	}

	@Override
	public boolean stepToTiles(TileSprite sprite, boolean dontKill) {
        if ( _game.gameData.rommysStyleMoveConditions && sprite.isCreature() && !(sprite.type == Type.FIREBALL)) {
            Sounds.play(Sounds.crackle); // should be sizzle
            sprite.kill();
        }
        if (sprite.type == Type.ICEBLOCK) {
            sprite.kill();
            TileSprite sprite2 = reuseSprite(Type.WATER);
            if (sprite2 == null) {
                _game.gameData._elements.add(new Water(_game, this.currentGridX, this.currentGridY, Type.WATER, this._layer, GameData.NONE));
            } else {
                _game.gameData._elements.add(sprite2);
            }
            this.kill();
            Sounds.play(Sounds.splash);
        }

        stoodon = sprite;
		return true;
	}

	@Override
	public boolean stoodOn(TileSprite sprite, boolean onORoff) {
		if (sprite.type == Type.PLAYER && _game.gameData._inventory.hasInventory(this.type))
		    return true;

        if (sprite.type != Type.FIREBALL && sprite.type != Type.BLOCK
                && sprite.type != Type.ICEBLOCK && sprite.type != Type.GLIDER && sprite.active) {
            Sounds.play(Sounds.crackle);
            sprite.kill(type);
        }

		return true;
	}	

}
