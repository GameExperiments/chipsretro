package org.porteousclan.chipsretro.game.elements.tiles;

import org.porteousclan.chipsretro.DirectedGame;
import org.porteousclan.chipsretro.game.data.DirectionStack;
import org.porteousclan.chipsretro.game.data.GameData;
import org.porteousclan.chipsretro.game.data.Sounds;
import org.porteousclan.chipsretro.game.data.Type;
import org.porteousclan.chipsretro.game.elements.TileSprite;

import java.util.ArrayList;

public class Trap extends TileSprite {


    boolean startedOnTrap=true;
	boolean releaseRequest=false;
	public boolean release = false;

	public Trap(DirectedGame game, int x, int y, int _type, int layer, int direction) {
		super(null, game, x, y, direction);

		_layer = layer;
		_originalType = type = _type;
		_joinedElements = new ArrayList<JoinedTile>();
        release = false;
		initialize();
	}

	@Override
	protected void initialize() {
		animNorth = "trap";
		animSouth = "trap";
		animEast = "trap";
		animWest = "trap";
		super.initialize();
	}

    @Override
    public void reset() {
        super.reset();
        release = false;
        startedOnTrap=true;
        _joinedElements.clear();
    }

	@Override
	public void update(float dt) {
		super.update(dt);

        if (releaseRequest) {
            int upperId = DirectionStack.getTopAllLayersId(_game, this, GameData.NONE);
            TileSprite upperSprite = DirectionStack.getElement(_game, upperId);

            if (_game.gameData.lynxStyleMoveConditions) {
                if (upperSprite != null && upperSprite != this)
                    this.direction = upperSprite.lastdirection;
                if (this.direction == GameData.NONE)
                    this.direction = GameData.NORTH;
            }

            if (upperSprite != null && upperSprite != this && (_game.gameData.lynxStyleMoveConditions || upperSprite.lastMoveForcedBy != GameData.NONE))
                upperSprite.moveDir(this.direction);
        }

	}

    @Override
    public boolean canMoveFrom(TileSprite sprite, int moveDirection) {
        if (sprite.type == Type.PLAYER) {
            if (_game.gameData._inventory.hasInventory(type)) {
                return true;
            }
        }
        if (startedOnTrap && _game.gameData.msStyleMoveConditions && !sprite.isCreature()) {
            release = true;
        } else {
            release = releaseRequest;
        }

        startedOnTrap = false;
        return release;
    }

	@Override
	public boolean canMoveToMe(TileSprite sprite, int moveDirection) {
		return true;
	}
	
	@Override
	public boolean stepToTiles(TileSprite sprite, boolean dontKill) {
		if (!releaseRequest) //if false
			release = false;

        if(!dontKill)
            startedOnTrap = false;

		stoodon = sprite;
        //if (!_game.gameData.msStyleMoveConditions)
        this.direction = sprite.direction;
		return true;
	}
	
	@Override
	public void trigger(boolean release) {
        releaseRequest = release;
		if (release) {
			this.release = release; //gets set to true only
		}
	}

	@Override
	public boolean stoodOn(TileSprite sprite, boolean onORoff) {
		Sounds.play(Sounds.traphit);
		return true;
	}

    @Override
    public int yourDirectionAffected(TileSprite sprite, int directionWanted) {

        int creatureDirection = directionWanted;

        if ((sprite.type == Type.TANK) || (sprite.type == Type.PLAYER))
            return directionWanted;

        if(_game.gameData.msStyleMoveConditions) {
            if (sprite.type == Type.BLOB) {
                    creatureDirection = sprite.getNextAvailableTurnBased(directionWanted);
            }
            //doesn't check if creature is immobile and reverse in rare cases
            if (sprite.type == Type.BUG || sprite.type == Type.PARAMECIUM || sprite.type == Type.TEETH) {
                creatureDirection = GameData.NONE; //needs a boss
                if (sprite._creatureMoveIndex > 0) {
                    for (int x = sprite._creatureMoveIndex-1; x>=0;x--) {
                        TileSprite spriteLastInMoveList = _game.gameData._creatureMoveOrderList.get(sprite._creatureMoveIndex - 1);
                        if (spriteLastInMoveList.active && spriteLastInMoveList.type != Type.PLAYER) {
                            creatureDirection = spriteLastInMoveList.lastdirection;
                            break;
                        }
                    }
                }
            }
        }
        if (_game.gameData.lynxStyleMoveConditions){
                creatureDirection = this.direction; //sprite.lastdirection;
        }
//        if (_game.gameData.rommysStyleMoveConditions){
//            creatureDirection = sprite.lastdirection; //sprite.lastdirection;
//        }
        return creatureDirection;
    }

}
