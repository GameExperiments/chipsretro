package org.porteousclan.chipsretro.game.elements.tiles;

import org.porteousclan.chipsretro.DirectedGame;
import org.porteousclan.chipsretro.game.data.Sounds;
import org.porteousclan.chipsretro.game.elements.TileSprite;

import java.util.ArrayList;

public class Bomb extends TileSprite {
	

	public Bomb(DirectedGame game, int x, int y, int _type, int layer, int direction) {
		super(null, game, x, y, direction);

		_layer = layer;
		_originalType = type = _type;
		_joinedElements = new ArrayList<JoinedTile>();
		initialize();

	}

	@Override
	protected void initialize() {
		animNorth = "bomb";
		animSouth = "bomb";
		animEast = "bomb";
		animWest = "bomb";
		super.initialize();
	}

	@Override
	public void reset() {
		super.reset();
		_joinedElements.clear();
	}

	@Override
	public boolean canMoveFrom(TileSprite sprite, int moveDirection) {
		return true;
	}
	
	@Override
	public boolean canMoveToMe(TileSprite sprite, int moveDirection) {
//		if (sprite.type == GameData.Type.PLAYER) {
			return true;
//		}
//		return false;
	}

	@Override
	public boolean stoodOn(TileSprite sprite, boolean onORoff) {
//		if (stoodon == sprite) {
			Sounds.play(Sounds.bomb);
			sprite.kill(type);
			kill();
//		}
		return true; 
	}	

	@Override
	public boolean stepToTiles(TileSprite sprite, boolean dontKill) {
		stoodon = sprite;
		return true;
	}

}
