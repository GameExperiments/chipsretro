package org.porteousclan.chipsretro.game.elements.tiles;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.utils.Align;

import org.porteousclan.chipsretro.DirectedGame;
import org.porteousclan.chipsretro.game.data.GameData;
import org.porteousclan.chipsretro.game.data.Type;
import org.porteousclan.chipsretro.game.elements.TileSprite;

import java.util.ArrayList;

public class Hint extends TileSprite {
	private boolean textVisible = false;
	private float text_timer = 0;


	public Hint(DirectedGame game, int x, int y, int _type, int layer, int direction) {
		super(null, game, x, y, direction);

		_layer = layer;
		_originalType = type = _type;
		_joinedElements = new ArrayList<JoinedTile>();
		initialize();

	}

	@Override
	protected void initialize() {
		animNorth = "hint";
		animSouth = "hint";
		animEast = "hint";
		animWest = "hint";
		super.initialize();
	}

	@Override
	public void update(float dt) {
		super.update(dt);

		if(stoodon == null || stoodon.type != Type.PLAYER) {
			if (text_timer > 0) {
				text_timer -= dt;
			} else {
				textVisible = false;
				text_timer =0; //er
			}
		}

	}

	@Override
	public void reset() {
		super.reset();
		_joinedElements.clear();
	}
	
	@Override
	public boolean canMoveToMe(TileSprite sprite, int moveDirection) {

		if (sprite.type == Type.PLAYER ) {
			return true;
		}
        if (!_game.gameData.lynxStyleMoveConditions &&
                (sprite.isCreature() || sprite.type == Type.BLOCK || sprite.type == Type.ICEBLOCK || sprite.type == Type.CLONEBLOCK))
            return true;

        return false;
	}

	@Override
	public boolean stepToTiles(TileSprite sprite, boolean dontKill) {
		if(sprite.type == Type.PLAYER) {
			_game.gameData.hint = this; //this is now done in levelfactory (but we may have a muti-hint situation)
			text_timer = 4;
			textVisible = true;
		}
		stoodon = sprite;
		return true;
	}

	@Override
	public boolean stoodOn(TileSprite sprite, boolean onORoff) {
		_joinedElements.size();
		if(sprite.type == Type.PLAYER) {
			_game.gameData.gsEvent = GameData.GameScreenEventMessage.HINTMESSAGE;
		}
		
		return true;
	}
	
//	@Override
//	public boolean stepOffTiles(TileSprite sprite, boolean clearAhead) {
//		super.stepOffTiles(sprite,clearAhead);
//		return false;
//	}

	public void hideText() {
		textVisible = false;
		text_timer = 0;
	}

	public void drawText(SpriteBatch spriteBatch) {
		if (textVisible) {
			_game.gameData.font2.setColor(0.1f, 0.1f, 0.1f, 1.0f);
			_game.gameData.font2.draw(spriteBatch, _game.gameData.levelhint, GameData.tileWidth, _game.screenHeight - GameData.tileWidth*2f, GameData.tileWidth * 8f, Align.center,true);//GameData.tileHeight * 7f, GameData.tileWidth * 8f, Align.center,true);
		}
	}

}
