package org.porteousclan.chipsretro.game.elements.tiles;

import com.badlogic.gdx.math.MathUtils;

import org.porteousclan.chipsretro.DirectedGame;
import org.porteousclan.chipsretro.game.data.Sounds;
import org.porteousclan.chipsretro.game.data.Type;
import org.porteousclan.chipsretro.game.elements.TileSprite;

import java.util.ArrayList;

public class RandomForceFloor extends TileSprite {
	

	public RandomForceFloor(DirectedGame game, int x, int y, int _type, int layer, int direction) {
		super(null, game, x, y, direction);

		_layer = layer; //clone machine does not follow this
		_originalType = type = _type;
		_joinedElements = new ArrayList<JoinedTile>();
		initialize();

	}

	@Override
	protected void initialize() {
		animNorth = "random_force_floor";
		animSouth = "random_force_floor";
		animEast = "random_force_floor";
		animWest = "random_force_floor";
		super.initialize();
	}

	@Override
	public void update(float dt) {
		super.update(dt);
		this.direction = MathUtils.random(0, 3);
	}

	@Override
	public void reset() {
		super.reset();
		_joinedElements.clear();
	}
	
	@Override
	public boolean canMoveFrom(TileSprite sprite, int moveDirection) {
		stoodon = sprite;
		if (sprite.type == Type.PLAYER) {
			if (_game.gameData._inventory.hasInventory(type)) {
				return true;
			}
		}

		//if (sprite.getReversed(sprite.direction) == direction && !testforactingwall(sprite) )
		//	return false;
//        if () // uuuuummmmmm so be only random direction
		return true;
	}


	@Override
	public boolean canMoveToMe(TileSprite sprite, int moveDirection) {
		//MS RULES MONSTERS TREAT THIS AS A WALL
        if (_game.gameData.msStyleMoveConditions && sprite.isCreature() ) return false;

		return true;
	}

	@Override
	public boolean stoodOn(TileSprite sprite, boolean onORoff) {
		if (!(sprite.type == Type.PLAYER && _game.gameData._inventory.hasInventory(this.type))) {
			Sounds.play(Sounds.force);
		}
		return true; 
	}
	
	@Override
	public boolean stepToTiles(TileSprite sprite, boolean dontKill) {
//		sprite.goAhead();
		stoodon = sprite;
		return true;				
	}

	@Override
	public boolean slippery(TileSprite sprite) {
        if (sprite.type == Type.PLAYER && _game.gameData._inventory.hasInventory(this.type))
            return false;

        if (stoodon == sprite ) { //&& !sprite.moving ) {
			//Sounds.play(Sounds.force);
			sprite.slide(direction, type); //randomized
		}
		return true;
	}

}
