package org.porteousclan.chipsretro.game.elements.tiles;

import org.porteousclan.chipsretro.DirectedGame;
import org.porteousclan.chipsretro.game.data.Sounds;
import org.porteousclan.chipsretro.game.data.Type;
import org.porteousclan.chipsretro.game.elements.TileSprite;

import java.util.ArrayList;

public class ComputerChip extends TileSprite { //item??
    private boolean addedToInventory=false;
	public ComputerChip(DirectedGame game, int x, int y, int _type, int layer, int direction) {
		super(null, game, x, y, direction);

		_layer = layer; //clone machine does not follow this
		_originalType = type = _type;
		_joinedElements = new ArrayList<JoinedTile>();
		initialize();


	}
	@Override
	protected void initialize() {
		animNorth = "computer_chip";
		animSouth = "computer_chip";
		animEast = "computer_chip";
		animWest = "computer_chip";
		super.initialize();
	}

	@Override
	public void reset() {
		super.reset();
        addedToInventory= false;
        _joinedElements.clear();
	}
	
	@Override
	public boolean canMoveFrom(TileSprite sprite, int moveDirection) {
		return true;
	}
	
	@Override
	public boolean canMoveToMe(TileSprite sprite, int moveDirection) {
		if (sprite.type == Type.PLAYER) {
			return true;
		}
		return false;
	}

	@Override
	public boolean stepToTiles(TileSprite sprite, boolean dontKill) {
		if (sprite.type == Type.PLAYER) {
			stoodon = sprite;
			return true;
		}
		return false;
	}

	@Override
	public boolean stepOffTiles(TileSprite sprite, boolean clearAhead) {

		if (_game.gameData.rommysStyleMoveConditions && addedToInventory && sprite.type == Type.PLAYER) {
			kill();
		}
		return true;
	}
	
	@Override
	public boolean stoodOn(TileSprite sprite, boolean onORoff) {
		if (sprite.type == Type.PLAYER) {
			Sounds.play(Sounds.chack);
//            stoodon = sprite;
			_game.gameData.chips--;
            addedToInventory = true;
            // Rommys version allows safe haven on (computer)chip
            if (!_game.gameData.rommysStyleMoveConditions)
                kill(); //both ms and lynx do this
		}
		return true;
	}

}
