package org.porteousclan.chipsretro.game.elements.tiles;

import org.porteousclan.chipsretro.DirectedGame;
import org.porteousclan.chipsretro.game.data.Sounds;
import org.porteousclan.chipsretro.game.data.Type;
import org.porteousclan.chipsretro.game.elements.TileSprite;

import java.util.ArrayList;

public class Exit extends TileSprite {
	
	public Exit(DirectedGame game, int x, int y, int _type, int layer, int direction) {
		super(null, game, x, y, direction);

		_layer = layer;
		_originalType = type = _type;
		_joinedElements = new ArrayList<JoinedTile>();
		initialize();

	}

	@Override
	protected void initialize() {
		animNorth = "exit";
		animSouth = "exit";
		animEast = "exit";
		animWest = "exit";
		super.initialize();
	}

	@Override
	public void reset() {
		super.reset();
		_joinedElements.clear();
	}
	
	@Override
	public boolean canMoveFrom(TileSprite sprite, int moveDirection) {
		if (sprite.type == Type.PLAYER && stoodon == sprite ) {
			Sounds.play(Sounds.tada);
			sprite.goalreached = true;
			stoodon = null;
			return false;
		}
		return true;
	}
	
	@Override
	public boolean canMoveToMe(TileSprite sprite, int moveDirection) {
		if (sprite.type == Type.PLAYER ) {
			return true;
		}
        if (sprite.type == Type.BLOCK || sprite.type == Type.ICEBLOCK || sprite.type == Type.CLONEBLOCK)
            return true;

        return false;
	}

	@Override
	public boolean stoodOn(TileSprite sprite, boolean onORoff) {
		if (sprite.type == Type.PLAYER && sprite.isFirstMove == false) {
			Sounds.play(Sounds.tada);
			sprite.animateType = this.type;
			sprite.goalreached = true;
			stoodon = null;
		}
		return true;
	}
	
	@Override
	public boolean stepToTiles(TileSprite sprite, boolean dontKill) {
		stoodon = sprite;
		return true;
	}
	
	@Override
	public boolean stepOffTiles(TileSprite sprite, boolean clearAhead) {
		return true;
	}

}
