package org.porteousclan.chipsretro.game.elements.tiles;

import org.porteousclan.chipsretro.DirectedGame;
import org.porteousclan.chipsretro.game.data.GameData;
import org.porteousclan.chipsretro.game.data.ImageCache;
import org.porteousclan.chipsretro.game.data.Sounds;
import org.porteousclan.chipsretro.game.data.Type;
import org.porteousclan.chipsretro.game.elements.TileSprite;

import java.util.ArrayList;

public class RecessedWall extends TileSprite {


	public RecessedWall(String skinName, int idx, DirectedGame game, int x, int y, int _type, int layer, int direction) {
		super(null, game, x, y, direction);
		_layer = layer;
		_originalType = type = _type;
		_joinedElements = new ArrayList<JoinedTile>();

		if (skinName != "")
			setSkin(ImageCache.getFrame(skinName,idx));

		initialize();

	}
	public RecessedWall(DirectedGame game, int x, int y, int _type, int layer, int direction) {
		this("",-1,game,x,y,_type,layer,direction);
	}
	
	@Override
	public void reset() {
		super.reset();
		_joinedElements.clear();
	}
	@Override
	protected void initialize() {
		animNorth = "recessed_wall";
		animSouth = "recessed_wall";
		animEast = "recessed_wall";
		animWest = "recessed_wall";
		super.initialize();
		show();
	}

	@Override
	public boolean canMoveToMe(TileSprite sprite, int moveDirection) {
		if (sprite.type == Type.PLAYER) {
			return true;				//will just play oof if not I hope
		}
		return false;
	}
	
	@Override
	public boolean stepToTiles(TileSprite sprite, boolean dontKill) {
		if (sprite.type == Type.PLAYER) {
			stoodon = sprite;
			return true;
		}
		return false;
	}

	@Override
	public boolean stepOffTiles(TileSprite sprite, boolean clearAhead) {
		if (clearAhead) {
			if (sprite.type == Type.PLAYER) {
				if (stoodon == sprite) {
					Sounds.play(Sounds.popup);
					TileSprite sprite2  = reuseSprite(Type.WALL);
					if (sprite2 == null) {
						_game.gameData._elements.add(new Wall(_game, this.currentGridX, this.currentGridY, Type.WALL, this._layer, GameData.NONE));
//					} else {
//						_game.gameData._elements.add(sprite2);
					}
					this.kill();

				}
				return true;
			}
		}
		return false;
	}

}
