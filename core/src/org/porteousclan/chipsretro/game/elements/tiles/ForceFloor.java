package org.porteousclan.chipsretro.game.elements.tiles;

import org.porteousclan.chipsretro.DirectedGame;
import org.porteousclan.chipsretro.game.data.DirectionStack;
import org.porteousclan.chipsretro.game.data.GameData;
import org.porteousclan.chipsretro.game.data.Sounds;
import org.porteousclan.chipsretro.game.data.Type;
import org.porteousclan.chipsretro.game.elements.TileSprite;

import java.util.ArrayList;

public class ForceFloor extends TileSprite {
	

	public ForceFloor(DirectedGame game, int x, int y, int _type, int layer, int direction) {
		super(null, game, x, y, direction);

		_layer = layer; //clone machine does not follow this
		_originalType = type = _type;
		_joinedElements = new ArrayList<JoinedTile>();
		initialize();

	}

	@Override
	protected void initialize() {
		animNorth = "force_floor_n";
		animSouth = "force_floor_s";
		animEast  = "force_floor_e";
		animWest  = "force_floor_w";
		super.initialize();
	}

	@Override
	public void reset() {
		super.reset();
		_joinedElements.clear();
	}
	
	@Override
	public boolean canMoveFrom(TileSprite sprite, int moveDirection) {
		stoodon = sprite;
		if (sprite.type == Type.PLAYER) {
			if (_game.gameData._inventory.hasInventory(type)) {
				return true;
			}
		}
        //forced move exception
		if ( !_game.gameData.lynxStyleMoveConditions && sprite.lastMoveForcedBy != GameData.NONE && getReversed(moveDirection) == direction)
			return true;
        if ( !_game.gameData.lynxStyleMoveConditions && testforactingwall(sprite) && getReversed(moveDirection) == direction) {
            sprite.evenMove = true; //Gives an extra wanted move // TODO: doesn't seem to help
            return true;
        }
        //acting wall exception
        if ( getReversed(moveDirection) != direction) {
            //sprite.toggleEvenMove();
            return true;
        }
        sprite.direction = sprite.lastdirection = direction;
		return true;
	}
	
	private boolean testforactingwall(TileSprite sprite) {
		TileSprite sprite2=null;
		//check if wall or block in the way
        if (_game.gameData.lynxStyleMoveConditions ) return false;
		int tempType= DirectionStack.getTopAllLayersId(_game, this, direction);
		if ( tempType> Type.EMPTY) {
			sprite2 = DirectionStack.getElement(_game,tempType);//_game.gameData._elements.get(GameData.getTopAllLayersId(_game, this, direction));
		}
		if(sprite2 != null && (!sprite2.canMoveToMe(sprite, direction) )) {
			return true;
		}
        return false;
	}

	@Override
	public boolean canMoveToMe(TileSprite sprite, int moveDirection) {
		return true;
	}
	
	@Override
	public boolean stoodOn(TileSprite sprite, boolean onORoff) {
		if (!(sprite.type == Type.PLAYER && _game.gameData._inventory.hasInventory(this.type))) {
			Sounds.play(Sounds.force);
		}
		return true; 
	}

	@Override
	public boolean stepToTiles(TileSprite sprite, boolean dontKill) {
//		sprite.goAhead();
		stoodon = sprite;
		return true;				
	}

	@Override
	public boolean slippery(TileSprite sprite) {
        if (sprite.type == Type.PLAYER && _game.gameData._inventory.hasInventory(this.type))
            return false;

        if (stoodon == sprite ) {
			sprite.slide(direction, type);
            if(!_game.gameData.msStyleMoveConditions)
                sprite.speed = sprite.defaultSpeed * sprite.speedMultiplier;
            else
                sprite.speed = defaultSpeed * speedMultiplier;
            //lastMoveForcedBy = this.type;
		}
		return true;
	}


}
