package org.porteousclan.chipsretro.game.elements.tiles;

import org.porteousclan.chipsretro.DirectedGame;
import org.porteousclan.chipsretro.game.data.Sounds;
import org.porteousclan.chipsretro.game.data.Type;
import org.porteousclan.chipsretro.game.elements.TileSprite;

import java.util.ArrayList;

public class RedButton extends TileSprite {
	

	public RedButton(DirectedGame game, int x, int y, int _type, int layer, int direction) {
		super(null, game, x, y, direction);

		_layer = layer;
		_originalType = type = _type;
		_joinedElements = new ArrayList<JoinedTile>();
		initialize();

	}

	@Override
	protected void initialize() {
		animNorth = "red_button";
		animSouth = "red_button";
		animEast = "red_button";
		animWest = "red_button";
		super.initialize();
	}

	@Override
	public void reset() {
		super.reset();
		_joinedElements.clear();
	}
	
	@Override
	public boolean canMoveToMe(TileSprite sprite, int moveDirection) {
		return true;
	}

	@Override
	public boolean stepToTiles(TileSprite sprite, boolean dontKill) {
		Sounds.play(Sounds.click);
		stoodon = sprite;
		return true;
	}
	
	@Override
	public boolean stoodOn(TileSprite sprite, boolean onORoff) {
		int len = _joinedElements.size();
		int i = 0;
		TileSprite element = null;
		if (onORoff) {
			for (i = 0; i < len; i++) {
				element = _joinedElements.get(i).joinedSprite;
				if ( (element.type == Type.CLONEBLOCK) || (element.type == Type.CLONEMACHINE)) { //shouldbe cloneblock?
					element.trigger(onORoff);
				}
			}
		}
		return true;
	}

    @Override
    public void joinTiles(TileSprite sprite, int directionIn, int directionOut) {
        super.joinTiles(sprite,directionIn,directionOut);
        sprite.wired = true;
    }

	@Override
	public boolean stepOffTiles(TileSprite sprite, boolean clearAhead) {
		if (clearAhead) {
			stoodOn(sprite, false);
			return true;
		}
		return false;
	}

}
