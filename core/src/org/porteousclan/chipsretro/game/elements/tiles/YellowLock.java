package org.porteousclan.chipsretro.game.elements.tiles;

import org.porteousclan.chipsretro.DirectedGame;
import org.porteousclan.chipsretro.game.data.Sounds;
import org.porteousclan.chipsretro.game.data.Type;
import org.porteousclan.chipsretro.game.elements.TileSprite;

import java.util.ArrayList;

public class YellowLock extends TileSprite {
	

	public YellowLock(DirectedGame game, int x, int y, int _type, int layer, int direction) {
		super(null, game, x, y, direction);

		_layer = layer; //clone machine does not follow this
		_originalType = type = _type;
		_joinedElements = new ArrayList<JoinedTile>();
		initialize();

	}

	@Override
	protected void initialize() {
		animNorth = "yellow_lock";
		animSouth = "yellow_lock";
		animEast = "yellow_lock";
		animWest = "yellow_lock";
		super.initialize();
	}

	@Override
	public void reset() {
		super.reset();
		_joinedElements.clear();
	}
	
	@Override
	public boolean canMoveToMe(TileSprite sprite, int moveDirection) {
		if (sprite.type == Type.PLAYER) {
			if (_game.gameData._inventory.hasInventory(type)) {
				return true;
			}
		}
		return false;
	}

	@Override
	public boolean stepToTiles(TileSprite sprite, boolean dontKill) {
		if (sprite.type == Type.PLAYER) {
			if (_game.gameData._inventory.useInventory(type)) {
				Sounds.play(Sounds.click);
				kill();
				stoodon = sprite;
				return true;
			}
			Sounds.play(Sounds.oof);
		}
		return false;
	}

}
