package org.porteousclan.chipsretro.game.elements.tiles;

import org.porteousclan.chipsretro.DirectedGame;
import org.porteousclan.chipsretro.game.data.Sounds;
import org.porteousclan.chipsretro.game.data.Type;
import org.porteousclan.chipsretro.game.elements.TileSprite;

import java.util.ArrayList;

public class Socket extends TileSprite {

	public Socket(DirectedGame game, int x, int y, int _type, int layer, int direction) {
		super(null, game, x, y, direction);

		_layer = layer; //clone machine does not follow this
		_originalType = type = _type;
		_joinedElements = new ArrayList<JoinedTile>();
		initialize();

	}

	@Override
	protected void initialize() {
		animNorth = "socket";
		animSouth = "socket";
		animEast = "socket";
		animWest = "socket";
		super.initialize();
	}

	@Override
	public void reset() {
		super.reset();
		_joinedElements.clear();
	}
	
	@Override
	public boolean canMoveFrom(TileSprite sprite, int moveDirection) {
        //IF adding back please note why and use a condition as true is valid in some cases
//		if (sprite.type == Type.PLAYER) {
//			if (_game.gameData.chips <= 0) {
//				return true;
//			}
//			return false;
//		}
		return true;
	}
	
	@Override
	public boolean canMoveToMe(TileSprite sprite, int moveDirection) {
		if ((_game.gameData.chips <= 0) && (sprite.type == Type.PLAYER)) {
			return true;
		}
		return false;
	}


	@Override
	public boolean stepToTiles(TileSprite sprite, boolean dontKill) {
		if ((_game.gameData.chips <= 0) && (sprite.type == Type.PLAYER)) {
			stoodon = sprite;
			return true;
		}
		if (sprite.type == Type.PLAYER)
			Sounds.play(Sounds.oof);
		return false;
	}
	
	@Override
	public boolean stoodOn(TileSprite sprite, boolean onORoff) {
		if ((_game.gameData.chips <= 0) && (sprite.type == Type.PLAYER)) {
			Sounds.play(Sounds.chack);
			kill();
		}
		return true;
	}

}
