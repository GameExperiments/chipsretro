package org.porteousclan.chipsretro.game.elements.tiles;

import org.porteousclan.chipsretro.DirectedGame;
import org.porteousclan.chipsretro.game.data.GameData;
import org.porteousclan.chipsretro.game.data.Sounds;
import org.porteousclan.chipsretro.game.data.Type;
import org.porteousclan.chipsretro.game.elements.TileSprite;

import java.util.ArrayList;

public class Water extends TileSprite {
	

	public Water(DirectedGame game, int x, int y, int _type, int layer, int direction) {
		super(null, game, x, y, direction);

		_layer = layer;
		_originalType = type = _type;
		_joinedElements = new ArrayList<JoinedTile>();
		initialize();

	}

	@Override
	protected void initialize() {
		animNorth = "water";
		animSouth = "water";
		animEast = "water";
		animWest = "water";
		super.initialize();
	}

	@Override
	public void reset() {
		super.reset();
		_joinedElements.clear();
	}
	
	@Override
	public boolean canMoveToMe(TileSprite sprite, int moveDirection) {
			return true;
	}

	@Override
	public boolean stepToTiles(TileSprite sprite, boolean dontKill) {

		if ( sprite.type == Type.ICEBLOCK) {
			if (!dontKill) {
                sprite.kill();
            }

            TileSprite sprite2 = reuseSprite(Type.ICE);
            if (sprite2 == null) {
                _game.gameData._elements.add(new Ice(_game, this.currentGridX, this.currentGridY, Type.ICE, this._layer, GameData.NONE, false));
            } else {
                _game.gameData._elements.add(sprite2);
            }

            Sounds.play(Sounds.splash);
			this.kill();
		}

		if (sprite.type == Type.BLOCK) {
            if (!dontKill) {
                sprite.kill(); //kill block
            }
            TileSprite sprite2 = reuseSprite(Type.DIRT);
            if (sprite2 == null)
                _game.gameData._elements.add(new Dirt(_game, this.currentGridX, this.currentGridY, Type.DIRT, this._layer, GameData.NONE));

            Sounds.play(Sounds.splash);
			this.kill();
		}
		
		if(!dontKill &&  sprite.type != Type.PLAYER && sprite.type != Type.GLIDER &&  sprite.type != Type.BLOCK && sprite.type != Type.ICEBLOCK) {
			sprite.kill(type);
			Sounds.play(Sounds.splash);
		}
		stoodon = sprite;
		return true;
	}
	@Override
	public boolean stoodOn(TileSprite sprite, boolean onORoff) {
		if (sprite.type == Type.PLAYER && _game.gameData._inventory.hasInventory(this.type)) {
			Sounds.play(Sounds.plip);
			_game.gameData._player.isSwimming = true;
			return true;
		}
//		if (sprite.type == Type.GLIDER) {
//			return true;
//		}
		if (sprite.type == Type.PLAYER) {
			Sounds.play(Sounds.splash);
			sprite.kill(type);
		}

		return true; 
	}	

}
