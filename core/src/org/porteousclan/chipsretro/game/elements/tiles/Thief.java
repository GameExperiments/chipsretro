package org.porteousclan.chipsretro.game.elements.tiles;

import org.porteousclan.chipsretro.DirectedGame;
import org.porteousclan.chipsretro.game.data.Sounds;
import org.porteousclan.chipsretro.game.data.Type;
import org.porteousclan.chipsretro.game.elements.TileSprite;

import java.util.ArrayList;

public class Thief extends TileSprite {
	

	public Thief(DirectedGame game, int x, int y, int _type, int layer, int direction) {
		super(null, game, x, y, direction);

		_layer = layer;
		_originalType = type = _type;
		_joinedElements = new ArrayList<JoinedTile>();
		initialize();

	}

	@Override
	protected void initialize() {
		animNorth = "thief";
		animSouth = "thief";
		animEast = "thief";
		animWest = "thief";
		super.initialize();
	}

	public void reset() {
		super.reset();
		_joinedElements.clear();
	}
	
	@Override
	public boolean canMoveToMe(TileSprite sprite, int moveDirection) {
		if (sprite.type == Type.PLAYER) {
			return true;
		}
		return false;
	}
	@Override
	public boolean stoodOn(TileSprite sprite, boolean onORoff) {
		if (sprite.type == Type.PLAYER) {
			_game.gameData._inventory.addInventory(type);
			Sounds.play(Sounds.thief);
		}
		return true; 
	}	
	
	@Override
	public boolean stepToTiles(TileSprite sprite, boolean dontKill) {
		stoodon = sprite;
		return true;
	}

}
