package org.porteousclan.chipsretro.game.elements.tiles;

import org.porteousclan.chipsretro.DirectedGame;
import org.porteousclan.chipsretro.game.data.ImageCache;
import org.porteousclan.chipsretro.game.data.Sounds;
import org.porteousclan.chipsretro.game.data.Type;
import org.porteousclan.chipsretro.game.elements.TileSprite;

import java.util.ArrayList;

public class HiddenWall extends TileSprite {


	public HiddenWall(String skinName, int idx, DirectedGame game, int x, int y, int _type, int layer, int direction) {
		super(null, game, x, y, direction);
		_layer = layer;
		_originalType = type = _type;
		_joinedElements = new ArrayList<JoinedTile>();

		if (skinName != "")
			setSkin(ImageCache.getFrame(skinName,idx));

		initialize();

	}
	public HiddenWall(DirectedGame game, int x, int y, int _type, int layer, int direction) {
		this("",-1,game,x,y,_type,layer,direction);
	}
	
	@Override
	public void reset() {
		super.reset();
		_joinedElements.clear();
	}
	@Override
	protected void initialize() {
                animNorth = "wall";
                animEast = "wall";
                animSouth = "wall";
                animWest = "wall";

		super.initialize();
		hide();
	}

	@Override
	public boolean canMoveToMe(TileSprite sprite, int moveDirection) {
		if (sprite.type == Type.PLAYER) {
			return true;				//will just play oof if not I hope
		}
		return false;
	}
	
	@Override
	public boolean stepToTiles(TileSprite sprite, boolean dontKill) {
		if (sprite.type == Type.PLAYER) {
			Sounds.play(Sounds.oof);
			show();

		}
		return false;
	}

	@Override
	public boolean stepOffTiles(TileSprite sprite, boolean clearAhead) {
		return false;
	}

}
