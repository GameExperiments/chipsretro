package org.porteousclan.chipsretro.game.elements.tiles;

import org.porteousclan.chipsretro.DirectedGame;
import org.porteousclan.chipsretro.game.data.Sounds;
import org.porteousclan.chipsretro.game.data.Type;
import org.porteousclan.chipsretro.game.elements.TileSprite;

public class Gravel extends TileSprite {
	

	public Gravel(DirectedGame game, int x, int y, int _type, int layer, int direction) {
		super(null, game, x, y, direction);

		_layer = layer;
		_originalType = type = _type;
		initialize();

	}

	@Override
	protected void initialize() {
		animNorth = "gravel";
		animSouth = "gravel";
		animEast = "gravel";
		animWest = "gravel";
		super.initialize();
	}

	@Override
	public void reset() {
		super.reset();
		_joinedElements.clear();
	}
	
	@Override
	public boolean canMoveToMe(TileSprite sprite, int moveDirection) {
		if ((sprite.type == Type.PLAYER) || (sprite.type == Type.BLOCK) || (sprite.type == Type.CLONEBLOCK) || (sprite.type == Type.ICEBLOCK) ) {
			return true;
		}
		return false;
	}

	@Override
	public boolean stoodOn(TileSprite sprite, boolean onORoff) {
		Sounds.play(Sounds.whisk);
		return true; 
	}	

	@Override
	public boolean stepToTiles(TileSprite sprite, boolean dontKill) {
		if (sprite.type == Type.PLAYER || sprite.type == Type.BLOCK  || sprite.type == Type.ICEBLOCK || sprite.type == Type.CLONEBLOCK) {
//			sprite.goAhead();
			stoodon = sprite;
			return true;
		}
		return false;
	}

}
