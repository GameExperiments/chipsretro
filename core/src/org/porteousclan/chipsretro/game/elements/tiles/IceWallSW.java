package org.porteousclan.chipsretro.game.elements.tiles;

import com.badlogic.gdx.Gdx;

import org.porteousclan.chipsretro.DirectedGame;
import org.porteousclan.chipsretro.game.data.GameData;
import org.porteousclan.chipsretro.game.data.Sounds;
import org.porteousclan.chipsretro.game.data.Type;
import org.porteousclan.chipsretro.game.elements.TileSprite;

import java.util.ArrayList;

public class IceWallSW extends TileSprite {

	public IceWallSW(DirectedGame game, int x, int y, int _type, int layer, int direction) {
		super(null, game, x, y, direction);

		_layer = layer;
		_originalType = type = _type;
		_joinedElements = new ArrayList<JoinedTile>();
		initialize();

	}

	@Override
	protected void initialize() {
		animNorth = "ice_wall_sw";
		animSouth = "ice_wall_sw";
		animEast = "ice_wall_sw";
		animWest = "ice_wall_sw";
		super.initialize();
	}

	@Override
	public void update(float dt) {
		super.update(dt);
	}

	@Override
	public void place() {
		super.place();
	}

	@Override
	public void reset() {
		super.reset();
		_joinedElements.clear();
	}

	@Override
	public boolean canMoveToMe(TileSprite sprite, int moveDirection) {
		if (sprite.direction == GameData.SOUTH) {
			return true;
		}
		if (sprite.direction == GameData.WEST) {
			return true;
		}
		return false;
	}

	@Override
	public boolean stoodOn(TileSprite sprite, boolean onORoff) {
		if (sprite.type == Type.PLAYER && _game.gameData._inventory.hasInventory(this.type)) {
			Sounds.play(Sounds.skate);
		}
		return true; 
	}	

	@Override
	public boolean stepToTiles(TileSprite sprite, boolean dontKill) {
		if ((sprite.direction != GameData.NORTH)
				&& (sprite.direction != GameData.EAST)) {
			stoodon = sprite;
			return true;
		}
		return false;
	}

    @Override
    public boolean stepOffTiles(TileSprite sprite, boolean clearAhead) {
        if (!clearAhead)
            sprite.lastdirection = sprite.direction = sprite.getReversed(sprite.direction);
        return true;
    }

    @Override
	public boolean slippery(TileSprite sprite) {
        if (sprite.type == Type.PLAYER && _game.gameData._inventory.hasInventory(this.type))
            return false;

        sprite.direction = sprite.lastdirection;

        if (stoodon == sprite && !sprite.moving) {
            if(!_game.gameData.msStyleMoveConditions)
                sprite.speed = sprite.defaultSpeed * sprite.speedMultiplier;
            else
                sprite.speed = defaultSpeed * speedMultiplier;
				if (sprite.direction == GameData.SOUTH) {
                    sprite.lastdirection = GameData.EAST;
                    sprite.direction = GameData.EAST;
                    sprite.slide(GameData.EAST, type);
                }
				else {
					if (sprite.direction == GameData.WEST) {
                        sprite.lastdirection = GameData.NORTH;
                        sprite.direction = GameData.NORTH;
                        sprite.slide(GameData.NORTH, type);
                    }
				}
				//return true;
		}
		//return false;
        return true;
	}

    @Override
    public boolean canMoveFrom(TileSprite sprite, int moveDirection) {
        if (_game.gameData.msStyleMoveConditions && stoodon != sprite && sprite.type != Type.PLAYER) {
            Gdx.app.log("ic wall sw ", "ignore block");
            return true; //ignor iceblock
        }
        stoodon = sprite;
        if (sprite.type == Type.PLAYER) {
            if (_game.gameData._inventory.hasInventory(type)) {
                return true;
            }
        }

//        if (sprite.direction == GameData.SOUTH) {
//            sprite.direction = GameData.EAST;
//        }
//        if (sprite.direction == GameData.WEST) {
//            sprite.direction = GameData.NORTH;
//        }
//        return true;
        if (sprite.direction == GameData.EAST || sprite.direction == GameData.NORTH)
            return true;
        return false;

    }

    @Override
    public int yourDirectionAffected(TileSprite sprite, int direction) {
        if ((sprite.type == Type.PLAYER && _game.gameData._inventory.hasInventory(this.type))
                || (sprite.type == Type.BLOCK) || (sprite.type == Type.ICEBLOCK) || (sprite.type == Type.CLONEBLOCK))
            return direction;
        if (this.stoodon != sprite) return direction; //NOTE: ice under dirt fix.
        if (sprite.direction == GameData.SOUTH) {
            return GameData.EAST;
        }
        if (sprite.direction == GameData.WEST) {
            return GameData.NORTH;
        }
        return sprite.lastdirection;
    }
}
