package org.porteousclan.chipsretro.game.elements.tiles;

import org.porteousclan.chipsretro.DirectedGame;
import org.porteousclan.chipsretro.game.data.Sounds;
import org.porteousclan.chipsretro.game.data.Type;
import org.porteousclan.chipsretro.game.elements.TileSprite;

import java.util.ArrayList;

public class Ice extends TileSprite {
	boolean resetEnabled = true;

	//from water
	public Ice(DirectedGame game, int x, int y, int _type, int layer, int direction, boolean resetEnabled) {
		this(game,x,y,_type,layer,direction);
		this.resetEnabled = resetEnabled;
	}

	public Ice(DirectedGame game, int x, int y, int _type, int layer, int direction) {
		super(null, game, x, y, direction);
		_layer = layer; //clone machine does not follow this
		_originalType = type = _type;
		_joinedElements = new ArrayList<JoinedTile>();
		initialize();
	}

	@Override
	protected void initialize() {
		animNorth = "ice";
		animSouth = "ice";
		animEast = "ice";
		animWest = "ice";
		super.initialize();
	}

	@Override
	public void reset() {
		super.reset();
		active = resetEnabled;
		_joinedElements.clear();
	}

	@Override
	public boolean canMoveFrom(TileSprite sprite, int moveDirection) {
		stoodon = sprite; // this affects bounce back ( but not direction ... so much .. ok not tested yet )
		return true;
	}

	public boolean canMoveToMe(TileSprite sprite, int moveDirection) {
		return true;
	}
	
	@Override
	public boolean stoodOn(TileSprite sprite, boolean onORoff) {
		if (sprite.type == Type.PLAYER && _game.gameData._inventory.hasInventory(this.type)) {
			Sounds.play(Sounds.skate);
		}
		return true;
	}

	@Override
	public boolean stepToTiles(TileSprite sprite, boolean dontKill) {
		direction = sprite.direction;
		stoodon = sprite;
		return true;				
	}

	@Override
	public boolean stepOffTiles(TileSprite sprite, boolean clearAhead) {
		if (!clearAhead)
            sprite.lastdirection = sprite.direction = sprite.getReversed(sprite.direction);
		return true;
	}

	@Override
	public boolean slippery(TileSprite sprite) {
        if (sprite.type == Type.PLAYER && _game.gameData._inventory.hasInventory(this.type))
            return false;
		if (stoodon == sprite && !sprite.moving ) {
            sprite.slide(sprite.direction, type);
            if(!_game.gameData.msStyleMoveConditions)
                sprite.speed = sprite.defaultSpeed * sprite.speedMultiplier;
            else
                sprite.speed = defaultSpeed * speedMultiplier;
        }
		return true;
	}

    @Override
    public int yourDirectionAffected(TileSprite sprite, int direction) {
        if ((sprite.type == Type.PLAYER && _game.gameData._inventory.hasInventory(this.type))
                || (sprite.type == Type.BLOCK) || (sprite.type == Type.ICEBLOCK) || (sprite.type == Type.CLONEBLOCK))
            return direction;
        if (this.stoodon != sprite) return direction; //NOTE: ice under dirt fix.
        //put bounce back off wall here if make more sense than where it is.
        return sprite.lastdirection;
    }

}
