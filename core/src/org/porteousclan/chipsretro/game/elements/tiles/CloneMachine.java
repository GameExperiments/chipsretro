package org.porteousclan.chipsretro.game.elements.tiles;

import org.porteousclan.chipsretro.DirectedGame;
import org.porteousclan.chipsretro.game.data.DirectionStack;
import org.porteousclan.chipsretro.game.data.GameData;
import org.porteousclan.chipsretro.game.data.Type;
import org.porteousclan.chipsretro.game.elements.Block;
import org.porteousclan.chipsretro.game.elements.IceBlock;
import org.porteousclan.chipsretro.game.elements.TileSprite;
import org.porteousclan.chipsretro.game.elements.creature.Blob;
import org.porteousclan.chipsretro.game.elements.creature.Bug;
import org.porteousclan.chipsretro.game.elements.creature.FireBall;
import org.porteousclan.chipsretro.game.elements.creature.Glider;
import org.porteousclan.chipsretro.game.elements.creature.Paramecium;
import org.porteousclan.chipsretro.game.elements.creature.PinkBall;
import org.porteousclan.chipsretro.game.elements.creature.Tank;
import org.porteousclan.chipsretro.game.elements.creature.Teeth;
import org.porteousclan.chipsretro.game.elements.creature.Walker;

import java.util.ArrayList;

public class CloneMachine extends TileSprite {
    //NOTE: MS clone will use the last creature (except teeth, bugs and paramecium) in the list as a boss
	public static int totalClones = 250;

	public CloneMachine(DirectedGame game, int x, int y, int _type, int layer, int direction) {
		super(null, game, x, y, direction);
		_layer = layer;
		_originalType = type = _type;
		_joinedElements = new ArrayList<JoinedTile>();
		initialize();
        wired = false;
        if (_game.gameData.lynxStyleMoveConditions) {
            if (this.direction == GameData.NONE)
                this.direction = GameData.NORTH;
        }

    }

	@Override
	public void reset() {
		super.reset();
		_joinedElements.clear();
        wired = false;
	}
	
	protected void initialize() {
		animNorth = "clone_machine";
		animSouth = "clone_machine";
		animEast = "clone_machine";
		animWest = "clone_machine";
		super.initialize();

		CloneMachine.totalClones = 250;
		preferredmoves[0] = GameData.RIGHT;
		preferredmoves[1] = GameData.FWD;
		preferredmoves[2] = GameData.LEFT;
		preferredmoves[3] = GameData.BACK;
		preferredmoves[4] = GameData.NONE;
	}

    @Override
    public boolean canMoveToMe(TileSprite sprite, int moveDirection) {
        if (sprite.type == Type.PLAYER) {
            return true;
        }
        return false;
    }

    @Override
    public boolean stepToTiles(TileSprite sprite, boolean dontKill) {
        if (sprite.type == Type.PLAYER) {
            if (!wired) {
                direction = sprite.direction;
                trigger(true);
            }
        }
        return super.stepToTiles(sprite, dontKill);
    }


    @Override
	public void trigger(boolean release) {
        //Under MS, blobs, bugs, paramecia and teeth can exit beartraps and clone machines in a new direction
        //don't clone if way is blocked
        if(_game.gameData.lynxStyleMoveConditions && checkMove.obstructedMoveTo(this, this.direction)) {
            return;
        }
		if (release) {
			boolean success = false;
			int i, len;			
			TileSprite sprite;
			sprite = null;
			len = _joinedElements.size();
//			int randDirection = MathUtils.random(0, 3) -1;
			
			switch(this.direction) {
			case GameData.NORTH:
				_restFrame = TEXTURE_0;
				break;
			case GameData.EAST:
				_restFrame = TEXTURE_1;
				break;
			case GameData.SOUTH:
				_restFrame = TEXTURE_2;
				break;
			case GameData.WEST:
				_restFrame = TEXTURE_3;
				break;
			default:
				_restFrame = TEXTURE_2;
				break;
			}
			setSkin(_restFrame);

//			//TODO: change sprite to not active until path ahead is clear
//			sprite = reuseJoinedSprite();
//			if (sprite != null) {
//				sprite.direction = this.direction;
//				sprite.lastdirection = this.direction;
//				if (cloneType == Type.BLOCK || cloneType == Type.CLONEBLOCK || cloneType == Type.ICEBLOCK ) {
//                    stoodon = sprite;
//                    sprite.moving = false;
//                    sprite.direction = this.direction;
//                    sprite.moveDir(this.direction); //yeah should be move push
//				}
//				success = true;
//				return;
//			}



			if (!success && len < totalClones) {
				switch(cloneType) {
				case Type.BLOB:
					sprite = new Blob(_game, currentGridX, currentGridY, Type.BLOB, GameData.UPPER_LAYER, this.direction , false);
					sprite.setInMoveList();
					_game.gameData._elements.add(sprite);
					joinTiles(sprite, GameData.NONE, GameData.NONE);
					success = true;
					break;
				case Type.BLOCK:
				case Type.CLONEBLOCK:
					sprite = new Block(_game, currentGridX, currentGridY, Type.BLOCK, GameData.UPPER_LAYER, this.direction, false);
                    canMove = true;
					_game.gameData._elements.add(sprite);
					joinTiles(sprite, GameData.NONE, GameData.NONE);
                    stoodon = sprite;
                    sprite.moving = false;
                    sprite.direction = this.direction;
					sprite.moveDir(this.direction); //yeah should be move push
					success = true;
					break;
				case Type.ICEBLOCK:
					sprite = new IceBlock(_game, currentGridX, currentGridY, Type.ICEBLOCK, GameData.UPPER_LAYER, this.direction, false);
                    canMove = true;
					_game.gameData._elements.add(sprite);
					joinTiles(sprite, GameData.NONE, GameData.NONE);
//					sprite.lastdirection = this.direction;
                    stoodon = sprite;
					sprite.moveDir(this.direction);
					success = true;
					break;
				case Type.BUG:
					sprite = new Bug(_game, currentGridX, currentGridY, Type.BUG, GameData.UPPER_LAYER, this.direction, false);
					sprite.setInMoveList();
					_game.gameData._elements.add(sprite);
					joinTiles(sprite, GameData.NONE, GameData.NONE);
					success = true;
					break;
				case Type.FIREBALL:
					sprite = new FireBall(_game, currentGridX, currentGridY, Type.FIREBALL, GameData.UPPER_LAYER, this.direction, false);
					sprite.setInMoveList();
					_game.gameData._elements.add(sprite);
					joinTiles(sprite, GameData.NONE, GameData.NONE);
					success = true;
					break;
				case Type.GLIDER:
					sprite = new Glider(_game, currentGridX, currentGridY, Type.GLIDER, GameData.UPPER_LAYER, this.direction, false);
					sprite.setInMoveList();
					_game.gameData._elements.add(sprite);
					joinTiles(sprite, GameData.NONE, GameData.NONE);
					success = true;
					break;
				case Type.PARAMECIUM:
					sprite = new Paramecium(_game, currentGridX, currentGridY, Type.PARAMECIUM, GameData.UPPER_LAYER, this.direction, false);
					sprite.setInMoveList();
					_game.gameData._elements.add(sprite);
					joinTiles(sprite, GameData.NONE, GameData.NONE);
					success = true;
					break;
				case Type.PINKBALL:
					sprite = new PinkBall(_game, currentGridX, currentGridY, Type.PINKBALL, GameData.UPPER_LAYER, this.direction, false);
					sprite.setInMoveList();
					_game.gameData._elements.add(sprite);
					joinTiles(sprite, GameData.NONE, GameData.NONE);
					success = true;
					break;
				case Type.TANK:
					sprite = new Tank(_game, currentGridX, currentGridY, Type.TANK, GameData.UPPER_LAYER, this.direction, false);
					sprite.setInMoveList();
					_game.gameData._elements.add(sprite);
					joinTiles(sprite, GameData.NONE, GameData.NONE);
					//green buttons and togglewalls or bluebuttons and tanks
					TileSprite element;
					for (int ti = 0; ti < _game.gameData._elements.size(); ti++) {
						//element = null;
						element = DirectionStack.getElement(_game,ti);
						
						if (element.type == Type.BLUEBUTTON) {
							element.joinTiles(sprite, GameData.NONE, GameData.NONE);
							if (sprite.currentGridX == element.currentGridX && sprite.currentGridY == element.currentGridY) {
								element.stoodon = sprite;
							}
						}
						
					}
					
					success = true;
					break;
				case Type.TEETH:
					sprite = new Teeth(_game, currentGridX, currentGridY, Type.TEETH, GameData.UPPER_LAYER, this.direction, false);
					sprite.setInMoveList();
					_game.gameData._elements.add(sprite);
					joinTiles(sprite, GameData.NONE, GameData.NONE);
					success = true;
					break;
				case Type.WALKER:
					sprite = new Walker(_game, currentGridX, currentGridY, Type.WALKER, GameData.UPPER_LAYER, this.direction, false);
					sprite.setInMoveList();
					_game.gameData._elements.add(sprite);
					joinTiles(sprite, GameData.NONE, GameData.NONE);
					success = true;
					break;
				}

			}
			//need the actual sprite to get the results
            if(success && _game.gameData.msStyleMoveConditions) {
                if (sprite.type == Type.BLOB ) {
                    this.direction = sprite.getNextAvailable(this.direction);
                    sprite.lastdirection = sprite.direction = this.direction;
                }
                if(  sprite.type == Type.BUG || sprite.type == Type.PARAMECIUM || sprite.type == Type.TEETH) {
                    this.direction = GameData.NONE;
                    //if (sprite._creatureMoveIndex > 0) {
                        for (int x = _game.gameData._creatureMoveOrderList.size() - 1; x >= 0; x--) {
                            TileSprite spriteLastInMoveList = _game.gameData._creatureMoveOrderList.get(x);
                            if (spriteLastInMoveList.active && !(  spriteLastInMoveList.type == Type.PLAYER || spriteLastInMoveList.type == Type.BUG || spriteLastInMoveList.type == Type.PARAMECIUM || spriteLastInMoveList.type == Type.TEETH)) {
                                this.direction = spriteLastInMoveList.lastdirection;
                                break;
                            }
                        }
                    //}
                }
                if (this.direction == GameData.NONE || checkMove.obstructedMoveTo(sprite, this.direction)) {
                    sprite.kill(); //TODO: reexamine reuse tiles
                    success = false;
                }
            }
            if (success) totalClones = totalClones - 1;
        }

	}
	
	@Override
	public boolean stepOffTiles(TileSprite sprite, boolean clearAhead) {
		return true;
	}
	@Override
    public int yourDirectionAffected(TileSprite sprite, int direction) {

        int creatureDirection = this.direction;

        //MS  blobs, bugs, paramecia and teeth can exit in different direction
        if (!_game.gameData.lynxStyleMoveConditions && ( sprite.type == Type.BLOCK || sprite.type == Type.ICEBLOCK || sprite.type == Type.CLONEBLOCK))
            return sprite.direction;

        if (checkMove.obstructedMoveTo(sprite, creatureDirection)) {
            sprite.kill();
            return GameData.NONE;
        }
        return creatureDirection;
    }


}
