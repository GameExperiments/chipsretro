package org.porteousclan.chipsretro.game.elements;

import org.porteousclan.chipsretro.DirectedGame;
import org.porteousclan.chipsretro.game.data.DirectionStack;
import org.porteousclan.chipsretro.game.data.GameData;
import org.porteousclan.chipsretro.game.data.GroupType;
import org.porteousclan.chipsretro.game.data.ImageCache;
import org.porteousclan.chipsretro.game.data.Type;

import java.util.ArrayList;

public class CloneBlock extends TileSprite {
	private boolean expended = true;
	float deathTimer = 0f;

	public CloneBlock(DirectedGame game, int x, int y, int _type, int layer, int direction, boolean startActive) {
		super(null, game, x, y, direction);
		//_psudoType = _type;
		_initialActiveState = startActive;
		_layer = layer; //GameData.UPPER_LAYER;
		_originalType = type = _type;
		groupType = GroupType.BLOCK;
        initialize();
	}

	public CloneBlock(DirectedGame game, int x, int y, int _type, int layer, int direction) {
		this(game,x,y,_type,layer,direction,true);
	}

	@Override
	public void initialize() {
		type = _originalType;
		expended = true;

		transparency = true;
		//Mutated = false;
		canMove = true;

		_joinedElements = new ArrayList<JoinedTile>();
		
		if (TEXTURE_1 == null) {
				TEXTURE_0 = ImageCache.getFrame("clone_block_n", -1);
				TEXTURE_1 = ImageCache.getFrame("clone_block_e", -1);
				TEXTURE_2 = ImageCache.getFrame("clone_block_s", -1);
				TEXTURE_3 = ImageCache.getFrame("clone_block_w", -1);
				expended = false;
		}

		preferredmoves[0] = GameData.FWD;
		preferredmoves[1] = GameData.NONE;
		preferredmoves[2] = GameData.NONE;
		preferredmoves[3] = GameData.NONE;
		preferredmoves[4] = GameData.NONE;

		switch(direction) {
		case GameData.NORTH:
			_restFrame = TEXTURE_0;
			break;
		case GameData.EAST:
			_restFrame = TEXTURE_1;
			break;
		case GameData.SOUTH:
			_restFrame = TEXTURE_2;
			break;
		case GameData.WEST:
			_restFrame = TEXTURE_3;
			break;
		default:
			_restFrame = TEXTURE_2;
			break;
		}
		setSkin(_restFrame);
		show();
	}
	
		
	@Override
	public void update(float dt) {
		if (!moving) {
			preferredmoves[0] = GameData.FWD;
			preferredmoves[1] = GameData.NONE;
			preferredmoves[2] = GameData.NONE;
			preferredmoves[3] = GameData.NONE;
			preferredmoves[4] = GameData.NONE;
		}
		super.update(dt);
		if (expended) {
//			deathTimer += 20 * dt; //used to keep it but appears is wrong behaviour
//			if (deathTimer > 10 )
				this.kill();
		}
	}
	

	@Override
	public void place() {
		switch(direction) {
		case GameData.NORTH:
			_restFrame = TEXTURE_0;
			break;
		case GameData.EAST:
			_restFrame = TEXTURE_1;
			break;
		case GameData.SOUTH:
			_restFrame = TEXTURE_2;
			break;
		case GameData.WEST:
			_restFrame = TEXTURE_3;
			break;
		default:
			_restFrame = TEXTURE_2;
			break;
		}
		setSkin(_restFrame);
		super.place();
	}

	@Override
	public boolean canMoveToMe(TileSprite sprite, int moveDirection) {
		if (sprite.type == Type.PLAYER) {
			return true;
		}
		return false;
	}

	@Override
	public boolean stoodOn(TileSprite sprite, boolean onORoff) {
		if (sprite.type == Type.PLAYER) {
//			Sounds.play(Sounds.death);
			sprite.kill(type);
			kill();
		}
		return true; 
	}

	@Override
	public boolean stepToTiles(TileSprite sprite, boolean dontKill) {
//		if (sprite.type == Type.PLAYER) {
//			if (!expended) {
//                trigger(false);
//			}
//		}
		return false;
	}
	
	@Override
	public void trigger(boolean release) {
	    if (!expended) { // safeguard
            int id1;
            TileSprite sprite1 = null;

            id1 = DirectionStack.getLowerId(_game, this, GameData.NONE);
            if(id1 > Type.EMPTY) {
                sprite1 = DirectionStack.getElement(_game, id1);
            }

			TileSprite sprite2 = reuseSprite(Type.BLOCK);
			if (sprite2 == null) {
				sprite2 = new Block(_game, this.currentGridX, this.currentGridY, Type.BLOCK, GameData.UPPER_LAYER, this.direction, false);
				_game.gameData._elements.add(sprite2);
			}
            if (sprite1 != null) {
                if( sprite1.type != Type.CLONEMACHINE) {
                    if (release)
                        expended = true;
                    else {
                        sprite2.nextGridX = sprite2.currentGridX = sprite1.currentGridX;
                        sprite2.nextGridY = sprite2.currentGridY = sprite1.currentGridY;
                    }
                }
            }
			sprite2.moveDir(this.direction);
//			sprite2= null;
	    }
	}

	@Override
	public boolean stepOffTiles(TileSprite sprite, boolean clearAhead) {
		return true;
	}
	
	@Override
	public boolean isCloneable() {return true;}
    public int yourDirectionAffected(TileSprite sprite, int tmpdirection) {
        return this.direction;
    }

}
