package org.porteousclan.chipsretro.game.elements;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector3;

import org.porteousclan.chipsretro.DirectedGame;
import org.porteousclan.chipsretro.game.data.DirectionStack;
import org.porteousclan.chipsretro.game.data.GameData;
import org.porteousclan.chipsretro.game.data.GroupType;
import org.porteousclan.chipsretro.game.data.ImageCache;
import org.porteousclan.chipsretro.game.data.Type;

import java.util.ArrayList;
import java.util.List;

import static org.porteousclan.chipsretro.game.data.DirectionStack.isInDirectionStack;
import static org.porteousclan.chipsretro.game.data.DirectionStack.pushVisitorOnStack;

public class TileSprite extends Sprite {
	public static final float VIEWPORT_TILE_WIDTH = 1.0f;
	public static final float VIEWPORT_WIDTH = VIEWPORT_TILE_WIDTH  * 18.0f;
	public static final float VIEWPORT_HEIGHT = VIEWPORT_TILE_WIDTH * 10.0f;
    public CheckMove checkMove;
	public TextureRegion _restFrame;

	public TextureRegion TEXTURE_0;
	public TextureRegion TEXTURE_1;
	public TextureRegion TEXTURE_2;
	public TextureRegion TEXTURE_3;
	public TextureRegion TEXTURE_4;

	public int _index;
    public int _creatureMoveIndex=0;

//speed algorithm change 2017-04-17
	public float defaultSpeed = 4;
    public float speedMultiplier = 1.5f;
	public float speed = defaultSpeed; // speed is usually 1* or 2* defaultSpeed
	protected float _moveInterval = 0.0f;
	protected float _move = 1;
    protected float _frameTime = _move /4;

    public int directionIndex=0;
    public boolean obstructed = false;
	public boolean moving = false;
	public boolean stopping = false;
	public TileSprite stoodon = null;
	//***NEW
	public boolean goalreached = false;
	public boolean isFirstMove = true;
    public boolean evenMove=false;
    public int lastMoveForcedBy = GameData.NONE;

    public int type;
	public int _layer;
	protected int _originalType;

	public int currentGridX, currentGridY;
	public int nextGridX;
	public int nextGridY;

    public int drawDirection;
	public int direction;
	public int lastdirection = GameData.NONE;
	protected int moves[];
	protected int movespush[];
	public boolean canMove;
	public boolean transparency = false;
    public boolean wired; // used for clonemachine and no red button
    private int turnAvailableIndex = 0;
	
	public int preferredmoves[];
	public TileSprite teleloop = null;

	private Vector3 _startPoint;
	//public int _psudoType;
    public int groupType;
	public boolean _initialActiveState = true;
	//public boolean Mutated = false;


	public class JoinedTile {
		public TileSprite joinedSprite;
		public int directionIncoming = GameData.NONE;
		public int directionOut = GameData.NONE;
		public boolean pressed = false;
	}
	
	public List<JoinedTile> _joinedElements;
	protected int cloneType = Type.EMPTY;
	public Boolean isJoined = false;

	protected DirectedGame _game;
	public boolean active;
	public boolean visible;
	public int animateType = Type.EMPTY;
	protected String animNorth, animSouth, animEast, animWest;

	public TileSprite(String skinName, DirectedGame game, int x, int y, int direction) {
		super();

//        if (!game.gameData.msStyleMovement) {
//            defaultSpeed = 5;
//            speed = defaultSpeed; // speed is usually 1* or 2* defaultSpeed
//        } else {
//            defaultSpeed = 4;
//            speed = defaultSpeed; // speed is usually 1* or 2* defaultSpeed
//        }
        if (game.gameData.slowSpeed) {
            speed = defaultSpeed = defaultSpeed - 1;
        }

        checkMove = new CheckMove(game,this);
		_game = game;
        groupType = GroupType.TILE; //reset by other types later

		if (skinName != null)
			super.setRegion(ImageCache.getTexture(skinName));

		moves = new int[5];
		movespush = new int[5];
		preferredmoves = new int[5];
		super.setBounds(x, y, VIEWPORT_TILE_WIDTH, VIEWPORT_TILE_WIDTH);
		_startPoint = new Vector3();
		_joinedElements = new ArrayList<JoinedTile>();

		_startPoint.x = x;
		_startPoint.y = y;
		_startPoint.z = direction;
		this.direction = lastdirection = direction;
        drawDirection = direction;

		currentGridX = nextGridX = (int) _startPoint.x;
		currentGridY = nextGridY = (int) _startPoint.y;

		stoodon = null;
		for (int i = 0; i < 5; i++) {
			moves[i] = GameData.NONE;
		}
		for (int i = 0; i < 5; i++) {
			movespush[i] = GameData.NONE;
		}
		transparency = false;
		//Mutated = false;
		canMove = false;
		moving = false;
		stopping = false;
		type = _originalType;
		active = true;

		show();
	}

    public void chooseSkin(int direction) {
        switch(direction) {
            case GameData.NORTH:
                _restFrame = TEXTURE_0;
                break;
            case GameData.EAST:
                _restFrame = TEXTURE_1;
                break;
            case GameData.SOUTH:
                _restFrame = TEXTURE_2;
                break;
            case GameData.WEST:
                _restFrame = TEXTURE_3;
                break;
            default:
                if (TEXTURE_4 != null)
                    _restFrame = TEXTURE_4;
                else
                    _restFrame = TEXTURE_2;
                break;
        }
        setSkin(_restFrame);
    }

	public void setSkin (String skinName, int skinIndex) {
		setSkin (ImageCache.getFrame(skinName, skinIndex));
	}

	public void setSkin (TextureRegion texture) {
        if (texture != null) {
            setRegion(texture);
            visible = true;
        }
	}

	protected void initialize() {
		transparency = false;
		canMove = false;

		if (TEXTURE_1 == null) {
			TEXTURE_0 = ImageCache.getFrame(animNorth, -1);
			TEXTURE_1 = ImageCache.getFrame(animEast, -1);
			TEXTURE_2 = ImageCache.getFrame(animSouth, -1);
			TEXTURE_3 = ImageCache.getFrame(animWest, -1);
		}

		preferredmoves[0] = GameData.NONE;
		preferredmoves[1] = GameData.NONE;
		preferredmoves[2] = GameData.NONE;
		preferredmoves[3] = GameData.NONE;
		preferredmoves[4] = GameData.NONE;

        chooseSkin(drawDirection);
        if (type == Type.PLAYER && _game.gameData.msStyleMovement) //starts on even
            evenMove = true;

		show();
	}

	public void reset() {
		initialize();
		place();
		active = _initialActiveState;
		this.isFirstMove = true;
        _moveInterval = 0.0f;
        turnAvailableIndex = 0;
        _creatureMoveIndex=0;
	}

	public void setInMoveList() {
        canMove = true;
        _game.gameData._creatureMoveOrderList.add(this);
        _creatureMoveIndex =_game.gameData._creatureMoveOrderList.size() - 1;
	}

    public void moveWanted() {
    }

    /** sync grid and xy */ //tank overrides
	public void update(float dt) {
        if (!_game.gameData.msStyleMovement) {
            moveWanted();
        }

		TileSprite sprite2=null;
		setX(currentGridX);
		setY(currentGridY);

		if (stoodon != null) {
			if (!isInDirectionStack(_game, this, GameData.NONE, currentGridX, currentGridY)) {
				stoodon = null;
			}
		}

		if (stopping) {
			stopping = false;
		}

		if (moving) {
			if (_move < _moveInterval) {
				moving = false;
				stopping = true;
				_moveInterval = _moveInterval - _move;  // 0.0f; //maintains average, i hope
			}
            _moveInterval += speed * dt;
		}
	}

	public void place() {
		currentGridX = nextGridX;
		currentGridY = nextGridY;
		setX(currentGridX);
		setY(currentGridY);
	}

	public void show () { visible = true; }
	public void hide () { visible = false; }

    public void draw (SpriteBatch spriteBatch) {
		setFlip(false,true);
		//Gdx.app.log("type draw", this.type + " ");
        if (visible) {
            super.draw(spriteBatch);
        }
    }

	public void kill(int type) {
		//lastdirection = GameData.NONE;
		animateType = type;
		stopping = false;
		kill();
	}
	public void kill() { active = false; }

	public void moveDir(int _direction) {
		if (_game.gameData._player.hadFirstMove == false) {
			clearMove();
			clearPush();
			return;
		}
		if (movespush[0] == GameData.NONE)
			moves[0] = _direction;
	}

	public void clearMove() {
		for (int i = 0; i < 5; i++) {
			moves[i] = GameData.NONE;
		}
	}

	public int queryNextMove() {
		return moves[0];
	}
	public void clearPush() {
		for (int i = 0; i < 5; i++) {
			movespush[i] = GameData.NONE;
		}
	}

	public void movePush(int _direction) {
		//forcetype = type;
		
		if (_game.gameData._player.hadFirstMove == false) {
			clearMove();
			clearPush();
			return;
		}

		if (_direction == GameData.NONE) {
			movespush[0] = lastdirection;
		} else {
			movespush[0] = _direction;
		}
	}

	public void slide(int _direction, int type) {
		if (this.type == Type.PLAYER) {
			if (_game.gameData._inventory.hasInventory(type)) {
				return;
			}
		}
		this.direction = _direction;
		
		if (!moving) {
			movePush(_direction);
		}
	}

	public void toggleEvenMove() {
        if (this.type == Type.PLAYER)
            evenMove = !evenMove;
    }

	public int movePop() {
        int forceType = GameData.NONE;
        int ret = GameData.NONE;
        TileSprite sprite2=null;

		if (!moving) {
            speed = defaultSpeed;
            if (DirectionStack.getTopUpperLowerId(_game, this, GameData.NONE) > Type.EMPTY) {
                sprite2 = DirectionStack.getElement(_game,DirectionStack.getTopUpperLowerId(_game, this, GameData.NONE));
            }
            if (sprite2 != null && sprite2.stoodon == this) {
                if (sprite2.slippery(this))
                    forceType = sprite2.type;
            }
            else
                forceType = GameData.NONE;
        }
        if (!moving) {
			switch (forceType)  {
			case Type.ICE:
			case Type.ICEWALL_NE:
			case Type.ICEWALL_NW:
			case Type.ICEWALL_SE:
			case Type.ICEWALL_SW:
                lastMoveForcedBy = Type.ICE;
				ret = movespush[0];
                evenMove = true;
                //toggleEvenMove();
				break;
			case Type.FORCEFLOOR:
			case Type.RANDOMFORCEFLOOR:
				for (int i = 1; i < 5; i++) {
					movespush[i] = GameData.NONE;
				}
				for (int i = 1; i < 5; i++) {
					moves[i] = GameData.NONE;
				} //fall through
                if (type == Type.PLAYER && evenMove && (moves[0] != GameData.NONE) && (moves[0] != movespush[0])) {
                    ret = moves[0];
                    toggleEvenMove();
                } else {
                    if (movespush[0] != GameData.NONE) {
                        ret = movespush[0];
                        lastMoveForcedBy = Type.FORCEFLOOR;
                        evenMove = true;
                    }
                }
                break;
            case Type.CLONEMACHINE:
            case Type.TELEPORT:
                    lastMoveForcedBy = Type.TELEPORT;
                    ret = movespush[0];
                    for (int i = 0; i < 5; i++) {
                        movespush[i] = GameData.NONE;
                    }
                    for (int i = 0; i < 5; i++) {
                        moves[i] = GameData.NONE;
                    } //fall through
                    break;
            default:
				ret = moves[0];
                lastMoveForcedBy = GameData.NONE;
				break;
			}

			//shift everything to the front for both queues
			for (int i = 0; i < 4; i++) {
				moves[i] = moves[i + 1];
			}
			for (int i = 0; i < 4; i++) {
				movespush[i] = movespush[i + 1];
			}
			moves[4] = GameData.NONE;	
			movespush[4] = GameData.NONE;
			
			if (ret != GameData.NONE ) {
				moving = true; // set moving true
			} else {
				ret = direction; 
			}
		}
		return ret;
	}

    public int getNextAvailable(int currentDirection) {

        if (!_game.gameData.msStyleMovement) {
            return getNextAvailableTurnBased(currentDirection);
        }
        int newDirection;
        for (int d = 0; d < 4; d++) {
            newDirection = getNextWanted(currentDirection, d);
            //if (this.isCreature() && that.isCreature())
            if (!checkMove.obstructedMoveFrom(this,newDirection) && !checkMove.obstructedMoveTo(this, newDirection)) {
                return newDirection;
            }
        }
        return GameData.NONE;
    }

    public int getNextAvailableTurnBased(int currentDirection) {
        int newDirection;
        if (turnAvailableIndex >= 4) turnAvailableIndex = 0;
//        int d = turnAvailableIndex;
//        for (; d < 4; d++) {
            newDirection = getNextWanted(lastdirection, turnAvailableIndex);
            turnAvailableIndex++;
            //if (this.isCreature() && that.isCreature())
            if (!checkMove.obstructedMoveFrom(this,newDirection) && !checkMove.obstructedMoveTo(this, newDirection)) {
                return newDirection;
            }
  //      }
        return GameData.NONE;
    }

    public int getNextWanted(int currentDirection, int index) {
        if (index > 4) index = 0; //also reset on succesful move
        int diri = currentDirection + preferredmoves[index];

        if (diri < GameData.NORTH)	diri += GameData.NONE;
        if (diri > GameData.WEST)	diri -= GameData.NONE;
        return diri;
    }

	public int getReversed(int direction) {
		int diri;
		diri = direction + 2;
		if (diri >= GameData.NONE) diri -= GameData.NONE;
		return diri; //directionAffected(diri);
	}

	/** called from an existing move usually
	 * set moving true and my next position - regardless */
//    public boolean goAhead() {
//        return goAhead(this.direction,currentGridX,currentGridY);
//    }

	public boolean goAhead(int dir, int gX, int gY) {
        turnAvailableIndex = 0;
        this.lastdirection = dir;
        this.drawDirection = dir;
        nextGridX = DirectionStack.getGridXinDirection(dir, gX);
        nextGridY = DirectionStack.getGridYinDirection(dir, gY);
		DirectionStack.clearTile(_layer, gX, gY);
		_layer = pushVisitorOnStack(nextGridX, nextGridY, _index);
        if (_layer > Type.BORDERS)
    		return true;
        return false;
	}

	public JoinedTile findJoined(int direction) {
		int len = _joinedElements.size();
		for (int i = 0; i < len; i++) {
			if (_joinedElements.get(i).directionIncoming == direction) {
				return _joinedElements.get(i);
			}
		}
		return null;
	}
	
	public void joinTiles(TileSprite sprite, int directionIn, int directionOut) {

		JoinedTile tile = new JoinedTile();
		tile.joinedSprite = sprite;
		
		_joinedElements.add(tile);
		this.isJoined = true;
		sprite.isJoined = true;
	}

	public boolean isItem() {
		return false;
	}

    public TileSprite reuseSprite(int typeWanted) {
        TileSprite sprite2 = null;
        for (int i = 0; i < _game.gameData._elements.size(); i++) {

            if (!DirectionStack.getElement(_game,i).active && DirectionStack.getElement(_game,i).type == typeWanted) {
                sprite2 = DirectionStack.getElement(_game,i);  //use GameData.getElement
                sprite2.nextGridX = sprite2.currentGridX = this.currentGridX;
                sprite2.nextGridY = sprite2.currentGridY = this.currentGridY;
                sprite2.active = true;
                break;
            }
        }
        return sprite2; //could return null
    }

    //clone machine only??
    public TileSprite reuseJoinedSprite() {
        TileSprite sprite = null;
        for (int i = 0; i < _joinedElements.size(); i++) {
            sprite = _joinedElements.get(i).joinedSprite;
            if (!sprite.active) {
                sprite.currentGridX = sprite.nextGridX = currentGridX;
                sprite.currentGridY = sprite.nextGridY = currentGridY;
                sprite.active = true;
//                sprite.direction = this.direction;
//                sprite.lastdirection = this.direction;
                break;
            }
            sprite = null;
        }
        return sprite;
    }

//DEFAULTS BUT NEEDED
	public int yourDirectionAffected(TileSprite sprite, int direction) {
		return direction;
	}

	/** can move from may change your direction  i.e. ice corner so test before */
	public boolean canMoveFrom(TileSprite sprite, int moveDirection) {
		return true;
	}

	public boolean canMoveToMe(TileSprite sprite, int moveDirection) {
		return false;
	}

	public boolean stoodOn(TileSprite sprite, boolean onORoff) {
		return true;
	}

	public void trigger(boolean release) {
	}

	public boolean stepOffTiles(TileSprite sprite, boolean clearAhead) {
		return false;
	}

	public boolean stepToTiles(TileSprite sprite, boolean dontKill) {
		return false;
	}

	public boolean slippery(TileSprite sprite) {
		return false;
	}


    public boolean isCloneable() {
		return false;
	}
	public boolean isCreature() {
		return false;
	}
    public boolean isRealChip() {
        return false;
    }
	public void setCloneType(int type) {
		cloneType = type;
	}


}
