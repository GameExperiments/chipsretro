package org.porteousclan.chipsretro.game.elements;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.utils.Align;

import org.porteousclan.chipsretro.DirectedGame;
import org.porteousclan.chipsretro.game.data.GameData;
import org.porteousclan.chipsretro.game.data.GroupType;
import org.porteousclan.chipsretro.game.data.ImageCache;
import org.porteousclan.chipsretro.game.data.Sounds;
import org.porteousclan.chipsretro.game.data.Type;

public class Player extends TileSprite {

	public boolean dead = false;
	public boolean textVisible = false;
	//private float text_timer=0;
	public boolean completed_level = false;
	public boolean isSwimming = false;
	public boolean hadFirstMove = false;
	public boolean canMoveToMe = true;
	
	private TextureRegion _chipSouth;
	private TextureRegion _chipNorth;
	private TextureRegion _chipEast;
	private TextureRegion _chipWest;
	private TextureRegion _chipSwimSouth;
	private TextureRegion _chipSwimNorth;
	private TextureRegion _chipSwimEast;
	private TextureRegion _chipSwimWest;
	private TextureRegion _restFrame;
	private TextureRegion _chipDrowned;
	private TextureRegion _chipBurned;
	private float elapsedTime = 0.01f;

	private boolean controllableChip = false;

	public Player(String skinName, DirectedGame game, int x, int y, int _type, int layer, int direction) {
		this(skinName,-1,game,x,y,_type,layer,direction);
	}
	public Player(String skinName, int idx, DirectedGame game, int x, int y, int _type, int layer, int direction) {
		super(null, game, x, y, direction);
		setSkin(skinName, idx);
		canMove = true;
		type = _type;
		_layer = layer;
//		transparency = true;
		initialize();
		speed = defaultSpeed = 5;

	}

	@Override
	protected void initialize() {

		//Mutated = false;
		canMove = false;

		preferredmoves[0] = GameData.NONE;
		preferredmoves[1] = GameData.NONE;
		preferredmoves[2] = GameData.NONE;
		preferredmoves[3] = GameData.NONE;
		preferredmoves[4] = GameData.NONE;

		if ( _restFrame == null) {
			_chipSouth = ImageCache.getTexture("chip_s");
			_chipNorth = ImageCache.getTexture("chip_n");
			_chipEast = ImageCache.getTexture("chip_e");
			_chipWest = ImageCache.getTexture("chip_w");

			_chipSwimSouth = ImageCache.getTexture("chip_swim_s");
			_chipSwimNorth = ImageCache.getTexture("chip_swim_n");
			_chipSwimEast = ImageCache.getTexture("chip_swim_e");
			_chipSwimWest = ImageCache.getTexture("chip_swim_w");
			_chipDrowned = ImageCache.getTexture("chip_drowned");
			_chipBurned = ImageCache.getFrame("chip_burn", 1);
			_restFrame = _chipSouth;
		}

		switch (type) {
			case Type.BURNEDCHIP:
				_restFrame = _chipBurned;
				canMoveToMe = false;
				break;
			case Type.DROWNEDCHIP:
				_restFrame = _chipDrowned;
				canMoveToMe = false;
				break;
			case Type.SWIMMINGCHIP:
				_restFrame = _chipSwimSouth;
				break;
			case Type.PLAYER:
				_restFrame = _chipSouth;
				break;
		}

	}

	@Override
	public void reset () {
		super.reset();
		animateType = Type.EMPTY; //no animation type
		elapsedTime = 0.01f;

		dead = false;
		stoodon = null;
		hadFirstMove = false;
		goalreached = false;
		if (isRealChip()) {
			isSwimming = false;
			_game.gameData.gameMode = DirectedGame.GAME_STATE_PLAY;
			direction = GameData.SOUTH;
		}
	}

    @Override
	public boolean isRealChip() {
		return controllableChip;
	}

	public void isRealChip(boolean value) {
        //if (!_game.gameData.lynxStyleMoveConditions)
        transparency = true; //real player is transparent
		controllableChip = value;
        if (value)
    		groupType = GroupType.PLAYER;
        else
            groupType = GroupType.TILE;
	}

	@Override
	public void update (float dt) {	

		if ((dead || goalreached) && _game.gameData.gameMode != DirectedGame.GAME_STATE_PAUSE ) {
			_game.gameData.gameMode = DirectedGame.GAME_STATE_ANIMATE;
			elapsedTime += dt;
			if (elapsedTime >= 1f ) {
                if (dead) _game.gameData.gsEvent = GameData.GameScreenEventMessage.PLAYERDIED;
				if (goalreached) _game.gameData.gsEvent = GameData.GameScreenEventMessage.LEVELDONE;
				elapsedTime = 0f;
				_game.gameData.gameMode = DirectedGame.GAME_STATE_PAUSE;
//                _game.gameData.gsEvent = GameData.GameScreenEventMessage.NORMAL;
            }
		}

        if (isRealChip()) {
			preferredmoves[0] = GameData.FWD;
            if (_game.gameData.gameMode == DirectedGame.GAME_STATE_ANIMATE) {
                //elapsedTime += Gdx.graphics.getDeltaTime();
                switch (animateType) {
                    case Type.WATER:
                        setSkin((TextureRegion) ImageCache.p_drownAnimation.getKeyFrame(elapsedTime, false));
                        break;
                    case Type.FIRE:
                        setSkin((TextureRegion) ImageCache.p_burnAnimation.getKeyFrame(elapsedTime, false));
                        break;
                    case Type.BOMB:
                        setSkin((TextureRegion) ImageCache.p_explodeAnimation.getKeyFrame(elapsedTime, false));
                        break;
                    case Type.EXIT:
                        goalreached = true;
                        setSkin((TextureRegion) ImageCache.p_finishAnimation.getKeyFrame(elapsedTime, false));
                        break;
                    default:
                        setSkin((TextureRegion) ImageCache.p_deathAnimation.getKeyFrame(elapsedTime, false));
                        break;
                }
            }
        }
        super.update(dt);
    }
	

	@Override
	public void place () {
		super.place();
		int dir = direction;
		if (direction == GameData.NONE)
			dir = lastdirection;
		showMoveFrame(dir);
	}
	
	@Override
	public void kill() {
		if (!dead) {
			//speed = 0;
			dead = true;
			Sounds.play(Sounds.death);
		}
		
	}

	@Override
	public void kill(int type) {
        //we going to kill you and get you to do a theatrical death
		animateType = type;
		kill();
	}


	@Override
	public void show () {
		super.show();
	}
	@Override
	public void hide () {
		super.hide();
	}

	@Override
	public void moveDir(int _direction) {
		if (!moving) {
			if (!_game.gameData.touchAndHold)
				addMove(_direction);
			else
				super.moveDir(_direction);
		}
	}

	private void addMove(int _direction) {
		int i = 0;
		for (i = 0; i < 5; i++) {
			if (moves[i] != GameData.NONE) { // put on the lowest empty
				continue;
			} else {
				moves[i] = _direction;
				i++; // step forward so we don't clear this value
				break;
			}
		}
		for (; i < 5; i++) {
			moves[i] = GameData.NONE;
		}
	}

	@Override
	public boolean stoodOn(TileSprite sprite, boolean onORoff) {
//		Gdx.app.log("player", " stoodon " + sprite.type);
        if (!_game.gameData.enableTransparencyGlitch) kill(sprite.type);
		return true; 
	}	
	@Override
	public boolean canMoveFrom(TileSprite sprite, int moveDirection) {
		return true; 
	}
	@Override
	public boolean canMoveToMe(TileSprite sprite, int moveDirection) {
		if (sprite.type == Type.PLAYER)
			return false;
		return canMoveToMe;
	}

	@Override
	public boolean stepToTiles(TileSprite sprite, boolean dontKill) {
		if (sprite.active) { //check if this is chip and other not or whatever
			kill(sprite.type); //TODO: hides creature that killed chip (do we want this to remain visible?)
			stoodon = sprite;
			return true;
		}
		return false;
	}


	/** Choose the sprite to match direction */
	private void showMoveFrame(int dir) {

		if (isRealChip()) {
			switch (dir) {
			case GameData.WEST:
				if (isSwimming) {
					//setSkin(_chipSwimWest);
					_restFrame = _chipSwimWest;				
				} else {
					//setSkin(_chipWest); //Crash here ??
					_restFrame = _chipWest;
				}
				break;
			case GameData.EAST:
				if (isSwimming) {
					//setSkin(_chipSwimEast);
					_restFrame = _chipSwimEast;		
				} else {
				//setSkin(_chipEast);
				_restFrame = _chipEast;
				}
				break;
			case GameData.NORTH:
				if (isSwimming) {
					//setSkin(_chipSwimNorth);
					_restFrame = _chipSwimNorth;				
				} else {
					//setSkin(_chipNorth); //Crash here ??
					_restFrame = _chipNorth;
				}
				break;
			case GameData.SOUTH:
				if (isSwimming) {
					//setSkin(_chipSwimSouth);
					_restFrame = _chipSwimSouth;				
				} else {
					//setSkin(_chipSouth);
					_restFrame = _chipSouth;
				}
				break;
			}
		} else {
			switch (type) {
			case Type.BURNEDCHIP:
				//setSkin(_chipBurned);
				_restFrame = _chipBurned;				
				break;
			case Type.DROWNEDCHIP:
				//setSkin(_chipDrowned);
				_restFrame = _chipDrowned;				
				break;
			case Type.SWIMMINGCHIP:
				//setSkin(_chipSwimSouth);
				_restFrame = _chipSwimSouth;				
				break;
			case Type.PLAYER:
				//setSkin(_chipSouth);
				_restFrame = _chipSouth;
				break;
			}
		}

        setSkin(_restFrame);

    }
	public void hideText() {
		textVisible = false;
		//text_timer = 0;
	}

	public void draw_death_text(SpriteBatch spriteBatch) {

		if (textVisible == true) {
			CharSequence str;
			switch (animateType) {
				case Type.WATER:
					str = GameData.getLanguageString("warn flippers");
					break;
				case Type.FIRE:
					str = GameData.getLanguageString("warn fireboots");
					break;
				case Type.BOMB:
					str = GameData.getLanguageString("warn bombs");
					break;
				case Type.CLONEBLOCK:
				case Type.ICEBLOCK:
				case Type.BLOCK:
					str = GameData.getLanguageString("warn blocks");
					break;
				default:
					str = GameData.getLanguageString("warn creatures");
					break;
			}

			_game.gameData.font3.setColor(0.1f, 0.1f, 0.1f, 1.0f);
			_game.gameData.font3.draw(spriteBatch, str, GameData.tileWidth,  _game.screenHeight - GameData.tileWidth*2f, GameData.tileWidth * 8f, Align.center, true);

		}

	}
    @Override
    public boolean goAhead (int dir, int gX, int gY) {
        if (isRealChip() && _game.gameData.gameMode != DirectedGame.GAME_STATE_PAUSE) {
            isSwimming = false;
            return super.goAhead(dir,gX,gY);
        }
        return false;
    }
}
