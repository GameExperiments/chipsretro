package org.porteousclan.chipsretro.game.elements.creature;

import org.porteousclan.chipsretro.DirectedGame;
import org.porteousclan.chipsretro.game.data.GameData;

public class Tank extends Creature {
    private boolean crashedIntoSomething = false;
    private float moveStartDelay = 0.2f;
	protected float moveStart = moveStartDelay;
	
	public Tank(DirectedGame game, int x, int y, int _type,  int layer, int direction, boolean startActive) {
		super(game, x, y, _type, layer, direction, startActive);
	}

	@Override
	protected void initialize() {
		animNorth = "tank_n";
		animEast = "tank_e";
		animSouth = "tank_s";
		animWest = "tank_w";
		super.initialize();
        preferredmoves[0] = GameData.FWD;
        preferredmoves[1] = GameData.NONE;
        preferredmoves[2] = GameData.NONE;
        preferredmoves[3] = GameData.NONE;
        preferredmoves[4] = GameData.NONE;
        moveStart = moveStartDelay;

	}

	@Override
	public void moveWanted() {
		if (!moving) {
            if ((_game.gameData.msStyleMoveConditions || _game.gameData.rommysStyleMoveConditions) && obstructed) {
                crashedIntoSomething = true;
            }

			if (canMove && !(crashedIntoSomething)) {
				if (moveStart <= 0)
					moveDir(lastdirection);
			}
		}
	}

    @Override
    public void update(float dt) {
        if (moveStart > 0)
            moveStart -= 1 * dt;
        else
            moveStart = 0;

        moveWanted();
        super.update(dt);
    }

	@Override
	public void trigger(boolean release) {
        //find another way - this is faulty
//       if (!_game.gameData.lynxStyleMoveConditions || (_game.gameData.lynxStyleMoveConditions && obstructed)) //if NOT (lynx and moving)
    		lastdirection = direction = getReversed(lastdirection);
        drawDirection = direction;
		crashedIntoSomething = false;
        obstructed = false;
        //Gdx.app.log("Crash", "false");
        if (!moving)
			moveStart = moveStartDelay;
		else
			moveStart = 0;
	}

//	@Override
//    public boolean goAhead(int dir, int gX, int gY) {
//        if (moveStart <= 0)
//            return goAhead(dir,gX,gY);
//        else
//            return false;
//
//    }

}
