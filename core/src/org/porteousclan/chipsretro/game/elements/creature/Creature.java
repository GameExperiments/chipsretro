package org.porteousclan.chipsretro.game.elements.creature;

import org.porteousclan.chipsretro.DirectedGame;
import org.porteousclan.chipsretro.game.data.GameData;
import org.porteousclan.chipsretro.game.data.GroupType;
import org.porteousclan.chipsretro.game.data.Sounds;
import org.porteousclan.chipsretro.game.data.Type;
import org.porteousclan.chipsretro.game.elements.TileSprite;

public class Creature extends TileSprite {

	public Creature(DirectedGame game, int x, int y, int _type,  int layer, int direction, boolean startActive) {
		super(null, game, x, y, direction);
		_layer = GameData.UPPER_LAYER;
		_originalType = type = _type;
		_initialActiveState = startActive;
		groupType = GroupType.CREATURE;
		initialize();
        transparency = true;
	}

	@Override
	public void place() {
//		int dir = direction;
//		if (direction == GameData.NONE)
//			dir = lastdirection;
		switch(drawDirection) {
		case GameData.NORTH:
			_restFrame = TEXTURE_0;
			break;
		case GameData.EAST:
			_restFrame = TEXTURE_1;
			break;
		case GameData.SOUTH:
			_restFrame = TEXTURE_2;
			break;
		case GameData.WEST:
			_restFrame = TEXTURE_3;
			break;
		default:
			_restFrame = TEXTURE_2;
			break;
		}
		setSkin(_restFrame);
		super.place();
	}


	@Override
	public boolean canMoveToMe(TileSprite sprite, int moveDirection) {
		if (sprite.type == Type.PLAYER) {
			return true;
		}
		return false;
	}

	@Override
	public boolean stoodOn(TileSprite sprite, boolean onORoff) {
		if (sprite.type == Type.PLAYER) {
			Sounds.play(Sounds.death);
			sprite.kill(type);
			kill(); //?? necessary ??
		}
		return true; 
	}	

	@Override
	public boolean stepToTiles(TileSprite sprite, boolean dontKill) {
		if (sprite.type == Type.PLAYER) {
			stoodon = sprite;
			return true;
		}
		return false;
	}
	@Override
	public boolean isCloneable() {return true;}
	@Override
	public boolean isCreature() {return true;}

}
