package org.porteousclan.chipsretro.game.elements.creature;

import com.badlogic.gdx.math.MathUtils;

import org.porteousclan.chipsretro.DirectedGame;
import org.porteousclan.chipsretro.game.data.GameData;

public class Blob extends Creature {

	public Blob(DirectedGame game, int x, int y, int _type,  int layer, int direction, boolean startActive) {
		super(game, x, y, _type, layer, direction, startActive);
	}
	@Override
	protected void initialize() {
		animNorth = "blob_n";
		animEast = "blob_e";
		animSouth = "blob_s";
		animWest = "blob_w";
		super.initialize();
		speed = defaultSpeed = defaultSpeed/2; //half speed
	}

	@Override
    public void moveWanted() {
        if (!moving) {
            if(_game.gameData.rommysStyleMoveConditions)
                preferredmoves[0] = GameData.FWD;
            else
                preferredmoves[0] = MathUtils.random(0,3) -1;

            preferredmoves[1] = MathUtils.random(0,3) -1;
            preferredmoves[2] = MathUtils.random(0,3) -1;
            preferredmoves[3] = MathUtils.random(0,3) -1;
            preferredmoves[4] = GameData.NONE;
            if (canMove) {
                moveDir(getNextAvailable(lastdirection));
            }
        }
    }

//	@Override
//	public void update(float dt) {
//        //moveWanted(dt);
//        super.update(dt);
//	}

}
