package org.porteousclan.chipsretro.game.elements.creature;

import org.porteousclan.chipsretro.DirectedGame;
import org.porteousclan.chipsretro.game.data.GameData;

public class FireBall extends Creature {

	public FireBall(DirectedGame game, int x, int y, int _type,  int layer, int direction, boolean startActive) {
		super(game, x, y, _type, layer, direction, startActive);
	}
	@Override
	protected void initialize() {
		animNorth = "fire_ball_n";
		animEast = "fire_ball_e";
		animSouth = "fire_ball_s";
		animWest = "fire_ball_w";
		super.initialize();
        preferredmoves[0] = GameData.FWD;
        preferredmoves[1] = GameData.RIGHT;
        preferredmoves[2] = GameData.LEFT;
        preferredmoves[3] = GameData.BACK;
        preferredmoves[4] = GameData.NONE;
	}

    @Override
    public void moveWanted() {
        if (!moving) {
            if (canMove) {
                moveDir(getNextAvailable(lastdirection));
            }
        }
    }
//    @Override
//    public void moveWanted() {
//        if (!moving) {
//            if (canMove) {
//                if (obstructed) {
//                    moveDir(getNextAvailable(lastdirection));                }
//                else
//                    moveDir(lastdirection);
//            }
//        }
//    }

//	@Override
//	public void update(float dt) {
//        moveWanted(dt);
//		super.update(dt);
//	}

}
