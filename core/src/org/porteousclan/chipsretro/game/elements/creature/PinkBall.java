package org.porteousclan.chipsretro.game.elements.creature;

import org.porteousclan.chipsretro.DirectedGame;
import org.porteousclan.chipsretro.game.data.GameData;

public class PinkBall extends Creature {

	public PinkBall(DirectedGame game, int x, int y, int _type,  int layer, int direction, boolean startActive) {
		super(game, x, y, _type, layer, direction, startActive);
	}

	@Override
	protected void initialize() {
		animNorth = "pink_ball_n";
		animEast = "pink_ball_e";
		animSouth = "pink_ball_s";
		animWest = "pink_ball_w";
		super.initialize();
        preferredmoves[0] = GameData.FWD;
        preferredmoves[1] = GameData.BACK;
        preferredmoves[2] = GameData.FWD;
        preferredmoves[3] = GameData.BACK;
        preferredmoves[4] = GameData.NONE;
	}

    @Override
    public void moveWanted() {
        if (!moving) {
            if (canMove) {
                moveDir(getNextAvailable(lastdirection));
            }
        }
    }

}
