package org.porteousclan.chipsretro.game.elements.creature;

import org.porteousclan.chipsretro.DirectedGame;
import org.porteousclan.chipsretro.game.data.GameData;

public class Paramecium extends Creature {

	public Paramecium(DirectedGame game, int x, int y, int _type,  int layer, int direction, boolean startActive) {
		super(game, x, y, _type, layer, direction, startActive);
	}

	@Override
	protected void initialize() {
		animNorth = "paramecium_n";
		animEast = "paramecium_e";
		animSouth = "paramecium_s";
		animWest = "paramecium_w";
		super.initialize();
        preferredmoves[0] = GameData.RIGHT;
        preferredmoves[1] = GameData.FWD;
        preferredmoves[2] = GameData.LEFT;
        preferredmoves[3] = GameData.BACK;
        preferredmoves[4] = GameData.NONE;
	}

	@Override
	public void moveWanted() {

		if (!moving) {
            if (canMove) {
                moveDir(getNextAvailable(lastdirection));
            }
		}
	}

//    @Override
//    public void update(float dt) {
//        moveWanted(dt);
//        super.update(dt);
//    }
}
