package org.porteousclan.chipsretro.game.elements.creature;

import com.badlogic.gdx.math.MathUtils;

import org.porteousclan.chipsretro.DirectedGame;
import org.porteousclan.chipsretro.game.data.GameData;

public class Walker extends Creature {

	public Walker(DirectedGame game, int x, int y, int _type,  int layer, int direction, boolean startActive) {
		super(game, x, y, _type, layer, direction, startActive);
	}

	@Override
	protected void initialize() {
		animNorth = "walker_n";
		animEast = "walker_e";
		animSouth = "walker_s";
		animWest = "walker_w";
		super.initialize();
	}

	@Override
	public void moveWanted() {
		if (!moving) {
			preferredmoves[0] = GameData.FWD;
			preferredmoves[1] = MathUtils.random(0, 3) - 1;
			preferredmoves[2] = MathUtils.random(0, 3) - 1;
			preferredmoves[3] = GameData.RIGHT;
			preferredmoves[4] = GameData.NONE;
		}

		if (!moving) {
			if (canMove) {
                if (obstructed) {
                    moveDir(getNextAvailable(lastdirection));
                }
                else
                    moveDir(lastdirection);
            }
		}
	}

//    @Override
//    public void update(float dt) {
//        moveWanted(dt);
//        super.update(dt);
//    }


}
