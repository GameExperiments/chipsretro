package org.porteousclan.chipsretro.game.elements.creature;

import org.porteousclan.chipsretro.DirectedGame;
import org.porteousclan.chipsretro.game.data.GameData;

public class Teeth extends Creature {

	public Teeth(DirectedGame game, int x, int y, int _type,  int layer, int direction, boolean startActive) {
		super(game, x, y, _type, layer, direction, startActive);
	}

	@Override
	protected void initialize() {
		animNorth = "teeth_n";
		animEast = "teeth_e";
		animSouth = "teeth_s";
		animWest = "teeth_w";
		super.initialize();
        speed = defaultSpeed = defaultSpeed/2f;
        speedMultiplier = speedMultiplier * 2f;

    }

	@Override
	public void moveWanted() {
		if (!moving) {
			int y,x,y1,x1,ns,ew;
            //where is player in relation to teeth
			y1 = y = currentGridY - _game.gameData._player.currentGridY;
			x1 = x = currentGridX - _game.gameData._player.currentGridX;
			ns = GameData.NONE;
			ew = GameData.NONE;
			
			preferredmoves[0] = GameData.FWD;
            preferredmoves[1] = GameData.NONE;
			preferredmoves[2] = GameData.NONE;
			preferredmoves[3] = GameData.NONE;
			preferredmoves[4] = GameData.NONE;

            //if Value positive I'm north or West so should face other way
			if (y>0) {ns = GameData.NORTH;}
			if (y<0) {ns = GameData.SOUTH; y1 = -y;}
			if (x>0) {ew = GameData.WEST;}
			if (x<0) {ew = GameData.EAST; x1 = -x;}

            if (y1 == x1) {
                if (evenMove)
                    y1 +=1;
                else
                    x1+=1;
                evenMove = !evenMove;
            }
            //choose the largest distance to go first
			if (y1>x1){
                //face north or south (FWD)
				direction = ns;

				if (ns == GameData.NORTH) {
					if( ew == GameData.WEST) {
						preferredmoves[1] = GameData.LEFT;
					}
					if( ew == GameData.EAST) {
						preferredmoves[1] = GameData.RIGHT;
					}
				}
				if (ns == GameData.SOUTH) {
					if( ew == GameData.EAST) {
						preferredmoves[1] = GameData.LEFT;
					}
					if( ew == GameData.WEST) {
						preferredmoves[1] = GameData.RIGHT;
					}
				}
				
			}
//            if (y1<x1){
            else {
				direction = ew;
				if (ew == GameData.WEST) {
					if( ns == GameData.SOUTH) {
						preferredmoves[1] = GameData.LEFT;
					}
					if( ns == GameData.NORTH) {
						preferredmoves[1] = GameData.RIGHT;
					}
				}
				if (ew == GameData.EAST) {
					if( ns == GameData.NORTH) {
						preferredmoves[1] = GameData.LEFT;
					}
					if( ns == GameData.SOUTH) {
						preferredmoves[1] = GameData.RIGHT;
					}
				}
			} 
			 
		}

		if (!moving) {
            //teeth has no preferred but will simply try follow player
			if (canMove)
			    if (_game.gameData.msStyleMovement)
    			    moveDir(getNextAvailable(direction));
                else {
                        moveDir(getNextWanted(direction, directionIndex));
                        directionIndex++;
                        if (directionIndex > 1) directionIndex = 0;
//                        moveDir(direction);
                }
		}
	}

//    @Override
//    public void update(float dt) {
//        moveWanted(dt);
//        super.update(dt);
//    }


}
