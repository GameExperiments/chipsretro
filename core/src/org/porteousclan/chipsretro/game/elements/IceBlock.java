package org.porteousclan.chipsretro.game.elements;

import com.badlogic.gdx.Gdx;

import org.porteousclan.chipsretro.DirectedGame;
import org.porteousclan.chipsretro.game.data.DirectionStack;
import org.porteousclan.chipsretro.game.data.GameData;
import org.porteousclan.chipsretro.game.data.GroupType;
import org.porteousclan.chipsretro.game.data.ImageCache;
import org.porteousclan.chipsretro.game.data.Sounds;
import org.porteousclan.chipsretro.game.data.Type;

import java.util.ArrayList;

public class IceBlock extends TileSprite {
	//private boolean expended = true;
	float deathTimer = 0f;

	public IceBlock(DirectedGame game, int x, int y, int _type, int layer, int direction, boolean startActive) {
		super(null, game, x, y, direction);
		//_psudoType = _type;
		_initialActiveState = startActive;
		_layer = layer; //GameData.UPPER_LAYER;
		_originalType = type = _type;
		groupType = GroupType.BLOCK;
        initialize();
	}

	public IceBlock(DirectedGame game, int x, int y, int _type, int layer, int direction) {
		this(game,x,y,_type,layer,direction,true);
	}

	@Override
	public void initialize() {
		type = _originalType;
		//expended = true;

		transparency = true;
		//Mutated = false;
		canMove = true;

		_joinedElements = new ArrayList<JoinedTile>();
		
		if (TEXTURE_1 == null) {
				TEXTURE_0 = ImageCache.getFrame("ice_block", -1);
				TEXTURE_1 = ImageCache.getFrame("ice_block", -1);
				TEXTURE_2 = ImageCache.getFrame("ice_block", -1);
				TEXTURE_3 = ImageCache.getFrame("ice_block", -1);
		}

		preferredmoves[0] = GameData.FWD;
		preferredmoves[1] = GameData.NONE;
		preferredmoves[2] = GameData.NONE;
		preferredmoves[3] = GameData.NONE;
		preferredmoves[4] = GameData.NONE;

		switch(direction) {
		case GameData.NORTH:
			_restFrame = TEXTURE_0;
			break;
		case GameData.EAST:
			_restFrame = TEXTURE_1;
			break;
		case GameData.SOUTH:
			_restFrame = TEXTURE_2;
			break;
		case GameData.WEST:
			_restFrame = TEXTURE_3;
			break;
		default:
			_restFrame = TEXTURE_2;
			break;
		}
		setSkin(_restFrame);
		show();
	}
	
		
	@Override
	public void update(float dt) {
		if (!moving) {
			preferredmoves[0] = GameData.FWD;
			preferredmoves[1] = GameData.NONE;
			preferredmoves[2] = GameData.NONE;
			preferredmoves[3] = GameData.NONE;
			preferredmoves[4] = GameData.NONE;
			//this causes this to keep sliding
			//if ( directionAffected(direction) == GameData.NONE ) lastdirection = GameData.NONE;
			//if ( lastdirection != GameData.NONE )
			//	moveDir(lastdirection);
		}
		super.update(dt);

	}

	@Override
	public void place() {
		switch(direction) {
		case GameData.NORTH:
			_restFrame = TEXTURE_0;
			break;
		case GameData.EAST:
			_restFrame = TEXTURE_1;
			break;
		case GameData.SOUTH:
			_restFrame = TEXTURE_2;
			break;
		case GameData.WEST:
			_restFrame = TEXTURE_3;
			break;
		default:
			_restFrame = TEXTURE_2;
			break;
		}
		setSkin(_restFrame);
//		show();
		super.place();
	}
	

	@Override
	public boolean canMoveToMe(TileSprite sprite, int moveDirection) {
		if (sprite.type == Type.PLAYER || sprite.type == Type.ICEBLOCK || sprite.type == Type.BLOCK) {
			return true;
		}
		return false;
	}

	@Override
	public boolean stoodOn(TileSprite sprite, boolean onORoff) {
		if (sprite.type == Type.PLAYER) {
//			Sounds.play(Sounds.death);
			sprite.kill(type);
			kill();
		}
		return true; 
	}

	@Override
	public boolean stepToTiles(TileSprite sprite, boolean dontKill) {
		int id2;
		TileSprite sprite2 = null;

		if (sprite.type == Type.PLAYER || sprite.type == Type.ICEBLOCK || sprite.type == Type.BLOCK) {
			this.direction = this.lastdirection = sprite.direction;

            if (sprite.type == Type.PLAYER) {
                if (_game.gameData.slapOnly)
                    Sounds.play(Sounds.bump);
                else
                    Sounds.play(Sounds.oof);
            }

			moveDir(sprite.direction);
			moving = false; stopping = false;

            if(_game.gameData.msStyleMoveConditions) { //get corner to ignore block
                id2 = DirectionStack.getTopUpperLowerId(_game, this, GameData.NONE);
                if(id2 > Type.EMPTY) {
                    sprite2 = DirectionStack.getElement(_game,id2);
                    if (sprite2 != null && (sprite2.type == Type.ICEWALL_NE || sprite2.type == Type.ICEWALL_NW || sprite2.type == Type.ICEWALL_SE || sprite2.type == Type.ICEWALL_SW)) {
                        sprite2.stoodon = null;
                        Gdx.app.log("block", "telling ice corner to ignor me. ic type=" + sprite2.type);
                    }
                }
            }

            if (checkMove.attemptMove()) {
                if (_game.gameData.slapOnly) {
                     return false;
                } else {
                    stoodon = sprite;
                    return true;
                }
			}
		}
		return false;
		
	}

	@Override
	public boolean stepOffTiles(TileSprite sprite, boolean clearAhead) {
		return true;
	}
	
	@Override
	public boolean isCloneable() {return true;}

}
