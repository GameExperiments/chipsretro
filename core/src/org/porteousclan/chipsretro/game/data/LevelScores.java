package org.porteousclan.chipsretro.game.data;

import com.badlogic.gdx.Gdx;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

/**
 * Created by richard on 23/08/16.
 */

public class LevelScores {
    public static String levelSaveMsg = "";
    public static String levelReadMsg = "";


    public static int[] loadlevelscores (String FILENAME) {
        BufferedReader in = null;
        int level=0, timeleft=-1, lives=0;
        String value=null;
        int[] ret = {level,timeleft,lives};

        try {
            if (Gdx.files.local(FILENAME).exists()) {
                in = new BufferedReader(new InputStreamReader(Gdx.files.local(FILENAME).read()));
                value = in.readLine();
                if (value != null) { //break;
                    level = Integer.parseInt(value);
                }
                value = in.readLine();
                if (value != null) { //break;
                    timeleft = Integer.parseInt(value);
                }
                value = in.readLine();
                if (value != null) { //break;
                    lives = Integer.parseInt(value);
                }
                ret[0] = level;
                ret[1] = timeleft;
                ret[2] = lives;
            }
        } catch (Throwable e) {}
        finally {
            try {
                if (in != null) in.close();
            } catch (Throwable e) {}
        }
        return ret;
    }

    public static boolean savelevelscores (GameData data, int level, int timeleft, int lives) {
        BufferedWriter out = null;
        int oldscores[] = {0,-1,0};

        int level_bonus = get_level_bonus(level, lives);
        int time_bonus = get_time_bonus(timeleft);
        int score = level_bonus + time_bonus;
        //boolean ret = false;

        levelSaveMsg = data.getLanguageString("Unknown Error") + " 0";
        String FILENAME = data.lastDataSet + "_levels_" + level;
        try {
            if (Gdx.files.local(FILENAME).exists()) {
                levelSaveMsg = data.getLanguageString("Unknown Error") + " 1";
                oldscores = loadlevelscores (FILENAME);
                levelSaveMsg = data.getLanguageString("Unknown Error") + " 1";
                int time_bonus_old = get_time_bonus(oldscores[1]); //timeleft
                int level_bonus_old = get_level_bonus(level, oldscores[2]); //lives
                if ( score <= ( level_bonus_old + time_bonus_old)) {
                    levelSaveMsg = data.getLanguageString("Old Better");
                    return false;
                }
                levelSaveMsg = data.getLanguageString("Unknown Error") + " 2";
                Gdx.files.local(FILENAME).delete();
            }
        } catch (Throwable e) {
            levelSaveMsg = data.getLanguageString("Unknown Error") + " 3";

        }


        try {
            levelSaveMsg = data.getLanguageString("Save failed") + " 0";
            String outStr;
            out = new BufferedWriter(new OutputStreamWriter(Gdx.files.local(FILENAME).write(false)));
            levelSaveMsg = data.getLanguageString("Save failed") + " 1";
            outStr = Integer.toString(level) + "\n";
            out.write(outStr);
            outStr = Integer.toString(timeleft) + "\n";
            out.write(outStr);
            outStr = Integer.toString(lives) + "\n";
            out.write(outStr);
            //read somewhere this was important.
            levelSaveMsg = data.getLanguageString("Save failed") + " 2";
            out.flush();
            levelSaveMsg = data.getLanguageString("Saved");

        } catch (Throwable e) {
            //nothing really
        } finally {
            try {
                if (out != null) out.close();
            } catch (IOException e) {
            }
        }

        return true;
    }

    public static int get_level_bonus(int level, int lives) {
        int level_bonus=500;
        if (level > 0) {
            level_bonus = level * 500;
            if (lives > 0 ) {
                level_bonus = (int)(level_bonus / ( 0.8 * lives ));
            }
        }
        if (level_bonus < 500) level_bonus = 500;
        return level_bonus;
    }

    public static int get_time_bonus(int timeleft) {
        if (timeleft > 0) {
            if (timeleft > 999) timeleft = 999;
            return (int) timeleft*10;
        }
        return (int)0;
    }


    public static LevelScores createLevelScores() {
        return new LevelScores();
    }
}
