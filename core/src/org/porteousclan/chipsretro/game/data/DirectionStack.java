package org.porteousclan.chipsretro.game.data;

import org.porteousclan.chipsretro.DirectedGame;
import org.porteousclan.chipsretro.game.elements.TileSprite;

import java.util.List;

/**
 * Created by richp on 2016-12-16.
 */

public class DirectionStack {
    public static int[][][] _tiles; //initilized in GameData

    /** check if our stoodon sprite exists in this bag */
    public static boolean isInDirectionStack(DirectedGame _game, TileSprite sprite, int _direction, int x, int y) {
        TileSprite sprite2 = null;
        int bag[] = getDirectionStack(_direction, x, y);
        for (int i = GameData.LOWER_LAYER; i <= GameData.VISITOR_LAYER; i++) {
            if (bag[i] > Type.EMPTY) {
                sprite2 = getElement(_game, bag[i]);
                if (sprite.stoodon == sprite2) return true;
            }
        }
        return false;
    }
    /** Set what tiles are near */
    public static int[] getDirectionStack(int _direction, int x, int y) {

        int x1 = getGridXinDirection(_direction, x);
        int y1 = getGridYinDirection(_direction, y);

        return getStack(x1,y1);
    }

    public static int[] getStack(int x1, int y1) {
        int _id[] = {Type.EMPTY,Type.EMPTY,Type.EMPTY,Type.EMPTY};
        _id[GameData.VISITOR_LAYER] = getTileId(GameData.VISITOR_LAYER, x1, y1);
        _id[GameData.UPPER_LAYER] = getTileId(GameData.UPPER_LAYER, x1, y1);
        _id[GameData.LOWER_LAYER] = getTileId(GameData.LOWER_LAYER, x1, y1);
        return _id;
    }
    public static int getTileId(int layer, int x, int y) { //only used in get Stack
        if (!(layer >= GameData.LOWER_LAYER && layer <= GameData.VISITOR_LAYER)) {
            return Type.BORDERS;
        }
        if (!inLimitsXY( x, y)) {
            return Type.BORDERS;
        }
        return _tiles[x - 1][y - 1][layer];
    }

    public static int pushVisitorOnStack(int x, int y, int value) {
        if (inLimitsX( x) && inLimitsY( y))
        {
            if (_tiles[x - 1][y - 1][GameData.VISITOR_LAYER] <= Type.EMPTY) {
                _tiles[x - 1][y - 1][GameData.VISITOR_LAYER] = value;
                return GameData.VISITOR_LAYER;
            }
        }
        return Type.BORDERS; //cheat//return GameData.Type.EMPTY; //sorry we are full -- throw an exception?
    }

    /** on the level map XY */
    public static boolean inLimitsXY(int x, int y) {
        if (inLimitsX(x) && inLimitsY(y)) {
            return true;
        } else {
            return false;
        }
    }

    /** on the level map X */
    public static boolean inLimitsX(int x) {
        if (x < 1 || x > GameData.totaltilesX) {
            return false;
        } else {
            return true;
        }
    }

    /** on the level map Y */
    public static boolean inLimitsY(int y) {
        if (y < 1 || y > GameData.totaltilesY) {
            return false;
        } else {
            return true;
        }
    }

    public static TileSprite getElement(DirectedGame _game, int index) {
        if (_game.gameData._elements.size() > index && index >=0)
            return _game.gameData._elements.get(index);
        return null;

    }


    public static boolean setTile(int layer, int x, int y, int value) {
        if (layer >= GameData.LOWER_LAYER && layer <= GameData.VISITOR_LAYER) {
            if (inLimitsX( x) && inLimitsY( y)) {
                if ((_tiles[x - 1][y - 1][layer] == Type.EMPTY) || (_tiles[x - 1][y - 1][layer] == value)) {
                    _tiles[x - 1][y - 1][layer] = value;
                    return true;
                }
                return false;
            }
        }
        return false;
    }

    public static boolean clearTile(int layer, int x, int y) {
        if (layer >= GameData.LOWER_LAYER && layer <= GameData.VISITOR_LAYER) {
            if (inLimitsX( x) && inLimitsY( y))
            {
                    _tiles[x - 1][y - 1][layer] = Type.EMPTY;
            }
        }
        return false;
    }

    public static boolean clearTileBag(int x, int y) {
        //clearTile(GameData.VISITOR_UPPER_LAYER, x, y);
        clearTile(GameData.VISITOR_LAYER, x, y);
        clearTile(GameData.UPPER_LAYER, x, y);
        clearTile(GameData.LOWER_LAYER, x, y);
        return false;
    }

    public static void clearAllTiles() {
        for (int x = 1; x<= GameData.totaltilesX; x++) {
            for (int y = 1; y<= GameData.totaltilesX; y++) {
                clearTileBag(x, y);
            }
        }
    }

    public static void reSetAllTiles(List<TileSprite> elements) {
        clearAllTiles();
        TileSprite sprite;
        int len = elements.size();
        for (int i = 0; i < len; i++) {
            sprite = elements.get(i);
            if (!sprite.active) {
                continue;
            }
            setTile(sprite._layer, sprite.currentGridX,sprite.currentGridY, i);
        }
    }

    public static int getGridXinDirection(int direction, int x) {
        switch (direction) {
        case GameData.WEST:
            return x - 1;
        case GameData.EAST:
            return x + 1;
        case GameData.NORTH:
        case GameData.SOUTH:
        default:
            return x;
        }
    }

    public static int getGridYinDirection(int direction, int y) {
        switch (direction) {
        case GameData.NORTH:
            return y - 1;
        case GameData.SOUTH:
            return y + 1;
        case GameData.WEST:
        case GameData.EAST:
        default:
            return y;
        }
    }

    public static int getTopUpperLowerITEMid(DirectedGame _game, TileSprite sprite, int _direction) {
        TileSprite sprite2 = null;
        int bag[], id;

        bag = getDirectionStack(_direction, sprite.currentGridX, sprite.currentGridY);

        id = bag[GameData.UPPER_LAYER];
        if (id> Type.EMPTY) {
            sprite2 = getElement(_game, id);
            if (sprite2.isItem()) {
                return id;
            }
        }

        id = bag[GameData.LOWER_LAYER];
        if (id> Type.EMPTY) {
            sprite2 = getElement(_game, id);
            if (sprite2.isItem()) {
                return id;
            }
        }
        if (id > Type.EMPTY)
            return Type.EMPTY;
        return id;
    }

    public static int getTopUpperLowerId(DirectedGame _game, TileSprite sprite, int _direction) {
        TileSprite sprite2 = null;
        int bag[], id;
        bag = getDirectionStack(_direction, sprite.currentGridX, sprite.currentGridY);
        id = bag[GameData.UPPER_LAYER];
        if (id> Type.EMPTY) {
            sprite2 = getElement(_game, id);
            if (sprite2 != sprite && !sprite2.transparency) {
                return id;
            }
        }
        id = bag[GameData.LOWER_LAYER];
        if (id> Type.EMPTY) {
            sprite2 = getElement(_game, id);
            if (sprite2 != sprite && !sprite2.transparency) {
                return id;
            }
        }

        if (id <= Type.EMPTY)
            return id;
        else
            return Type.EMPTY;
    }

    public static int getTopAllLayersId(DirectedGame _game, TileSprite sprite, int _direction) {
        int bag[], id;
        TileSprite sprite2 = null;
        bag = getDirectionStack(_direction, sprite.currentGridX, sprite.currentGridY);

        id = bag[GameData.VISITOR_LAYER];
        if (id> Type.EMPTY) {
            sprite2 = getElement(_game, id);
            if (sprite2 != sprite) {
                return id;
            }
        }
        id = bag[GameData.VISITOR_LAYER];
        if (id> Type.EMPTY) {
            sprite2 = getElement(_game, id);
            if (sprite2 != sprite) {
                return id;
            }
        }
        id = bag[GameData.UPPER_LAYER];
        if (id> Type.EMPTY) {
            sprite2 = getElement(_game, id);
            if (sprite2 != sprite) {
                return id;
            }
        }
        id = bag[GameData.LOWER_LAYER];
        if (id> Type.EMPTY) {
            sprite2 = getElement(_game, id);
            if (sprite2 != sprite) {
                return id;
            }
        }
        if (id <= Type.EMPTY)
            return id;
        else
            return Type.EMPTY;
    }

    public static int getVisitorId(DirectedGame _game, TileSprite sprite, int _direction) {
        int bag[], id;
        TileSprite sprite2 = null;
        bag = getDirectionStack(_direction, sprite.currentGridX, sprite.currentGridY);

        id = bag[GameData.VISITOR_LAYER];
        if (id> Type.EMPTY) {
            sprite2 = getElement(_game, id);
            if (sprite2 != sprite) {
                return id;
            }
        }
        if (id <= Type.EMPTY)
            return id;
        else
            return Type.EMPTY;
    }

    public static int getUpperId(DirectedGame _game, TileSprite sprite, int _direction) {
        int bag[], id;
        TileSprite sprite2 = null;
        bag = getDirectionStack(_direction, sprite.currentGridX, sprite.currentGridY);

        id = bag[GameData.UPPER_LAYER];
        if (id> Type.EMPTY) {
            sprite2 = getElement(_game, id);
            if (sprite2 != sprite) {
                return id;
            }
        }
        if (id <= Type.EMPTY)
            return id;
        else
            return Type.EMPTY;
    }

    public static int getLowerId(DirectedGame _game, TileSprite sprite, int _direction) {
        int bag[], id;
        TileSprite sprite2 = null;
        bag = getDirectionStack(_direction, sprite.currentGridX, sprite.currentGridY);

        id = bag[GameData.LOWER_LAYER];
        if (id> Type.EMPTY) {
            sprite2 = getElement(_game, id);
            if (sprite2 != sprite) {
                return id;
            }
        }
        if (id <= Type.EMPTY)
            return id;
        else
            return Type.EMPTY;
    }
}
