package org.porteousclan.chipsretro.game.data;

import com.badlogic.gdx.Gdx;

/**
 * Created by richard on 23/08/16.
 */

public class Preferences {
    public static void loadSettings (GameData data) {
        try {
            com.badlogic.gdx.Preferences prefs = Gdx.app.getPreferences("RommyGuantletPreferences");
            data.lastDataSet = prefs.getString("lastDataSet", "");
            data.lastLevel = prefs.getInteger("lastLevel", 1);
            Sounds.SoundOn = prefs.getBoolean("soundOn", true);
            Sounds.SoundEffectsOn = prefs.getBoolean("soundEffectsOn", true);

            Sounds.volume = prefs.getFloat("volume", 1f); //Sounds.volume);
            Sounds.effectsvolume = prefs.getFloat("effectsvolume", 1f); //Sounds.effectsvolume);

            //data.scoreExternal = prefs.getBoolean("scoreExternal", true);
            data.touchAndHold = prefs.getBoolean("pressAndHold", data.touchAndHold);

            data.catchBackKey = prefs.getBoolean("catchBackKey", data.catchBackKey);
            data.msStyleAnimation = prefs.getBoolean("msStlyeAnimation", data.msStyleAnimation);
            data.msStyleMovement = prefs.getBoolean("msStlyeMovement", data.msStyleMovement);
            data.msStyleMoveConditions = prefs.getBoolean("msStyleMoveConditions", data.msStyleMoveConditions);
            data.lynxStyleMoveConditions = prefs.getBoolean("lynxStyleMoveConditions", data.lynxStyleMoveConditions);
            data.rommysStyleMoveConditions = prefs.getBoolean("rommysStyleMoveConditions", data.rommysStyleMoveConditions);
            data.slowSpeed = prefs.getBoolean("slowSpeed", data.slowSpeed);
            data.enableTransparencyGlitch = prefs.getBoolean("transparencyGlitch", data.enableTransparencyGlitch);

        } catch (Exception e) {}
    }

    public static void saveSettings(GameData data) {
        try {
            com.badlogic.gdx.Preferences prefs = Gdx.app.getPreferences("RommyGuantletPreferences");
            //prefs.clear();
            prefs.putString("lastDataSet", data.lastDataSet);
            prefs.putInteger("lastLevel", data.lastLevel);

            prefs.putBoolean("soundOn", Sounds.SoundOn);
            prefs.putBoolean("soundEffectsOn", Sounds.SoundEffectsOn);

            //prefs.putFloat("volume", Sounds.volume);
            //prefs.putFloat("effectsvolume", Sounds.effectsvolume);

            prefs.putBoolean("searchForLevels",data.searchForNewLevelSets);
            //prefs.putBoolean("scoreExternal", true);
            prefs.putBoolean("pressAndHold", data.touchAndHold);
            prefs.putBoolean("catchBackKey",data.catchBackKey);
            prefs.putBoolean("msStlyeMovement", data.msStyleMovement);
            prefs.putBoolean("msStlyeAnimation", data.msStyleAnimation);
            prefs.putBoolean("msStyleMoveConditions", data.msStyleMoveConditions);
            prefs.putBoolean("lynxStyleMoveConditions", data.lynxStyleMoveConditions);
            prefs.putBoolean("rommysStyleMoveConditions", data.rommysStyleMoveConditions);
            prefs.putBoolean("slowSpeed", data.slowSpeed);
            prefs.putBoolean("transparencyGlitch", data.enableTransparencyGlitch);


            prefs.flush();
        } catch (Exception e) {}
    }

    public static void saveLevelpos(String levelname, float position) {
        try {
            com.badlogic.gdx.Preferences levelprefs = Gdx.app.getPreferences(levelname);
            levelprefs.putFloat("position", position);
            levelprefs.flush();
        } catch (Exception e) {}

    }
    public static float getLevelpos(String levelname) {
        try {
            com.badlogic.gdx.Preferences levelprefs = Gdx.app.getPreferences(levelname);
            return levelprefs.getFloat("position", 0);
        } catch (Exception e) {}
        return 0;
    }

}
