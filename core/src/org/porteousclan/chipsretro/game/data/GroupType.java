package org.porteousclan.chipsretro.game.data;

/**
 * Created by Richard on 2017-04-08.
 */

public class GroupType {
    public static final int CREATURE = 1;
    public static final int BLOCK = 2;
    public static final int ITEM = 3;
    public static final int TILE = 4;
    public static final int PLAYER = 5;
}