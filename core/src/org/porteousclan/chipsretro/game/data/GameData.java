package org.porteousclan.chipsretro.game.data;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;

import org.porteousclan.chipsretro.DirectedGame;
import org.porteousclan.chipsretro.game.gameLevels.InputStreamFileLevelReader;
import org.porteousclan.chipsretro.game.elements.Player;
import org.porteousclan.chipsretro.game.elements.TileSprite;
import org.porteousclan.chipsretro.game.elements.tiles.Hint;
import org.porteousclan.chipsretro.game.gui.Inventory;
import org.porteousclan.chipsretro.game.gui.TimeBar;

import java.util.ArrayList;
import java.util.List;

public class GameData {

	public int  pv_offset_X;
	public int  pv_offset_Y;

	static org.porteousclan.chipsretro.util.LanguagesManager lang = org.porteousclan.chipsretro.util.LanguagesManager.getInstance();

	public String texturefolder = "data/tilesets/tileworld/";
	public static String skinsfolder = "data/skins/";
	public String tileset2d = "tileset35x35.atlas";
	public String sidepanel = "sidepanel.atlas";

	public boolean catchBackKey = true;
	public boolean touchAndHold =true;
/****RULESET VARS****/
	public boolean msStyleMovement=true;
	public boolean rommysStyleMoveConditions = true;
    public boolean msStyleMoveConditions = false;
    public boolean lynxStyleMoveConditions = false;
	public boolean msStyleAnimation = true;
    public boolean slowSpeed = true;
    public boolean enableTransparencyGlitch = true; //this is for things under items i.e. sokoban
    public boolean slapOnly = false;
/********************/

	public String splitcontrol = "splitcontrols.atlas";
	public String numbers = "numbers.atlas";
	public String numbers_prefix = "";

    public List<TileSprite> _creatureMoveOrderList;
	public List<TileSprite> _elements;
	public List<TileSprite> _floors;
    public Player _player;
	public Hint hint;
    public String levelPassword;
	public Inventory _inventory;
	public TimeBar _timebar;

	public String dataFolder = "/porteousclan/rommysgauntlet/";
	public String gamefolder = "data/games/";

	public boolean searchForNewLevelSets = true;
	public String file = ".chipsretro";
	public boolean needToSave = false;
	public String lastDataSet = "";
	public int lastLevel = 1;
	public static int dataSetLocation = InputStreamFileLevelReader.INTERNAL;

	public CharSequence levelname = "";
	public CharSequence levelhint = "";

	public enum GameScreenEventMessage{
		NORMAL,PLAYERDIED,HINTMESSAGE,LEVELDONE,TIMEOUTMESSAGE
	};
	public GameScreenEventMessage gsEvent = GameScreenEventMessage.NORMAL;

	public int chips = 0;
	public int chipscount = 0;
	public int level = 1;
	public int lives = 1;
	public int timeleft = 1000;
	public int gameMode;
	public int targetsReached = 0;

	public static int totaltilesX=32;
	public static int totaltilesY=32;
	public int gridFrameCurrentX;
	public int gridFrameCurrentY;
	public static float tileWidth;
	public static float tileHeight;
	public int tilesInFrameX;
	public int tilesInFrameY;

	public static final int  NORTH = 0;
	public static final int  EAST = 1;
	public static final int  SOUTH = 2;
	public static final int  WEST = 3;
	public static final int  NONE = 4;

    public static final int  LEFT = -1;
	public static final int  FWD = 0;
	public static final int  RIGHT = 1;
	public static final int  BACK = 2;

    public static final int  BASE_LAYER = -1;
	public static final int  LOWER_LAYER = 0;
	public static final int  UPPER_LAYER = 1;
	public static final int VISITOR_LAYER = 2;
	//public static final int  VISITOR_UPPER_LAYER = 3;

	public BitmapFont font1;
	public BitmapFont font2;
	public BitmapFont font3;
	public BitmapFont font4;
	public BitmapFont scaledfont1;
	public BitmapFont scaledfont2;
	public BitmapFont scaledfont3;
	public BitmapFont scaledfont4;

	public GameData (DirectedGame game) {
		gameMode = DirectedGame.GAME_STATE_PAUSE;
		font1 = getNewLimitedScaledTTFont(19);
		font2 = getNewLimitedScaledTTFont(22);
		font3 = getNewLimitedScaledTTFont(25);
		font4 = getNewLimitedScaledTTFont(40);
		scaledfont1 = getNewScaledTTFont(3);
		scaledfont2 = getNewScaledTTFont(5);
		scaledfont3 = getNewScaledTTFont(8);
		scaledfont4 = getNewScaledTTFont(10);

		org.porteousclan.chipsretro.game.data.Preferences.loadSettings(this);

		lang = org.porteousclan.chipsretro.util.LanguagesManager.getInstance();
		totaltilesX = 32;
		totaltilesY = 32;

		level = lastLevel;
		lives = 0;
		targetsReached = 0;
		DirectionStack._tiles = new int[totaltilesX][totaltilesY][4];

		game.screenHeight = Gdx.graphics.getHeight();
		game.screenWidth = Gdx.graphics.getWidth();

        game.screenHUDHeight = 480;
        game.screenHUDWidth = 800;

		skinsfolder = "data/skins/";

		tileWidth = 64;
		tileHeight = 64;
		tileset2d = "tileset64x64.atlas";
		numbers_prefix = "l";

        texturefolder = "data/tilesets/tileworld/";
        sidepanel = "sidepanel.atlas";
        numbers = "numbers.atlas";
        pv_offset_X = (int)4;
        pv_offset_Y = (int)((game.screenWidth/2) - (tileHeight*9)/2);

		reset();
	}
	

 	public static String getLanguageString(String translate) {
 		return lang.getString(translate);
 	}

	private BitmapFont getNewLimitedScaledTTFont(int dp) {

		//SCALE dp based on density
		float scaled_size = dp * Gdx.graphics.getDensity();
		//NOW scale below max size
		float screenheightIN = Gdx.graphics.getHeight() / Gdx.graphics.getPpiY();
		float maxSizeHeightItemIN=2.2f;
		if (screenheightIN < maxSizeHeightItemIN) { //GameData.magic_screen_height){
			scaled_size = scaled_size * (screenheightIN/maxSizeHeightItemIN);
		}
		int scaled_dp = (int)(scaled_size - (scaled_size%1));
		if ((scaled_size%1) >= 0.5)
			scaled_dp += 1;

		FreeTypeFontGenerator generator = new FreeTypeFontGenerator(Gdx.files.internal("fonts/" + org.porteousclan.chipsretro.util.LanguagesManager.getFontName()));
		FreeTypeFontGenerator.FreeTypeFontParameter parameter = new FreeTypeFontGenerator.FreeTypeFontParameter();
		parameter.size = scaled_dp;
		parameter.characters = FreeTypeFontGenerator.DEFAULT_CHARS + GameData.getLanguageString("characters");

		BitmapFont scaledFont = generator.generateFont(parameter);
		generator.dispose();
		return scaledFont;
	}

	private BitmapFont getNewScaledTTFont(int dp) {

		//SCALE dp based on density
		float scaled_size = dp * Gdx.graphics.getDensity();
		//NOW scale by screen size
		float screenheightIN = Gdx.graphics.getHeight() / Gdx.graphics.getPpiY();
		scaled_size = scaled_size * screenheightIN;
		int scaled_dp = (int)(scaled_size - (scaled_size%1));
		if ((scaled_size%1) >= 0.5)
			scaled_dp += 1;

		FreeTypeFontGenerator generator = new FreeTypeFontGenerator(Gdx.files.internal("fonts/" + org.porteousclan.chipsretro.util.LanguagesManager.getFontName()));
		FreeTypeFontGenerator.FreeTypeFontParameter parameter = new FreeTypeFontGenerator.FreeTypeFontParameter();
		parameter.size = scaled_dp;
		parameter.characters = FreeTypeFontGenerator.DEFAULT_CHARS + GameData.getLanguageString("characters");
		BitmapFont scaledFont = generator.generateFont(parameter); // font size 12 pixels
		generator.dispose(); // don't forget to dispose to avoid memory leaks!
		return scaledFont;
	}

	public void reset () {
		gridFrameCurrentX = 1;
		gridFrameCurrentY = 1;
		tilesInFrameX = 9;
		tilesInFrameY = 9;
		chips = 0;
		chipscount = 0;
		if (_elements != null) {
			_elements.clear();
            _creatureMoveOrderList.clear();
		} else {
			_elements = new ArrayList<TileSprite>();
            _creatureMoveOrderList = new ArrayList<TileSprite>();
		}
		if (_floors == null)
			_floors = new ArrayList<TileSprite>();
	}
	

	//Some folder listing done here
	public int countDoneLevels(String datasetname) {
		int countLevels = 0;
		String FILENAME = datasetname + "_levels_";
		//int namelen = FILENAME.length();
		try {
			FileHandle[] files = Gdx.files.local("./").list();
			for(FileHandle file: files) {
				   // do something interesting here
					if (file.name().contains(FILENAME)) {
							countLevels++;
					}
				}
		} catch (Exception e){
			
		}

		return countLevels;
	}
	
// ***************************************STATIC******************************************


}
