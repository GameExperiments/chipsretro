package org.porteousclan.chipsretro.game.data;

/**
 * Created by richp on 2016-12-16.
 */



public class Type {
    public static final int BORDERS = -2;
    public static final int EMPTY = -1;
    public static final int BLOB = 0;
    public static final int BLOCK = 1;
    public static final int BLUEBUTTON = 2;
    public static final int BLUEKEY = 3;
    public static final int BLUELOCK = 4;
    public static final int BLUEWALLREAL = 5;
    public static final int BLUEWALLFAKE = 6;
    public static final int BOMB = 7;
    public static final int BROWNBUTTON = 8;
    public static final int BUG = 9;
    public static final int BURNEDCHIP = 10;
    public static final int PLAYER = 11;
    public static final int CLONEBLOCK = 12;
    public static final int CLONEMACHINE = 13;
    public static final int COMPUTERCHIP = 14;
    public static final int DIRT = 15;
    public static final int DROWNEDCHIP = 16;
    public static final int EXIT = 17;
    public static final int FAKEEXIT = 18;
    public static final int FIRE = 19;
    public static final int FIREBOOTS = 20;
    public static final int FIREBALL = 21;
    public static final int FLIPPERS = 22;
    public static final int FLOOR = 23;
    public static final int FORCEFLOOR = 24;
    public static final int RANDOMFORCEFLOOR = 25;
    public static final int GLIDER = 26;
    public static final int GRAVEL = 27;
    public static final int GREENBUTTON = 28;
    public static final int GREENKEY = 29;
    public static final int GREENLOCK = 30;
    public static final int HIDDENWALL = 31;
    public static final int HINT = 32;
    public static final int ICE = 33;

    public static final int		ICEWALL_NW	= 62;
    public static final int	    ICEWALL_NE	= 63;
    public static final int	    ICEWALL_SW	= 64;
    public static final int	    ICEWALL_SE	= 65;

    //public static final int ICECORNER = 34;
    public static final int ICEBLOCK = 35;
    public static final int ICESKATES = 36;
    public static final int INVISIBLEWALL = 37;
    public static final int LOCK = 38;
    public static final int PARAMECIUM = 39;
    public static final int PINKBALL = 40;
    public static final int RECESSEDWALL = 41;
    public static final int REDBUTTON = 42;
    public static final int REDKEY = 43;
    public static final int REDLOCK = 44;
    public static final int SOCKET = 45;
    public static final int SUCTIONBOOTS = 46;
    public static final int SWIMMINGCHIP = 47;
    public static final int TANK = 48;
    public static final int TEETH = 49;
    public static final int TELEPORT = 50;
    public static final int THIEF = 51;
    //        public static final int THINWALL = 52;
    public static final int 	WALL_N		= 66;
    public static final int		WALL_W		= 67;
    public static final int	    WALL_S		= 68;
    public static final int	    WALL_E		= 52;
    public static final int WALL_SE = 53;
    public static final int TOGGLEWALLCLOSED = 54;
    public static final int TOGGLEWALLOPEN = 55;
    public static final int TOGGLEWALL = 69;
    public static final int TRAP = 56;
    public static final int WALKER = 57;
    public static final int WALL = 58;
    public static final int WATER = 59;
    public static final int YELLOWKEY = 60;
    public static final int YELLOWLOCK = 61;
}

/*
//known types for CC1 file data
public enum CC1Type {
    BORDERS(-2),
    EMPTY(-1),
    BLOB(0),
    BLOCK(1),
    BLUEBUTTON(2),
    BLUEKEY(3),
    BLUELOCK(4),
    BLUEWALLREAL(5),
    BLUEWALLFAKE(6),
    BOMB(7),
    BROWNBUTTON(8),
    BUG(9),
    BURNEDCHIP(10),
    PLAYER(11),
    CLONEBLOCK(12),
    CLONEMACHINE(13),
    COMPUTERCHIP(14),
    DIRT(15),
    DROWNEDCHIP(16),
    EXIT(17),
    FAKEEXIT(18),
    FIRE(19),
    FIREBOOTS(20),
    FIREBALL(21),
    FLIPPERS(22),
    FLOOR(23),
    FORCEFLOOR(24),
    RANDOMFORCEFLOOR(25),
    GLIDER(26),
    GRAVEL(27),
    GREENBUTTON(28),
    GREENKEY(29),
    GREENLOCK(30),
    HIDDENWALL(31),
    HINT(32),
    ICE(33),
    ICEWALL_NW(62),
    ICEWALL_NE(63),
    ICEWALL_SW(64),
    ICEWALL_SE(65),

    ICECORNER(34),
    ICEBLOCK(35),
    ICESKATES(36),
    INVISIBLEWALL(37),
    LOCK(38),
    PARAMECIUM(39),
    PINKBALL(40),
    RECESSEDWALL(41),
    REDBUTTON(42),
    REDKEY(43),
    REDLOCK(44),
    SOCKET(45),
    SUCTIONBOOTS(46),
    SWIMMINGCHIP(47),
    TANK(48),
    TEETH(49),
    TELEPORT(50),
    THIEF(51),
    //        THINWALL = 52;
    WALL_N(66),
    WALL_W(67),
    WALL_S(68),
    WALL_E(52),
    WALL_SE(53),
    TOGGLEWALLCLOSED(54),
    TOGGLEWALLOPEN(55),
    TOGGLEWALL(69),
    TRAP(56),
    WALKER(57),
    WALL(58),
    WATER(59),
    YELLOWKEY(60),
    YELLOWLOCK(61);

    private final int typeCode;

    CC1Type(int initCode) {
        typeCode = initCode;
    }

    public Boolean lessThanEmpty() {
        if (typeCode < GameData.Type.EMPTY) return true;
        return false;
    }
    //convienience method - have no idea why these should be used past refactoring
    public int getTypeCode() {
        return typeCode;
    }

}
 */