package org.porteousclan.chipsretro.game.data;

import com.badlogic.gdx.Gdx;
//import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;

public class Sounds {
	public  static boolean SoundOn = true;
	public  static boolean SoundEffectsOn = true;
	public  static float  volume = 1f;
	public  static float  effectsvolume = 1f;

	public static QueuedSound block;
	public static QueuedSound bomb;
	public static QueuedSound bump;
	public static QueuedSound chack;
	public static QueuedSound click;
	public static QueuedSound crackle;
	public static QueuedSound death;
	public static QueuedSound derezz;
	public static QueuedSound ding;
	public static QueuedSound force;
	public static QueuedSound oof;
	public static QueuedSound plip;
	public static QueuedSound popup;
	public static QueuedSound skate;
	public static QueuedSound skaturn;
	public static QueuedSound slurp;
	public static QueuedSound snick;
	public static QueuedSound socket;
	public static QueuedSound splash;
	public static QueuedSound tada;
	public static QueuedSound thief;
	public static QueuedSound tick;
	public static QueuedSound ting;
	public static QueuedSound traphit;
	public static QueuedSound whisk;
//	public static QueuedSound strike;

	public QueuedSound[] queuedSounds;
//	public  boolean MusicOn = true;
//	public  float  musicvolume = 1f;
//	public  Music music1;

	private class QueuedSound {
		Sound qsound;
		boolean play = false;

		public QueuedSound (String filename) {
			qsound = Gdx.audio.newSound(Gdx.files.internal("data/sounds/" + filename));
		}

		public void play() {
			play = true;
		}

		public void directPlay(boolean alwaystrue) {
			if (alwaystrue || (SoundOn && SoundEffectsOn))
			qsound.play(effectsvolume);
		}
		public void update(float dt) {
			//may find a use for dt - i.e. allow more than 1 play but limit
			if (play) {
				directPlay(false);
				play = false;
			}
		}
	}

	public void update(float dt) {
		for (QueuedSound mysound : queuedSounds) {
			mysound.update(dt);
		}

	}
	private static Sounds ourInstance = new Sounds();

	public static Sounds getInstance() {
		return ourInstance;
	}

	private Sounds() {

		block = new QueuedSound("block.ogg");
		bomb = new QueuedSound("bomb.ogg");
		bump = new QueuedSound("bump.ogg");
		chack = new QueuedSound("chack.ogg");
		click = new QueuedSound("click.ogg");
		crackle = new QueuedSound("crackle.ogg");
		death = new QueuedSound("death.ogg");
		derezz = new QueuedSound("derezz.ogg");
		ding = new QueuedSound("ding.ogg");
		force = new QueuedSound("force.ogg");
		oof = new QueuedSound("oof.ogg");
		plip = new QueuedSound("plip.ogg");
		popup = new QueuedSound("popup.ogg");
		skate = new QueuedSound("skate.ogg");
		skaturn = new QueuedSound("skaturn.ogg");
		slurp = new QueuedSound("slurp.ogg");
		snick = new QueuedSound("snick.ogg");
		socket = new QueuedSound("socket.ogg");
		splash = new QueuedSound("splash.ogg");
		tada = new QueuedSound("tada.ogg");
		thief = new QueuedSound("thief.ogg");
		tick = new QueuedSound("tick.ogg");
		ting = new QueuedSound("ting.ogg");
		traphit = new QueuedSound("traphit.ogg");
		whisk = new QueuedSound("whisk.ogg");
		queuedSounds = new QueuedSound[]{block,bomb,bump,chack,click,crackle,death,derezz,ding,force,oof,plip,popup,skate,skaturn,slurp,snick,socket,splash,tada,thief,tick,ting,traphit,whisk};
//		strike = new QueuedSound("strike.ogg");
//		music1 = loadMusic("piratemusic1.ogg");

	}
	

	public static void play (QueuedSound sound) {
			sound.play();
	}
	public  static void play (QueuedSound sound, boolean alwaystrue) {
			sound.directPlay(alwaystrue);
	}
	/** if we can load music */
/*	private  Music loadMusic(String filename) {
		try {
			return Gdx.audio.newMusic(Gdx.files.internal("data/music/" + filename));
		} catch (Throwable e) {
			MusicOn = false;
			return null;
		}
	}
	*/
	public static void soundOff() {
//		if (Sounds.music1 != null)
//			stopMusic(music1);
		SoundOn = false;
	}


	public static void soundOn() {
//		if (Sounds.music1 != null) {
			SoundOn = true;
//			play(music1);
//		}
	}


/*
	public  void play (Music music) {
		if (Sounds.music1 != null) {
			if (SoundOn && MusicOn) {
				music.setVolume(musicvolume);
				music.setLooping(true);
				music.play();
			}
		}
	}
	public  void stopMusic (Music music) {
		if (Sounds.music1 != null) {
			music.setVolume(musicvolume);
			music.pause();
		}
//			MusicOn = false; //?? 
	}
*/

	public static void setVolume(float avolume) {
		SoundOn = true;
		volume = avolume;
		effectsvolume = avolume;
//		musicvolume = avolume;
	}
	
}
