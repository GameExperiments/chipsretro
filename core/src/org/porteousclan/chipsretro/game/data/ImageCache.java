package org.porteousclan.chipsretro.game.data;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.utils.Array;

import org.porteousclan.chipsretro.DirectedGame;

public class ImageCache {
	public static Skin uiSkin;
	public static TextureAtlas skinAtlas;


	public static Texture sheet;
	public static TextureAtlas atlas;
	public static TextureAtlas numbersAtlas;
	public static TextureAtlas sidepanelAtlas;
	public static TextureAtlas splitcontrolAtlas;
	public static TextureAtlas buttonAtlas;
	private static Texture buttons;
	private static TextureRegion buttonRegion;
	private static Texture splash;
	private static TextureRegion splashRegion;
	private static Texture whiteness;
	private static TextureRegion whitenessRegion;
	
	public static Animation p_deathAnimation;
	public static Animation p_explodeAnimation;
	public static Animation p_burnAnimation;
	public static Animation p_drownAnimation;
	public static Animation p_finishAnimation;
	static Array<TextureAtlas.AtlasRegion> deathRegions;
	static Array<TextureAtlas.AtlasRegion> explodeRegions;
	static Array<TextureAtlas.AtlasRegion> drownRegions;
	static Array<TextureAtlas.AtlasRegion> burnRegions;
	static Array<TextureAtlas.AtlasRegion> finishRegions;
	
	public static void load (DirectedGame game) {
		GameData gameData = game.gameData;
		String textureFile = gameData.texturefolder + gameData.tileset2d;
		atlas = new TextureAtlas(Gdx.files.internal(textureFile), Gdx.files.internal(gameData.texturefolder));

		deathRegions = atlas.findRegions("chip_s");
		deathRegions.add(atlas.findRegion("chip_s"));
		p_deathAnimation = new Animation(0.2f, deathRegions); //getRegions());
		
		explodeRegions = atlas.findRegions("explode");
		explodeRegions.add(atlas.findRegion("floor"));
		p_explodeAnimation = new Animation(0.2f, explodeRegions );
		
		burnRegions = atlas.findRegions("chip_burn");
		//burnRegions.add(atlas.findRegion("fire"));
		p_burnAnimation = new Animation(0.2f, burnRegions);
		
		drownRegions = atlas.findRegions("chip_drowned");
		drownRegions.add(atlas.findRegion("water"));
		p_drownAnimation = new Animation(0.2f, drownRegions);
		
		finishRegions = atlas.findRegions("chip_exit");
		finishRegions.add(atlas.findRegion("exit"));
		p_finishAnimation = new Animation(0.2f, finishRegions);
		
		numbersAtlas = new TextureAtlas(gameData.texturefolder + gameData.numbers);
		sidepanelAtlas = new TextureAtlas(gameData.skinsfolder + gameData.sidepanel);
		splitcontrolAtlas = new TextureAtlas(gameData.skinsfolder + gameData.splitcontrol);
		skinAtlas = new TextureAtlas(gameData.skinsfolder + "uiskin.atlas");
		uiSkin = new Skin();
		ImageCache.uiSkin.add("default-font",gameData.scaledfont2);
		uiSkin.addRegions(skinAtlas);
		uiSkin.load(Gdx.files.internal(gameData.skinsfolder + "uiskin.json"));
//		uiSkin = new Skin(Gdx.files.internal(gameData.skinsfolder + "uiskin.json"));
//		uiSkin.addRegions(skinAtlas);

		whiteness = loadTexture(gameData.skinsfolder + "whiteness.png");
		whitenessRegion = new TextureRegion(whiteness, 0, 0, game.screenWidth , game.screenHeight);
		skinAtlas.addRegion("whiteness", whitenessRegion);
		
		splash = loadTexture(gameData.skinsfolder + "splash_screen.png");
		splashRegion = new TextureRegion(splash, 0, 0, 1024, 1024);
		skinAtlas.addRegion("splash", splashRegion);
		splash = loadTexture(gameData.skinsfolder + "gameback.png");
		splashRegion = new TextureRegion(splash, 0, 0, game.screenWidth, game.screenHeight);
		skinAtlas.addRegion("bg_screen", splashRegion);

        buttonAtlas = new TextureAtlas(gameData.skinsfolder + "buttons.atlas");

		buttons = loadTexture(gameData.skinsfolder + "level.png");
		buttonRegion = new TextureRegion(buttons, 0, 0, 163, 256 ); //70, 128);
		skinAtlas.addRegion("level_select", buttonRegion);
		
//		buttons = loadTexture(gameData.skinsfolder + "level_big.png");
//		buttonRegion = new TextureRegion(buttons, 0, 0, 140, 256);
//		skinAtlas.addRegion("big_level_select", buttonRegion);

//        buttons = loadTexture(gameData.skinsfolder + "level_big_half.png");
//        buttonRegion = new TextureRegion(buttons, 0, 0, 70, 128);
//        skinAtlas.addRegion("big_level_select2", buttonRegion);

		buttons = loadTexture(gameData.skinsfolder + "browser.png");
		buttonRegion = new TextureRegion(buttons, 0, 0, buttons.getWidth(),buttons.getHeight());
		skinAtlas.addRegion("browser_button", buttonRegion);

	}

	public static TextureRegion getTexture (String name) {
		TextureRegion regval=null;
		regval =  atlas.findRegion(name);
		if ( regval == null ) {
			regval =  buttonAtlas.findRegion(name);
		}
		if ( regval == null ) {
			regval =  skinAtlas.findRegion(name);
		}
		if ( regval == null ) {
			regval =  numbersAtlas.findRegion(name);
		}
		if ( regval == null ) {
			regval =  sidepanelAtlas.findRegion(name);
		}
		if ( regval == null ) {
			regval =  splitcontrolAtlas.findRegion(name);
		}
		return regval;
	}

	public static Image getImage(String name, float width, float height) {
		Image image = new Image(getTexture(name));
		image.setSize(width,height);
		return image;
	}
	
	public static TextureRegion getFrame (String name, int index) {
		TextureRegion regval=null;
		regval =  atlas.findRegion(name, index);
		if ( regval == null ) {
			regval =  buttonAtlas.findRegion(name);
		}
		if ( regval == null ) {
			regval =  skinAtlas.findRegion(name);
		}
		if ( regval == null ) {
			regval =  numbersAtlas.findRegion(name,index);
		}
		if ( regval == null ) {
			regval =  sidepanelAtlas.findRegion(name);
		}
		if ( regval == null ) {
			regval =  splitcontrolAtlas.findRegion(name);
		}
		return regval;
	}
	

	public static Texture loadTexture (String file) {
		return new Texture(Gdx.files.internal(file));
	}
	
}
