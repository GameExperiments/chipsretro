package org.porteousclan.chipsretro.game.gameLevels;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.utils.GdxRuntimeException;

import org.porteousclan.chipsretro.game.data.GameData;

import java.io.BufferedInputStream;
import java.io.IOException;

public class InputStreamFileLevelReader implements LevelFileReader {

    public static final int INTERNAL=0;
    public static final int LOCAL=1;
    public static final int EXTERNAL=2;
    
	private final FileHandle file;
	BufferedInputStream bis = null;

	private InputStreamFileLevelReader() {
		file = null;
	}

	private InputStreamFileLevelReader(FileHandle file) {
//		file = Gdx.files.internal(filename); //throws FileNotFoundException
		this.file = file; 
		bis = new BufferedInputStream(this.file.read());
		
	}
	private InputStreamFileLevelReader(String filename)
			throws GdxRuntimeException {
		switch (GameData.dataSetLocation) {
		case INTERNAL:
			file = Gdx.files.internal(filename); //throws FileNotFoundException
			break;
		case LOCAL:
//			break;
		default:
			file = Gdx.files.external(filename); //throws FileNotFoundException
			break;
		}
		bis = new BufferedInputStream(file.read());
	}

//	private InputStreamFileLevelReader(String filename, String path)
//			throws GdxRuntimeException {
//		file = Gdx.files.external(path + filename); //throws FileNotFoundException
//		bis = new BufferedInputStream(file.read());
//	}

	public static InputStreamFileLevelReader create(String filename) {
		try {
			return new InputStreamFileLevelReader(filename);
		} catch (GdxRuntimeException ex) {
			return null;
		}
	}
	public static InputStreamFileLevelReader create(FileHandle file) {
		try {
			return new InputStreamFileLevelReader(file);
		} catch (GdxRuntimeException ex) {
			return null;
		}
	}

	@Override
	protected void finalize() throws Throwable {
		try {
			bis.close();
		} catch (IOException e) {
		} finally {
			super.finalize();
		}
	}

	public int readUnsignedDWord() throws IOException { // Int is a 32-bit
														// signed two's
														// complement integer -
														// but may change
		byte[] b = new byte[4];
		try {
			bis.read(b, 0, 4);
		} catch (IOException e) {
		}
		int b1 = (int) b[0] & 0xff;
		int b2 = (int) b[1] & 0xff;
		int b3 = (int) b[2] & 0xff;
		int b4 = (int) b[3] & 0xff;
		int i = b1 << 24 | b2 << 16 | b3 << 8 | b4 << 0;
		return ByteSwapper.swap(i) & 0xFFFFFFFF; // readInt()
	}

	public int readUnsignedWord() throws IOException { // Short is a 16-bit
														// signed two's
														// complement integer
		byte[] b = new byte[2];
		try {
			bis.read(b, 0, 2);
		} catch (IOException e) {
		}
		int b1 = (int) b[0] & 0xff;
		int b2 = (int) b[1] & 0xff;
		short s = (short) (b1 << 8 | b2 << 0);
		return ByteSwapper.swap(s) & 0xFFFF; // readInt()
		// return ByteSwapper.swap(file.readShort()) & 0xFFFF;
	}

	public int readUnsignedByte() throws IOException { // Byte is 8-bit signed
														// two's complement
														// integer
		byte[] b = new byte[1];
		try {
			bis.read(b, 0, 1);
		} catch (IOException e) {
		}
		return b[0] & 0xff;
	}

	public void seek(long offset) throws IOException {
		int skippediszero = 0;
		try {
			bis.close();
			bis = new BufferedInputStream(file.read());
			// bis.skip(offset);
			long skipped = 0;
			while (skipped < offset && skippediszero < 3) { // try 3 times of zero if possible
				skipped += bis.skip(offset - skipped);
				if (skippediszero <= 0)
					skippediszero++;
			}
		} catch (IOException e) {
		}
		// Gdx.app.log("seek", "done - by closing opening and skipping");

		// bis.seek(offset);
	}

	public void skipBytes(int bytes) throws IOException {
		int skippediszero = 0;
		int skipped = 0;
		try {
			while (skipped < bytes && skippediszero < 3) { // try 3 times of zero if possible
				skipped += (int) bis.skip(bytes - skipped);
				if (skippediszero <= 0)
					skippediszero++;
			}
		} catch (IOException e) {
		}
	}

	public void close() throws IOException {
		try {
			bis.close();
		} catch (IOException e) {
		}
	}

	public void readFully(byte[] arr) throws IOException {
		try {
			bis.read(arr);
		} catch (IOException e) {
		}
	}
}
