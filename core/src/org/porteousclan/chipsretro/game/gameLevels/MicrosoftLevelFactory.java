package org.porteousclan.chipsretro.game.gameLevels;


import com.badlogic.gdx.Gdx;

import org.porteousclan.chipsretro.DirectedGame;
import org.porteousclan.chipsretro.game.data.DirectionStack;
import org.porteousclan.chipsretro.game.data.GameData;
import org.porteousclan.chipsretro.game.data.Type;
import org.porteousclan.chipsretro.game.elements.Block;
import org.porteousclan.chipsretro.game.elements.CloneBlock;
import org.porteousclan.chipsretro.game.elements.IceBlock;
import org.porteousclan.chipsretro.game.elements.Items.BlueKey;
import org.porteousclan.chipsretro.game.elements.Items.FireBoots;
import org.porteousclan.chipsretro.game.elements.Items.Flippers;
import org.porteousclan.chipsretro.game.elements.Items.GreenKey;
import org.porteousclan.chipsretro.game.elements.Items.IceSkates;
import org.porteousclan.chipsretro.game.elements.Items.RedKey;
import org.porteousclan.chipsretro.game.elements.Items.SuctionBoots;
import org.porteousclan.chipsretro.game.elements.Items.YellowKey;
import org.porteousclan.chipsretro.game.elements.Player;
import org.porteousclan.chipsretro.game.elements.TileSprite;
import org.porteousclan.chipsretro.game.elements.creature.Blob;
import org.porteousclan.chipsretro.game.elements.creature.Bug;
import org.porteousclan.chipsretro.game.elements.creature.FireBall;
import org.porteousclan.chipsretro.game.elements.creature.Glider;
import org.porteousclan.chipsretro.game.elements.creature.Paramecium;
import org.porteousclan.chipsretro.game.elements.creature.PinkBall;
import org.porteousclan.chipsretro.game.elements.creature.Tank;
import org.porteousclan.chipsretro.game.elements.creature.Teeth;
import org.porteousclan.chipsretro.game.elements.creature.Walker;
import org.porteousclan.chipsretro.game.elements.tiles.BlueButton;
import org.porteousclan.chipsretro.game.elements.tiles.BlueFakeWall;
import org.porteousclan.chipsretro.game.elements.tiles.BlueLock;
import org.porteousclan.chipsretro.game.elements.tiles.BlueWallReal;
import org.porteousclan.chipsretro.game.elements.tiles.Bomb;
import org.porteousclan.chipsretro.game.elements.tiles.BrownButton;
import org.porteousclan.chipsretro.game.elements.tiles.CloneMachine;
import org.porteousclan.chipsretro.game.elements.tiles.ComputerChip;
import org.porteousclan.chipsretro.game.elements.tiles.Dirt;
import org.porteousclan.chipsretro.game.elements.tiles.Exit;
import org.porteousclan.chipsretro.game.elements.tiles.FakeExit;
import org.porteousclan.chipsretro.game.elements.tiles.Fire;
import org.porteousclan.chipsretro.game.elements.tiles.Floor;
import org.porteousclan.chipsretro.game.elements.tiles.ForceFloor;
import org.porteousclan.chipsretro.game.elements.tiles.Gravel;
import org.porteousclan.chipsretro.game.elements.tiles.GreenButton;
import org.porteousclan.chipsretro.game.elements.tiles.GreenLock;
import org.porteousclan.chipsretro.game.elements.tiles.HiddenWall;
import org.porteousclan.chipsretro.game.elements.tiles.Hint;
import org.porteousclan.chipsretro.game.elements.tiles.Ice;
import org.porteousclan.chipsretro.game.elements.tiles.IceWallNE;
import org.porteousclan.chipsretro.game.elements.tiles.IceWallNW;
import org.porteousclan.chipsretro.game.elements.tiles.IceWallSE;
import org.porteousclan.chipsretro.game.elements.tiles.IceWallSW;
import org.porteousclan.chipsretro.game.elements.tiles.InvisibleWall;
import org.porteousclan.chipsretro.game.elements.tiles.RandomForceFloor;
import org.porteousclan.chipsretro.game.elements.tiles.RecessedWall;
import org.porteousclan.chipsretro.game.elements.tiles.RedButton;
import org.porteousclan.chipsretro.game.elements.tiles.RedLock;
import org.porteousclan.chipsretro.game.elements.tiles.Socket;
import org.porteousclan.chipsretro.game.elements.tiles.Teleport;
import org.porteousclan.chipsretro.game.elements.tiles.Thief;
import org.porteousclan.chipsretro.game.elements.tiles.ThinWall;
import org.porteousclan.chipsretro.game.elements.tiles.ToggleWall;
import org.porteousclan.chipsretro.game.elements.tiles.Trap;
import org.porteousclan.chipsretro.game.elements.tiles.Wall;
import org.porteousclan.chipsretro.game.elements.tiles.Water;
import org.porteousclan.chipsretro.game.elements.tiles.YellowLock;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

//import com.badlogic.gdx.Gdx;

/**
 * Level factory which uses CHIPS.DAT to create levels.
 */
public class MicrosoftLevelFactory extends LevelFactory {

//    private Map<Block.Type, Byte> msType = new HashMap<Block.Type, Byte>();
    private int levelCount = -1;
    private int magicNumber;
    private Map<Integer, Integer> levelOffsets = new HashMap<Integer, Integer>();
    private Map<String, Integer> passwordToLevel = new HashMap<String, Integer>();
    private Map<Integer, String> levelToPassword = new HashMap<Integer, String>();

    
    // Get block by object code
    public TileSprite getBlock(DirectedGame _game, int objCode, int x, int y, int layer) {
//      private int addBlock(DirectedGame _game, int objCode, int x, int y, int layer) {
//      MicrosoftBlockFactory f = MicrosoftBlockFactory.getInstance();
//    		Gdx.app.log("getBlock", "objCode " + objCode + " x=" + x + " y=" + y);
    	switch (objCode) {
        case 0x00:
//        	if (layer == GameData.LOWER_LAYER) return null; //ignor lower floor for now
//        	Gdx.app.log("", "floor x=" + x + " y=" + y + " layer =" + layer);
        	return new Floor(_game, x, y, Type.FLOOR, layer, GameData.NONE); //f.get(Type.FLOOR);
//		       	return null;
        case 0x01:
        	return new Wall(_game, x, y, Type.WALL, layer, GameData.NONE); //f.get(Type.WALL);
        case 0x02:
        	return new ComputerChip(_game, x, y, Type.COMPUTERCHIP, layer, GameData.NONE); //f.get(Type.COMPUTERCHIP);
        case 0x03:
        	return new Water(_game, x, y, Type.WATER, layer, GameData.NONE); //f.get(Type.WATER);
        case 0x04:
        	return new Fire(_game, x, y, Type.FIRE, layer, GameData.NONE); //f.get(Type.FIRE);
        case 0x05:
        	return new InvisibleWall(_game, x, y, Type.INVISIBLEWALL, layer, GameData.NONE); //f.get(Type.INVISIBLEWALL);
        case 0x06:
        	return new ThinWall(_game, x, y, Type.WALL_N, layer, GameData.NORTH); //f.get(Type.THINWALL, Moves.UP);
        case 0x07:
        	return new ThinWall(_game, x, y, Type.WALL_W, layer, GameData.WEST); //f.get(Type.THINWALL, Moves.LEFT);
        case 0x08:
        	return new ThinWall(_game, x, y, Type.WALL_S, layer, GameData.SOUTH); //f.get(Type.THINWALL, Moves.DOWN);
        case 0x09:
        	return new ThinWall(_game, x, y, Type.WALL_E, layer, GameData.EAST); //f.get(Type.THINWALL, Moves.RIGHT);
        case 0x0A:
            return new Block(_game, x, y, Type.BLOCK, layer, GameData.NONE); //f.get(Type.BLOCK);
        case 0x0B:
            return new Dirt(_game, x, y, Type.DIRT, layer, GameData.NONE); //f.get(Type.DIRT);
        case 0x0C:
            return new Ice(_game, x, y, Type.ICE, layer, GameData.NONE); //f.get(Type.ICE);
        case 0x0D:
            return new ForceFloor(_game, x, y, Type.FORCEFLOOR, layer, GameData.SOUTH); //f.get(Type.FORCEFLOOR, Moves.DOWN);
        case 0x0E:
            return new CloneBlock(_game, x, y, Type.CLONEBLOCK, layer, GameData.NORTH); //f.get(Type.BLOCK, Moves.UP); // Cloning block UP
        case 0x0F:
            return new CloneBlock(_game, x, y, Type.CLONEBLOCK, layer, GameData.WEST); //f.get(Type.BLOCK, Moves.LEFT); // Cloning block LEFT
        case 0x10:
            return new CloneBlock(_game, x, y, Type.CLONEBLOCK, layer, GameData.SOUTH); //f.get(Type.BLOCK, Moves.DOWN); // Cloning block DOWN
        case 0x11:
            return new CloneBlock(_game, x, y, Type.CLONEBLOCK, layer, GameData.EAST); //f.get(Type.BLOCK, Moves.RIGHT); // Cloning block RIGHT
        case 0x12:
            return new ForceFloor(_game, x, y, Type.FORCEFLOOR, layer, GameData.NORTH); //f.get(Type.FORCEFLOOR, Moves.UP);
        case 0x13:
            return new ForceFloor(_game, x, y, Type.FORCEFLOOR, layer, GameData.EAST); //f.get(Type.FORCEFLOOR, Moves.RIGHT);
        case 0x14:
            return new ForceFloor(_game, x, y, Type.FORCEFLOOR, layer, GameData.WEST); //f.get(Type.FORCEFLOOR, Moves.LEFT);
        case 0x15:
//        	Gdx.app.log("MicroSoftLevelFactory", "exit(15) x=" + x + " y=" + y + " layer =" + layer);
            return new Exit(_game, x, y, Type.EXIT, layer, GameData.NONE); //f.get(Type.EXIT);
        case 0x16:
            return new BlueLock(_game, x, y, Type.BLUELOCK, layer, GameData.NONE); //f.get(Type.BLUELOCK);
        case 0x17:
            return new RedLock(_game, x, y, Type.REDLOCK, layer, GameData.NONE); //f.get(Type.REDLOCK);
        case 0x18:
            return new GreenLock(_game, x, y, Type.GREENLOCK, layer, GameData.NONE); //f.get(Type.GREENLOCK);
        case 0x19:
            return new YellowLock(_game, x, y, Type.YELLOWLOCK, layer, GameData.NONE); //f.get(Type.YELLOWLOCK);
        case 0x1A:
            return new IceWallNW(_game, x, y, Type.ICEWALL_NW, layer, GameData.EAST); //f.get(Type.ICECORNER, Moves.UP);
        case 0x1B:
            return new IceWallNE(_game, x, y, Type.ICEWALL_NE, layer, GameData.NORTH); //f.get(Type.ICECORNER, Moves.LEFT);
        case 0x1C:
            return new IceWallSE(_game, x, y, Type.ICEWALL_SE, layer, GameData.SOUTH); //f.get(Type.ICECORNER, Moves.DOWN);
        case 0x1D:
            return new IceWallSW(_game, x, y, Type.ICEWALL_SW, layer, GameData.WEST); //f.get(Type.ICECORNER, Moves.RIGHT);
        case 0x1E:
            return new BlueFakeWall(_game, x, y, Type.BLUEWALLFAKE, layer, GameData.NONE); //f.get(Type.BLUEWALLFAKE);
        case 0x1F:
            return new BlueWallReal(_game, x, y, Type.BLUEWALLREAL, layer, GameData.NONE); //f.get(Type.BLUEWALLREAL);
        case 0x20:
        	// NOT USED - transparency buffer - however can be a cool block to show wierd effects
        	// i dont use tansparency this way so for now will use a wall then try implement later
            //return null; 
//        	return new Wall("explode", 1,_game, x, y, Type.WALL, layer, GameData.NONE); //f.get(Type.WALL);
        	return new Wall(_game, x, y, Type.WALL, layer, GameData.NONE); //f.get(Type.WALL);
        case 0x21:
            return new Thief(_game, x, y, Type.THIEF, layer, GameData.NONE); //f.get(Type.THIEF);
        case 0x22:
            return new Socket(_game, x, y, Type.SOCKET, layer, GameData.NONE); //f.get(Type.SOCKET);
        case 0x23:
//        	Gdx.app.log("getBlock", "green button at x=" + x + " y=" + y);
//        	Gdx.app.log("MicroSoftLevelFactory", "green button x=" + x + " y=" + y + " layer =" + layer);
            return new GreenButton(_game, x, y, Type.GREENBUTTON, layer, GameData.NONE); //f.get(Type.GREENBUTTON);
        case 0x24:
//        	Gdx.app.log("getBlock", "red button");
            return new RedButton(_game, x, y, Type.REDBUTTON, layer, GameData.NONE); //f.get(Type.REDBUTTON);
            
/**  FLIPPING TOGGLE WALLS TO TEST AN IDEA */
        case 0x25:
            return new ToggleWall(_game, x, y, Type.TOGGLEWALL, layer, GameData.NORTH); //f.get(Type.TOGGLEWALLCLOSED);
        case 0x26:
            return new ToggleWall(_game, x, y, Type.TOGGLEWALL, layer, GameData.SOUTH); //f.get(Type.TOGGLEWALLOPEN);

        case 0x27:
//        	Gdx.app.log("getBlock", "brown button");
            return new BrownButton(_game, x, y, Type.BROWNBUTTON, layer, GameData.NONE); //f.get(Type.BROWNBUTTON);
        case 0x28:
//        	Gdx.app.log("getBlock", "blue button");
            return new BlueButton(_game, x, y, Type.BLUEBUTTON, layer, GameData.NONE); //f.get(Type.BLUEBUTTON);
        case 0x29:
//        	Gdx.app.log("getBlock", "teleport");
            return new Teleport(_game, x, y, Type.TELEPORT, layer, GameData.NONE); //f.get(Type.TELEPORT);
        case 0x2A:
            return new Bomb(_game, x, y, Type.BOMB, layer, GameData.NONE); //f.get(Type.BOMB);
        case 0x2B:
            return new Trap(_game, x, y, Type.TRAP, layer, GameData.NONE); //f.get(Type.TRAP);
        case 0x2C:
            return new HiddenWall(_game, x, y, Type.HIDDENWALL, layer, GameData.NONE); //f.get(Type.HIDDENWALL);
        case 0x2D:
            return new Gravel(_game, x, y, Type.GRAVEL, layer, GameData.NONE); //f.get(Type.GRAVEL);
        case 0x2E:
//        	Gdx.app.log("MicroSoftLevelFactory", "recessed wall");
            return new RecessedWall(_game, x, y, Type.RECESSEDWALL, layer, GameData.NONE); //f.get(Type.RECESSEDWALL);
        case 0x2F:
            return new Hint(_game, x, y, Type.HINT, layer, GameData.NONE); //f.get(Type.HINT);
        case 0x30:
            return new ThinWall(_game, x, y, Type.WALL_SE, layer, GameData.NONE); //f.get(Type.THINWALLSE);
        case 0x31:
            return new CloneMachine(_game, x, y, Type.CLONEMACHINE, layer, GameData.NONE); //f.get(Type.CLONEMACHINE);
        case 0x32:
            return new RandomForceFloor(_game, x, y, Type.RANDOMFORCEFLOOR, layer, GameData.NONE); //f.get(Type.RANDOMFORCEFLOOR);
        case 0x33:
        	return new Player("chip_drowned", _game, x, y, Type.DROWNEDCHIP, layer, GameData.SOUTH); //f.get(Type.SWIMMINGCHIP, Moves.DOWN);
        case 0x34:
        	return new Player("chip_burn", 1, _game, x, y, Type.BURNEDCHIP, layer, GameData.SOUTH); //f.get(Type.SWIMMINGCHIP, Moves.DOWN);
        case 0x35:
        	Gdx.app.log("MicroSoftLevelFactory", "player(35) x=" + x + " y=" + y + " layer =" + layer);
        	return new Player("chip_burn", 2, _game, x, y, Type.BURNEDCHIP, layer, GameData.SOUTH); //f.get(Type.SWIMMINGCHIP, Moves.DOWN);
        case 0x36:
//        	Gdx.app.log("MicroSoftLevelFactory", "0x36");
//            return null; // Not used
        	
        	return new Wall("explode", 1,_game, x, y, Type.WALL, layer, GameData.NONE); //f.get(Type.WALL);
        case 0x37:
//        	Gdx.app.log("MicroSoftLevelFactory", "0x37");
//            return null; // Not used
        	return new Wall("explode", 2,_game, x, y, Type.WALL, layer, GameData.NONE); //f.get(Type.WALL);
        case 0x38:
            return new IceBlock(_game, x, y, Type.ICEBLOCK, layer, GameData.NONE); //f.get(Type.BLOCK);
//        	Gdx.app.log("MicroSoftLevelFactory", "0x38");
//            return null; // Not used
        case 0x39:
//        	Gdx.app.log("MicroSoftLevelFactory", "0x39");
//            return null; // Chip in Exit
        	return new Wall("chip_exit", 1,_game, x, y, Type.WALL, layer, GameData.NONE); //f.get(Type.WALL);
        case 0x3A:
 //       	Gdx.app.log("MicroSoftLevelFactory", "exit(3a) x=" + x + " y=" + y + " layer =" + layer);
        	//return new Wall("chip_exit", 2,_game, x, y, Type.WALL, layer, GameData.NONE); //f.get(Type.WALL);
            return new FakeExit(_game, x, y, Type.EXIT, layer, GameData.NONE); //f.get(Type.EXIT); // Exit-End DirectedGame
        case 0x3B:
 //       	Gdx.app.log("MicroSoftLevelFactory", "exit(3b) x=" + x + " y=" + y + " layer =" + layer);
        	//return new Wall("chip_exit", 3,_game, x, y, Type.WALL, layer, GameData.NONE); //f.get(Type.WALL);
            return new FakeExit(_game, x, y, Type.EXIT, layer, GameData.NONE); //f.get(Type.EXIT); // Exit-End DirectedGame
        case 0x3C:
//        	Gdx.app.log(" add PLAYER ", " NORTH ");
//        	Gdx.app.log("MicroSoftLevelFactory", "player(3C) x=" + x + " y=" + y + " layer =" + layer);
        	return new Player("chip_swim_n", _game, x, y, Type.SWIMMINGCHIP, layer, GameData.NORTH); //f.get(Type.SWIMMINGCHIP, Moves.DOWN);
        case 0x3D:
//        	Gdx.app.log(" add PLAYER ", " WEST ");
//        	Gdx.app.log("MicroSoftLevelFactory", "player(3D) x=" + x + " y=" + y + " layer =" + layer);
        	return new Player("chip_swim_w", _game, x, y, Type.SWIMMINGCHIP, layer, GameData.WEST); //f.get(Type.SWIMMINGCHIP, Moves.DOWN);
        case 0x3E:
//        	Gdx.app.log(" add PLAYER ", " SOUTH ");
//        	Gdx.app.log("MicroSoftLevelFactory", "player(3E) x=" + x + " y=" + y + " layer =" + layer);
        	return new Player("chip_swim_s", _game, x, y, Type.SWIMMINGCHIP, layer, GameData.SOUTH); //f.get(Type.SWIMMINGCHIP, Moves.DOWN);
        case 0x3F:
//        	Gdx.app.log(" add PLAYER ", " EAST ");
//        	Gdx.app.log("MicroSoftLevelFactory", "player(3F) x=" + x + " y=" + y + " layer =" + layer);
        	return new Player("chip_swim_e", _game, x, y, Type.SWIMMINGCHIP, layer, GameData.EAST); //f.get(Type.SWIMMINGCHIP, Moves.DOWN);
        case 0x40:
            return new Bug(_game, x, y, Type.BUG, layer, GameData.NORTH, true); //f.get(Type.BUG, Moves.UP);
        case 0x41:
            return new Bug(_game, x, y, Type.BUG, layer, GameData.WEST, true); //f.get(Type.BUG, Moves.LEFT);
        case 0x42:
            return new Bug(_game, x, y, Type.BUG, layer, GameData.SOUTH, true); //f.get(Type.BUG, Moves.DOWN);
        case 0x43:
            return new Bug(_game, x, y, Type.BUG, layer, GameData.EAST, true); //f.get(Type.BUG, Moves.RIGHT);
        case 0x44:
//        	Gdx.app.log(" add fireball ", " NORTH ");
        	return new FireBall(_game, x, y, Type.FIREBALL, layer, GameData.NORTH, true); //f.get(Type.FIREBALL, Moves.UP);
        case 0x45:
//        	Gdx.app.log(" add fireball ", " WEST ");
        	return new FireBall(_game, x, y, Type.FIREBALL, layer, GameData.WEST, true); //f.get(Type.FIREBALL, Moves.LEFT);
        case 0x46:
//        	Gdx.app.log(" add fireball ", " SOUTH ");
            return new FireBall(_game, x, y, Type.FIREBALL, layer, GameData.SOUTH, true); //f.get(Type.FIREBALL, Moves.DOWN);
        case 0x47:
//        	Gdx.app.log(" add fireball ", " EAST ");
            return new FireBall(_game, x, y, Type.FIREBALL, layer, GameData.EAST, true); //f.get(Type.FIREBALL, Moves.RIGHT);
        case 0x48:
            return new PinkBall(_game, x, y, Type.PINKBALL, layer, GameData.NORTH, true); //f.get(Type.PINKBALL, Moves.UP);
        case 0x49:
            return new PinkBall(_game, x, y, Type.PINKBALL, layer, GameData.WEST, true); //f.get(Type.PINKBALL, Moves.LEFT);
        case 0x4A:
            return new PinkBall(_game, x, y, Type.PINKBALL, layer, GameData.SOUTH, true); //f.get(Type.PINKBALL, Moves.DOWN);
        case 0x4B:
            return new PinkBall(_game, x, y, Type.PINKBALL, layer, GameData.EAST, true); //f.get(Type.PINKBALL, Moves.RIGHT);
        case 0x4C:
            return new Tank(_game, x, y, Type.TANK, layer, GameData.NORTH, true); //f.get(Type.TANK, Moves.UP);
        case 0x4D:
            return new Tank(_game, x, y, Type.TANK, layer, GameData.WEST, true); //f.get(Type.TANK, Moves.LEFT);
        case 0x4E:
            return new Tank(_game, x, y, Type.TANK, layer, GameData.SOUTH, true); //f.get(Type.TANK, Moves.DOWN);
        case 0x4F:
            return new Tank(_game, x, y, Type.TANK, layer, GameData.EAST, true); //f.get(Type.TANK, Moves.RIGHT);
        case 0x50:
            return new Glider(_game, x, y, Type.GLIDER, layer, GameData.NORTH, true); //f.get(Type.GLIDER, Moves.UP);
        case 0x51:
            return new Glider(_game, x, y, Type.GLIDER, layer, GameData.WEST, true); //f.get(Type.GLIDER, Moves.LEFT);
        case 0x52:
            return new Glider(_game, x, y, Type.GLIDER, layer, GameData.SOUTH, true); //f.get(Type.GLIDER, Moves.DOWN);
        case 0x53:
            return new Glider(_game, x, y, Type.GLIDER, layer, GameData.EAST, true); //f.get(Type.GLIDER, Moves.RIGHT);
        case 0x54:
            return new Teeth(_game, x, y, Type.TEETH, layer, GameData.NORTH, true); //f.get(Type.TEETH, Moves.UP);
        case 0x55:
            return new Teeth(_game, x, y, Type.TEETH, layer, GameData.WEST, true); //f.get(Type.TEETH, Moves.LEFT);
        case 0x56:
            return new Teeth(_game, x, y, Type.TEETH, layer, GameData.SOUTH, true); //f.get(Type.TEETH, Moves.DOWN);
        case 0x57:
            return new Teeth(_game, x, y, Type.TEETH, layer, GameData.EAST, true); //f.get(Type.TEETH, Moves.RIGHT);
        case 0x58:
            return new Walker(_game, x, y, Type.WALKER, layer, GameData.NORTH, true); //f.get(Type.WALKER, Moves.UP);
        case 0x59:
            return new Walker(_game, x, y, Type.WALKER, layer, GameData.WEST, true); //f.get(Type.WALKER, Moves.LEFT);
        case 0x5A:
            return new Walker(_game, x, y, Type.WALKER, layer, GameData.SOUTH, true); //f.get(Type.WALKER, Moves.DOWN);
        case 0x5B:
            return new Walker(_game, x, y, Type.WALKER, layer, GameData.EAST, false); //f.get(Type.WALKER, Moves.RIGHT);
        case 0x5C:
            return new Blob(_game, x, y, Type.BLOB, layer, GameData.NORTH, true); //f.get(Type.BLOB, Moves.UP);
        case 0x5D:
            return new Blob(_game, x, y, Type.BLOB, layer, GameData.WEST, true); //f.get(Type.BLOB, Moves.LEFT);
        case 0x5E:
            return new Blob(_game, x, y, Type.BLOB, layer, GameData.SOUTH, true); //f.get(Type.BLOB, Moves.DOWN);
        case 0x5F:
            return new Blob(_game, x, y, Type.BLOB, layer, GameData.EAST, true); //f.get(Type.BLOB, Moves.RIGHT);
        case 0x60:
            return new Paramecium(_game, x, y, Type.PARAMECIUM, layer, GameData.NORTH, true); //f.get(Type.PARAMECIUM, Moves.UP);
        case 0x61:
            return new Paramecium(_game, x, y, Type.PARAMECIUM, layer, GameData.WEST, true); //f.get(Type.PARAMECIUM, Moves.LEFT);
        case 0x62:
            return new Paramecium(_game, x, y, Type.PARAMECIUM, layer, GameData.SOUTH, true); //f.get(Type.PARAMECIUM, Moves.DOWN);
        case 0x63:
            return new Paramecium(_game, x, y, Type.PARAMECIUM, layer, GameData.EAST, true); //f.get(Type.PARAMECIUM, Moves.RIGHT);
        case 0x64:
            return new BlueKey(_game, x, y, Type.BLUEKEY, layer, GameData.NONE); //f.get(Type.BLUEKEY);
        case 0x65:
            return new RedKey(_game, x, y, Type.REDKEY, layer, GameData.NONE); //f.get(Type.REDKEY);
        case 0x66:
            return new GreenKey(_game, x, y, Type.GREENKEY, layer, GameData.NONE); //f.get(Type.GREENKEY);
        case 0x67:
            return new YellowKey(_game, x, y, Type.YELLOWKEY, layer, GameData.NONE); //f.get(Type.YELLOWKEY);
        case 0x68:
            return new Flippers(_game, x, y, Type.FLIPPERS, layer, GameData.NONE); //f.get(Type.FLIPPERS);
        case 0x69:
            return new FireBoots(_game, x, y, Type.FIREBOOTS, layer, GameData.NONE); //f.get(Type.FIREBOOTS);
        case 0x6A:
            return new IceSkates(_game, x, y, Type.ICESKATES, layer, GameData.NONE); //f.get(Type.ICESKATES);
        case 0x6B:
            return new SuctionBoots(_game, x, y, Type.SUCTIONBOOTS, layer, GameData.NONE); //f.get(Type.SUCTIONBOOTS);
        case 0x6C:
 //       	Gdx.app.log("MicroSoftLevelFactory", "player(6C) x=" + x + " y=" + y + " layer =" + layer);
        	return new Player("chip_n", _game, x, y, Type.PLAYER, layer, GameData.NORTH); //f.get(Type.SWIMMINGCHIP, Moves.DOWN);
        case 0x6D:
 //       	Gdx.app.log("MicroSoftLevelFactory", "player(6D) x=" + x + " y=" + y + " layer =" + layer);
        	return new Player("chip_w", _game, x, y, Type.PLAYER, layer, GameData.WEST); //f.get(Type.SWIMMINGCHIP, Moves.DOWN);
        case 0x6E:
 //       	Gdx.app.log("MicroSoftLevelFactory", "player(6E) x=" + x + " y=" + y + " layer =" + layer);
        	return new Player("chip_s", _game, x, y, Type.PLAYER, layer, GameData.SOUTH); //f.get(Type.SWIMMINGCHIP, Moves.DOWN);
        case 0x6F:
 //       	Gdx.app.log("MicroSoftLevelFactory", "player(6F) x=" + x + " y=" + y + " layer =" + layer);
        	return new Player("chip_e", _game, x, y, Type.PLAYER, layer, GameData.EAST); //f.get(Type.SWIMMINGCHIP, Moves.DOWN);
        case 0x70:
//        	Gdx.app.log("MicroSoftLevelFactory", "0x39");
//            return null; // Chip in Exit
        	return new Wall(_game, x, y, Type.WALL, layer, GameData.NONE); //f.get(Type.WALL);
        }
    	
        return null;
    }
    
    private LevelFileReader chipDat = null;

    public MicrosoftLevelFactory(LevelFileReader lfr) throws IOException {
            chipDat = lfr;
            magicNumber = chipDat.readUnsignedDWord();
            //MS/LYNX/ROMMY-FILES are chucks files different? //Rommys will be
            if (magicNumber != 0x0002AAAC && magicNumber != 0x0003AAAC && magicNumber != 0x0102AAAC && magicNumber != 0x0103AAAC) {
                throw new IOException("Couldn't parse file");
            }
            levelCount = chipDat.readUnsignedWord();
            levelOffsets.put(1, 6);
            //chipDat.close();
    }

    @Override
    protected void finalize() throws Throwable {
        try {
            chipDat.close();
        } catch (Exception e) {
        } finally {
            super.finalize();
        }
    }
    
    public String getLevelName(DirectedGame _game, int n) {
    	String levelName = "";
        if (n < 1 || n > levelCount) {
            throw new IllegalArgumentException("Level outside of range");
        }

        try {
            int offset = getLevelOffset(n);
            chipDat.seek(offset);

//            int numBytesLevel = chipDat.readUnsignedWord();
            chipDat.skipBytes(10);
            int numberOfBytesLayer1 = chipDat.readUnsignedWord();
            chipDat.skipBytes(numberOfBytesLayer1);
            int numberOfBytesLayer2 = chipDat.readUnsignedWord();
            chipDat.skipBytes(numberOfBytesLayer2);
            int numBytesOptional = chipDat.readUnsignedWord();
            int numOptionalBytesRead = 0;            
            while (numOptionalBytesRead < numBytesOptional) {

                int fieldType = chipDat.readUnsignedByte();
                int sizeOfField = chipDat.readUnsignedByte();
                numOptionalBytesRead += 2;
//            	Gdx.app.log("MicroSoftLevelFactory", "field type " + fieldType);

                switch (fieldType) {
                
                    case 0x03: // level name
                        byte[] ASCIITitle = new byte[sizeOfField - 1];
                        chipDat.readFully(ASCIITitle);
                        levelName = new String(ASCIITitle, "ASCII");
                        chipDat.skipBytes(1);
                        break;
                    default:
                        chipDat.skipBytes(sizeOfField);
                }
                numOptionalBytesRead += sizeOfField;
            }
        } catch (Exception ex) {
//            ex.printStackTrace();
//        	Gdx.app.log("MicroSoftLevelFactory", "While loading level: ", ex);
//            System.out.println("While loading level: " + ex.getMessage());
        } finally {
        }
            
        return levelName;
    }


    private static MicrosoftLevelFactory mInstance = null;

    //@SuppressWarnings("finally")
	public GameLevel getLevel(DirectedGame _game, int n) {
        if (n < 1 || n > levelCount) {
//        	Gdx.app.log("Level outside of range", "level wanted = " + n);
            throw new IllegalArgumentException("Level outside of range");
        }
//        int width = 32;
//        int height = 32;
        GameLevel ret = null;
        try {
            int offset = getLevelOffset(n);
            chipDat.seek(offset);

            int numBytesLevel = chipDat.readUnsignedWord();

            // So we don't have to skip over the same level again later
            levelOffsets.put(n + 1, offset + numBytesLevel + 2);

            int levelNumber = chipDat.readUnsignedWord();
            int numSeconds = chipDat.readUnsignedWord();
            int numChipsNeeded = chipDat.readUnsignedWord();
            chipDat.readUnsignedWord();

            _game.gameData.level = levelNumber;
            _game.gameData.chips = numChipsNeeded;
            if (numSeconds < 1 ) { numSeconds = -1; } //use dashes
            _game.gameData.timeleft = numSeconds;
            ret = new GameLevel(_game);

            int numberOfBytesLayer1 = chipDat.readUnsignedWord();
            readLayer(_game, numberOfBytesLayer1, GameData.UPPER_LAYER); // Layer 1, upper

            int numberOfBytesLayer2 = chipDat.readUnsignedWord();
            readLayer(_game, numberOfBytesLayer2, GameData.LOWER_LAYER); // Layer 2, lower
            
            int numBytesOptional = chipDat.readUnsignedWord();
            int numOptionalBytesRead = 0;            
            int len = _game.gameData._elements.size();
            TileSprite sprite1, sprite2;
            while (numOptionalBytesRead < numBytesOptional) {

                int fieldType = chipDat.readUnsignedByte();
                int sizeOfField = chipDat.readUnsignedByte();
                numOptionalBytesRead += 2;
//            	Gdx.app.log("MicroSoftLevelFactory", "field type " + fieldType);

                switch (fieldType) {
                
                case 0x01: //time
//                	Gdx.app.log("MicroSoftLevelFactory", "time?");
                	break;
                case 0x02: //chips needed
//                	Gdx.app.log("MicroSoftLevelFactory", "chips needed?");
                	break;

                    case 0x03: // level name
                        byte[] ASCIITitle = new byte[sizeOfField - 1];
                        chipDat.readFully(ASCIITitle);
                        _game.gameData.levelname = new String(ASCIITitle, "ASCII");
                        chipDat.skipBytes(1);
                        break;

                    case 0x04: // traps from and to (brown button and traps)
                        for (int i = 0; i < sizeOfField / 10; i++) {
                            int buttonX = chipDat.readUnsignedWord();
                            int buttonY = chipDat.readUnsignedWord();
                            int trapX = chipDat.readUnsignedWord();
                            int trapY = chipDat.readUnsignedWord();

                           //Gdx.app.log("button connect", "Attempt to find brown");
                            sprite1 = null;
                            sprite2 = null;
                            for (int xi = 0; xi < len; xi++) {
                            	if ((DirectionStack.getElement(_game,xi).currentGridX == buttonX+1) && (DirectionStack.getElement(_game,xi).currentGridY == buttonY+1)) {
                            		if (DirectionStack.getElement(_game,xi).type == Type.BROWNBUTTON)
                            			sprite1 = DirectionStack.getElement(_game,xi);
                            	}
                            	if ((DirectionStack.getElement(_game,xi).currentGridX == trapX+1) && (DirectionStack.getElement(_game,xi).currentGridY == trapY+1)) {
                            		if (DirectionStack.getElement(_game,xi).type == Type.TRAP)
                            			sprite2 = DirectionStack.getElement(_game,xi);
                            	}
                            }
                            if (sprite1 != null && sprite2 != null) {
                            	sprite1.joinTiles(sprite2, GameData.NONE, GameData.NONE );
                            }
                            chipDat.skipBytes(2);
                        }
                        break;

                    case 0x05: //cloners from and to (red button and cloner)
                        for (int i = 0; i < sizeOfField / 8; i++) {
                            int buttonX = chipDat.readUnsignedWord();
                            int buttonY = chipDat.readUnsignedWord();
                            int clonerX = chipDat.readUnsignedWord();
                            int clonerY = chipDat.readUnsignedWord();

                            //Gdx.app.log("button connect", "Attempt to find red");
                            sprite1 = null;
                            sprite2 = null;
                            for (int xi = 0; xi < len; xi++) {
                            	if ((DirectionStack.getElement(_game,xi).currentGridX == buttonX+1) && (DirectionStack.getElement(_game,xi).currentGridY == buttonY+1)) {
                            		if (DirectionStack.getElement(_game,xi).type == Type.REDBUTTON)
                            			sprite1 = DirectionStack.getElement(_game,xi);
                            	}
                            	if ((DirectionStack.getElement(_game,xi).currentGridX == clonerX+1) && (DirectionStack.getElement(_game,xi).currentGridY == clonerY+1)) {
                            		if (DirectionStack.getElement(_game,xi).type == Type.CLONEMACHINE || DirectionStack.getElement(_game,xi).type == Type.CLONEBLOCK)
                            			sprite2 = DirectionStack.getElement(_game,xi);
                            		//else
                                        //Gdx.app.log("button connect", "well its not clone machine or clone block its " + DirectionStack.getElement(_game,xi).type);
                            			
                            	}
                            }
                            if (sprite1 != null && sprite2 != null) {
                            	sprite1.joinTiles(sprite2, GameData.NONE, GameData.NONE );
                            }
                        }
                        break;

                    case 0x06: // Password
                        String password = readPassword(sizeOfField);
                        ret.setPassword(password);
                        passwordToLevel.put(password, n);
                        levelToPassword.put(n, password);
                        chipDat.skipBytes(1);
                        break;

                    case 0x07: // Hint
                        byte[] ASCIIHint = new byte[sizeOfField - 1];
                        chipDat.readFully(ASCIIHint);
                        _game.gameData.levelhint = new String(ASCIIHint, "ASCII");
                        chipDat.skipBytes(1);
                        break;
                        
//                    case 0x08: //password

                    case 0x0A: // creature list = creatures that can move
                        for (int i = 0; i < sizeOfField / 2; i++) {
                            int creatureX = chipDat.readUnsignedByte();
                            int creatureY = chipDat.readUnsignedByte();
//                            Gdx.app.log("Creature list", "Attempt to add creature to move list");
                            sprite1 = null;
                            sprite2 = null;
                            for (int xi = 0; xi < len; xi++) {
                            	if ((DirectionStack.getElement(_game,xi).currentGridX == creatureX+1) && (DirectionStack.getElement(_game,xi).currentGridY == creatureY+1)) {
//                            		if (DirectionStack.getElement(_game,xi).type == GameData.Type.REDBUTTON) 
                            			sprite1 = DirectionStack.getElement(_game,xi);
                            			sprite1.setInMoveList();
//                                    	Gdx.app.log("supported Monster List added", "creature" + sprite1.type + " X=" + creatureX + " Y" + creatureY);
                            			break;
//                            		if (DirectionStack.getElement(_game,xi).type == GameData.Type.CLONEBLOCK || DirectionStack.getElement(_game,xi).type == GameData.Type.CLONEMACHINE) 
//                            			sprite2 = DirectionStack.getElement(_game,xi);
                            	}
                            }
                        }
                        break;

                    default:
                    	Gdx.app.log("MicroSoftLevelFactory", "Skipping Data size=" + sizeOfField);
                        chipDat.skipBytes(sizeOfField);
                }
                numOptionalBytesRead += sizeOfField;
            }
        } catch (Exception ex) {
//            ex.printStackTrace();
//        	Gdx.app.log("MicroSoftLevelFactory", "While loading level: ", ex);
//            System.out.println("While loading level: " + ex.getMessage());
        } finally {
        }
    	if( _game.gameData.chips > _game.gameData.chipscount) {
    		_game.gameData.chips = _game.gameData.chipscount; //just make sure we can do it
    	}
        return ret;
    }

    private String readPassword(int length) throws IOException {
        byte[] ASCIIPassword = new byte[length - 1];
        chipDat.readFully(ASCIIPassword);
        for (int i = 0; i < ASCIIPassword.length; i++) {
            ASCIIPassword[i] ^= 0x99;
        }
        String password = new String(ASCIIPassword, "ASCII");
        return password;
    }

    public void readLayer(DirectedGame _game, int numberOfBytes, int layer) throws IOException, BlockContainerFullException {
    	TileSprite sprite;
        int width = GameData.totaltilesX; // * _game.gameData.totaltilesY;
        //int height = GameData.totaltilesY;
        int bytesRead = 0;
        int objectsPlaced = 0;
        if (magicNumber == 0x0002AAAC || magicNumber == 0x0003AAAC || magicNumber == 0x0102AAAC || magicNumber == 0x0103AAAC) {
        	width = 32;
        //	height = 32;
        }
        
        while (bytesRead < numberOfBytes) {
            int read = chipDat.readUnsignedByte();
            bytesRead++;
            if (read >= 0x00 && read <= 0x70) {
            	//don't want floor(0) we just draw that
            	sprite = getBlock(_game, read,(objectsPlaced % width) + 1, (objectsPlaced / width) + 1, layer);
        		objectsPlaced++;
            	if (sprite != null) {
                    if (sprite.type == Type.COMPUTERCHIP)
                        _game.gameData.chipscount++;
                    if (sprite.type == Type.HINT)
                        _game.gameData.hint = (Hint)sprite;
            		_game.gameData._elements.add(sprite);
            	}
            } else if (read == 0xFF) {
                int numRepeats = chipDat.readUnsignedByte();
                int data = chipDat.readUnsignedByte();
                bytesRead += 2;
                while (numRepeats-- > 0) { //
                	sprite = getBlock(_game, data, (objectsPlaced % width) + 1, (objectsPlaced / width) + 1, layer);
            		objectsPlaced++;
                	if (sprite != null) {
                        if (sprite.type == Type.COMPUTERCHIP)
                            _game.gameData.chipscount++;
                        if (sprite.type == Type.HINT)
                            _game.gameData.hint = (Hint)sprite;
                		_game.gameData._elements.add(sprite);
                	}
                }
            } else {
            	objectsPlaced++;
    			Gdx.app.log("Not PARSEd", " value=" + read);
                System.out.println("Object I couldn't parse: " + read);
            }
        }
    }

    @Override
    public int getLastLevelNumber() {
        return levelCount;
    }

    public int getLevelOffset(int n) throws IOException {
        Integer offset = levelOffsets.get(n);
        if (offset != null) {
            return offset;
        }
        int level = n - 1;
        while (level >= 1) {
            offset = levelOffsets.get(level);
            if (offset != null) {
                break;
            }
            level--;
        }
        chipDat.seek(offset);
        while (level < n) {
            int numBytesInLevel = chipDat.readUnsignedWord();
            offset = offset + numBytesInLevel + 2;
            levelOffsets.put(level + 1, offset);
            level++;
            if (level == n) {
                break;
            }
            chipDat.skipBytes(numBytesInLevel);
        }
        return offset;
    }

    public String getLevelPassword(int n) throws IOException {
        String pass = levelToPassword.get(n);
        if (pass != null) {
            return pass;
        }
        chipDat.seek(getLevelOffset(n));

        chipDat.skipBytes(10);
        int numBytesFirstLayer = chipDat.readUnsignedWord();
        chipDat.skipBytes(numBytesFirstLayer);
        int numBytesSecondLayer = chipDat.readUnsignedWord();
        chipDat.skipBytes(numBytesSecondLayer);
        int numBytesOptional = chipDat.readUnsignedWord();
        int readOptional = 0;
        while (readOptional < numBytesOptional) {
            int fieldType = chipDat.readUnsignedByte();
            int fieldLength = chipDat.readUnsignedByte();
            readOptional += 2;
            if (fieldType == 0x06) {
                String password = readPassword(fieldLength);
                levelToPassword.put(n, password);
                passwordToLevel.put(password, n);
                return password;
            } else {
                chipDat.skipBytes(fieldLength);
            }
            readOptional += fieldLength;
        }
        return null;
    }
    @Override
    protected int getLevelNumberByPassword(String pass) {
        try {
            Integer level = null;

            // Check if password has already been seen
            level = passwordToLevel.get(pass);
            if (level != null) {
                return level;
            }

            // Password has not yet been seen, check all levels we have not yet seen
            for (int i = 1; i <= levelCount; i++) {
                String password = getLevelPassword(i);
                if (password != null && password.equals(pass)) {
                    return i;
                }
            }
        } catch (IOException ex) {
            System.out.println("While getting level by password: " + pass + ":" + ex.getMessage());
        }
        return -1;
    }

	public static MicrosoftLevelFactory getmInstance() {
		return mInstance;
	}

	public static void setmInstance(MicrosoftLevelFactory mInstance) {
		MicrosoftLevelFactory.mInstance = mInstance;
	}
}
