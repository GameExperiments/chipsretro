package org.porteousclan.chipsretro.game.gameLevels;

import org.porteousclan.chipsretro.DirectedGame;

public abstract class LevelFactory {

    public abstract GameLevel getLevel(DirectedGame _game, int n);

    public abstract int getLastLevelNumber();

    protected abstract int getLevelNumberByPassword(String pass);

    public final GameLevel getLevelByPassword(DirectedGame _game, String pass) {
        int n = getLevelNumberByPassword(pass);
        if (n == -1) {
            return null;
        }
        return getLevel(_game, n);
    }
}
