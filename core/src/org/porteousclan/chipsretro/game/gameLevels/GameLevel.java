package org.porteousclan.chipsretro.game.gameLevels;

import com.badlogic.gdx.Gdx;

import org.porteousclan.chipsretro.DirectedGame;
import org.porteousclan.chipsretro.game.data.DirectionStack;
import org.porteousclan.chipsretro.game.data.GameData;
import org.porteousclan.chipsretro.game.data.Type;
import org.porteousclan.chipsretro.game.elements.Player;
import org.porteousclan.chipsretro.game.elements.TileSprite;

import java.util.ArrayList;

public class GameLevel {
    protected DirectedGame _game;
	/**
	 * create playing area and sets it relative to the physical drawing area - I
	 * hope
	 */
	public GameLevel(DirectedGame game) {
        _game = game;
		game.gameData._elements = new ArrayList<TileSprite>();
	}

	//@Override
	public void reset() {
		int len = _game.gameData._elements.size();
		for (int i = 0; i < len; i++) {
			DirectionStack.getElement(_game,i).reset();
		}
	}

	public void setPassword(String passwd) {
        _game.gameData.levelPassword = passwd;
    }

    public void connectItems() {
        _game.gameData._player = findPlayer();
        connectTeleports();
        brownButtonsAndTraps();
        redButtonsClones();
        greenBlueRedBrown(); //wraps up but needs to be broken up

    }

    /*******************************
     * find item and connect
     */

    private Player findPlayer() {
        Player player;
        int 		len = _game.gameData._elements.size();
        //connect chip to player - reverse order - upper layer first
        for (int i = len - 1; i >= 0; i--) {
            if (DirectionStack.getElement(_game,i).type == Type.PLAYER && DirectionStack.getElement(_game,i)._layer >= GameData.UPPER_LAYER) {
                player = (Player)DirectionStack.getElement(_game,i);
                player.isRealChip(true);
                player.setInMoveList();
                return player;
            }
        }

        //ok so look if went swimming
        for (int i = len - 1; i >= 0; i--) {
            if (DirectionStack.getElement(_game,i).type == Type.SWIMMINGCHIP) {
                player = (Player)DirectionStack.getElement(_game,i);
                player.isRealChip(true);
                player.setInMoveList();
                return player;
            }
        }

        //cant find any so make one!
        player = new Player("chip_s", _game, 1, 1, Type.PLAYER, GameData.VISITOR_LAYER, GameData.SOUTH);
        _game.gameData._elements.add(player);
        player.isRealChip(true);
        player.setInMoveList();
        return player;
    }

    private void connectTeleports() {
        int len= _game.gameData._elements.size();
        TileSprite element = null;
        TileSprite sprite = null;
        //connect teleports (if any)
        //*******************************************
        for (int i = len - 1; i >= 0; i--) {
            element = null;
            element = DirectionStack.getElement(_game,i);
            if (element.type == Type.TELEPORT) {
                if (sprite == null) {
                    sprite = element;
                } else {
//					Gdx.app.log("GameScreen", "join teleports");
                    sprite.joinTiles(element, GameData.SOUTH, GameData.SOUTH);
                    sprite.joinTiles(element, GameData.NORTH, GameData.NORTH);
                    sprite.joinTiles(element, GameData.EAST, GameData.EAST);
                    sprite.joinTiles(element, GameData.WEST, GameData.WEST);
                    sprite = element;
                }
            }
        }

        //and connect first to last
        if (sprite != null) {
            for (int i = len - 1; i >= 0; i--) {
                element = null;
                element = DirectionStack.getElement(_game,i);
                if (element.type == Type.TELEPORT) {
//					Gdx.app.log("GameScreen", "join teleports - last");
                    sprite.joinTiles(element, GameData.SOUTH, GameData.SOUTH);
                    sprite.joinTiles(element, GameData.NORTH, GameData.NORTH);
                    sprite.joinTiles(element, GameData.EAST, GameData.EAST);
                    sprite.joinTiles(element, GameData.WEST, GameData.WEST);
                    break; // only one this loop
                }
            }
        }
    }

    public void brownButtonsAndTraps() {
        int len= _game.gameData._elements.size();
        TileSprite element = null;
        TileSprite sprite = null;
        //*************************************************************************
        //Potential disaster - brown buttons may or may not be connected (LYNX)
        //*************************************************************************
        for (int i = 0; i < len; i++) {
            //element =null;
            element = DirectionStack.getElement(_game,i);
            if (element.type != Type.BROWNBUTTON || element.isJoined) continue;
            for (int xi = i+1; xi < len; xi++) {
//                sprite = null;
                sprite = DirectionStack.getElement(_game,xi);
                if (!sprite.isJoined && sprite.type == Type.TRAP) {
                    element.joinTiles(sprite, GameData.NONE, GameData.NONE);
                    Gdx.app.log("brown button FWD", " joined next trap" + sprite.type + " x " + sprite.currentGridX + " y " + sprite.currentGridY);
                    break;
                }
            }
            if (element.isJoined) continue;
            //could wrap but doing back search instead (change??)
            for (int xi = i-1; xi >= 0; xi--) {
            //    sprite = null;
                sprite = DirectionStack.getElement(_game,xi);
                if (!sprite.isJoined && sprite.type == Type.TRAP) {
                    element.joinTiles(sprite, GameData.NONE, GameData.NONE);
//                    Gdx.app.log("brown button BCK", " joined next trap" + sprite.type + " x " + sprite.currentGridX + " y " + sprite.currentGridY);
                    break;
                }
            }
        }

    }

    private void redButtonsClones() {
        int len= _game.gameData._elements.size();
        TileSprite element = null;
        TileSprite sprite = null;

        //*************************************************************************
        //Potential disaster - red buttons may or may not be connected (LYNX)
        //*************************************************************************
        //first clone machines
        for (int i = 0; i < len; i++) {
//            element =null;
            element = DirectionStack.getElement(_game,i);
            if (element.type != Type.REDBUTTON || element.isJoined) continue;
            for (int xi = i+1; xi < len; xi++) {
//                sprite = null;
                sprite = DirectionStack.getElement(_game,xi);
                if (!sprite.isJoined && sprite.type == Type.CLONEMACHINE) {
                    element.joinTiles(sprite, GameData.NONE, GameData.NONE);
                    Gdx.app.log("red button FWD", " joined next clone machine" + sprite.type + " x " + sprite.currentGridX + " y " + sprite.currentGridY);
                    break;
                }
            }
            if (element.isJoined) continue;
            //could wrap but doing back search instead (change??)
            for (int xi = i-1; xi >= 0; xi--) {
//                sprite = null;
                sprite = DirectionStack.getElement(_game,xi);
                if (!sprite.isJoined && sprite.type == Type.CLONEMACHINE) {
                    element.joinTiles(sprite, GameData.NONE, GameData.NONE);
                    Gdx.app.log("red button BCK", " joined next clone machine" + sprite.type + " x " + sprite.currentGridX + " y " + sprite.currentGridY);
                    break;
                }
            }
        }
        //now clone blocks if we still have red buttons
        for (int i = 0; i < len; i++) {
//            element =null;
            element = DirectionStack.getElement(_game,i);
            if (element.type != Type.REDBUTTON || element.isJoined) continue;
            for (int xi = i+1; xi < len; xi++) {
//                sprite = null;
                sprite = DirectionStack.getElement(_game,xi);
                if (!sprite.isJoined && sprite.type == Type.CLONEBLOCK) {
                    element.joinTiles(sprite, GameData.NONE, GameData.NONE);
                    Gdx.app.log("red button FWD", " joined next clone block" + sprite.type + " x " + sprite.currentGridX + " y " + sprite.currentGridY);
                    break;
                }
            }
            if (element.isJoined) continue;
            //could wrap but doing back search instead (change??)
            for (int xi = i-1; xi >= 0; xi--) {
//                sprite = null;
                sprite = DirectionStack.getElement(_game,xi);
                if (!sprite.isJoined && sprite.type == Type.CLONEBLOCK) {
                    element.joinTiles(sprite, GameData.NONE, GameData.NONE);
                    Gdx.app.log("red button BCK", " joined next clone block" + sprite.type + " x " + sprite.currentGridX + " y " + sprite.currentGridY);
                    break;
                }
            }
        }

    }

    //wrap up. why not move where relevant
    private void greenBlueRedBrown() {
        int len= _game.gameData._elements.size();
        TileSprite element = null;
        TileSprite sprite = null;

        //green buttons and togglewalls or bluebuttons and tanks
        for (int i = 0; i < len; i++) {
            element = DirectionStack.getElement(_game,i);
            if (element.type == Type.GREENBUTTON
                    || element.type == Type.BLUEBUTTON
                    || element.type == Type.REDBUTTON
                    || element.type == Type.BROWNBUTTON ) {
                for (int xi = 0; xi < len; xi++) {
                    sprite = DirectionStack.getElement(_game,xi);
                    if ((sprite.type == Type.TOGGLEWALL && element.type == Type.GREENBUTTON)
                            || (sprite.type == Type.TANK && element.type == Type.BLUEBUTTON)) {
                        element.joinTiles(sprite, GameData.NONE, GameData.NONE);
                    }
                    if (sprite.currentGridX == element.currentGridX && sprite.currentGridY == element.currentGridY) {
                        element.stoodon = sprite; //PREP BUTTONS (YES/NO?)
                    }
                }
            }

            //red buttons should be joined when reading file - so set type
            if (element.type == Type.CLONEMACHINE) {
                for (int xi = 0; xi < len; xi++) {
                    sprite = DirectionStack.getElement(_game,xi);
                    if (sprite.currentGridX == element.currentGridX && sprite.currentGridY == element.currentGridY) {
                        if (sprite.isCloneable()) {
                                element.setCloneType(sprite.type);
                            element.direction = sprite.lastdirection;
                            sprite.active = false;
                        }
                    }
                }
            }

            if (element.type == Type.CLONEBLOCK) {
                element.setCloneType(Type.BLOCK);
                //element.direction = sprite.direction;
            }
        }
    }

    /*******************************/
    /** Add all background elements */
	//@Override
	public void update(float dt) {
		
		//super.update(dt);
		
		int len = _game.gameData._elements.size();
        int lenf = _game.gameData._floors.size();
        int lenC = _game.gameData._creatureMoveOrderList.size();
		int i;

		if ((_game.gameData._player.dead || _game.gameData._player.goalreached) && _game.gameData.gameMode != DirectedGame.GAME_STATE_PAUSE) {
			_game.gameData.gameMode = DirectedGame.GAME_STATE_ANIMATE;
		}
		
		if (_game.gameData.gameMode == DirectedGame.GAME_STATE_PLAY) {

			/** clear grid layers and place objects fresh */
			DirectionStack.reSetAllTiles(_game.gameData._elements);
			
			for (i = 0; i < len; i++) {
				TileSprite sprite = DirectionStack.getElement(_game,i);
				if (!sprite.active) {
					continue;
				}
				sprite._index = i;
                if (!_game.gameData._player.hadFirstMove) {
                        if (sprite.type == Type.BROWNBUTTON ) {
                            int upperId = DirectionStack.getUpperId(_game, sprite, GameData.NONE);
                            TileSprite upperSprite = DirectionStack.getElement(_game, upperId);
                            if (upperId > Type.EMPTY && upperSprite._index != 0 && upperSprite != sprite && !upperSprite.isItem()) {
//                                Gdx.app.log("brown button triggered ", " id=" + sprite._index + " type " + upperSprite.type);
                                sprite.stoodOn(upperSprite,true);

                        }
                    }
                }
				
				/* look for queued moves */
				int tempLayer = sprite._layer;
				
				/** THIS IS IMPORTANT **/
                if (!sprite.moving && _game.gameData._player.hadFirstMove && sprite.canMove) {
                    sprite.checkMove.attemptMove(); //ms style does move wanted here
                    sprite.isFirstMove = false;
                }
				
				//clear and move in grid - for remaining elements
				DirectionStack.setTile(tempLayer, sprite.currentGridX,sprite.currentGridY, GameData.NONE);
				sprite.place();
				DirectionStack.setTile(sprite._layer, sprite.currentGridX,sprite.currentGridY, i);
			}
		}

		if (_game.gameData._player.hadFirstMove) {
            //move the creature move list first
            for (i = 0; i < lenC; i++) {
                TileSprite sprite = _game.gameData._creatureMoveOrderList.get(i);
                if (!sprite.active) {
                    continue;
                }
                if (sprite.isCreature() && sprite.canMove) //temporary check
                    sprite.update(dt); //non ms style does move wanted here
            }

            for (i = 0; i < len; i++) {
                TileSprite sprite = DirectionStack.getElement(_game,i);
                if (!sprite.active) {
                    continue;
                }
                if (!(sprite.isCreature() && sprite.canMove) )
                    sprite.update(dt); //non ms style does move wanted here
            }
            for (i = 0; i < lenf; i++) {
                TileSprite sprite = _game.gameData._floors.get(i);
                if (!sprite.active) {
                    continue;
                }
                sprite.update(dt);
            }
        }
	}
	


}
