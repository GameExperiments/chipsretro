package org.porteousclan.chipsretro.game.buttons;

import org.porteousclan.chipsretro.DirectedGame;
import org.porteousclan.chipsretro.game.data.GameData;
import org.porteousclan.chipsretro.screens.GameScreen;
import org.porteousclan.chipsretro.screens.OptionsScreen;
import org.porteousclan.chipsretro.screens.ScrollMenu;
import org.porteousclan.chipsretro.screens.ScrolledLevelChoiceScreen;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;

/**
 * TO to hold item's data
 * 
 * @author Richard
 * 
 */
public final class Item extends Button {
	public final static int LEVEL_TYPE=0;
    public final static int WORLD_TYPE=1;
    public final static int EXT_URL_TYPE=2;

	DirectedGame _game = null;
	private String levelname;
    private int lives_used=-1;
    private int time_left=-1;
    private int type;
    private int location;

	private int id; //stores level number
	private Label description;
	private Label info;
	private Image image;
	private Color color;
	ScrollMenu callingMenu;


	public Item(DirectedGame game, int index, int type, int location, String title, TextureRegion textureRegion, Color color, Skin skin, BitmapFont font1, BitmapFont font2, ScrollMenu callingMenu) {
		_game = game;
		this.callingMenu = callingMenu;
		this.id = index;
		this.type = type;
		this.location = location;
		//****ITEM HEIGHT*****//
		float height;
		float screenheightIN = Gdx.graphics.getHeight() / Gdx.graphics.getPpiY();
		float maxSizeHeightItemIN=1.8f;//GameData.magic_screen_height;
		if (screenheightIN > maxSizeHeightItemIN) {
			height = maxSizeHeightItemIN*Gdx.graphics.getPpiY();
		}
		else {
			height = game.screenHeight * (screenheightIN/maxSizeHeightItemIN);
		}
		//*********//
		float width;
		width = height * 0.7f;
		this.image = new Image(textureRegion);
		this.image.setSize(width,height);
		this.color = color;

		image.setColor(color);
		this.setSkin(skin);
		this.setStyle(new ButtonStyle());

		this.description = new Label(  "" + title.trim(), skin);
		this.description.setStyle(new Label.LabelStyle(font1,Color.WHITE));
		this.description.setWrap(true);
		this.description.setAlignment(Align.top | Align.center);
		this.description.setWidth(image.getWidth());

		this.info = new Label(" ", skin);
		this.info.setStyle(new Label.LabelStyle(font2,Color.WHITE));
		this.info.setWrap(true);
		this.info.setAlignment(Align.top | Align.center); //Align.bottom |
		this.info.setWidth(image.getWidth());
		
		Table labelTable = new Table();
		labelTable.defaults().pad(0, 2, 1, 2);
//		labelTable.row();
		labelTable.add(this.description).align(Align.top| Align.center).minWidth(image.getWidth()).minHeight(image.getHeight()*1/3).fill();
		labelTable.row(); //.padTop(10);
		labelTable.add(this.info).align(Align.top | Align.center).minWidth(image.getWidth()).minHeight(image.getHeight()*3/8).fill();

		this.stack(image, labelTable).expand().fill();
		this.addListener(new levelClickListener(this));
	}

	/** time, lives  */
	public void setInfo(int levelnum, int time_left, int lives_used, String additionalInfo) {
		int time_bonus=0, level_bonus=0;
		if (time_left > 0) {
			this.time_left = time_left;
		} else {
			this.time_left = 0;
		}
		this.lives_used = lives_used;
		if(levelnum == this.id) {
			if (time_bonus > 0)
				time_bonus = this.time_left*10;
			level_bonus = this.id* 500;
			if (lives_used > 0) {
				level_bonus = (int) (level_bonus * ( 0.8 * this.lives_used ));
				if (level_bonus < 500) level_bonus = 500;
			}
			if (this.time_left > 0)
				this.info.setText(GameData.getLanguageString("Lives")+ "=" + this.lives_used + "\n" + GameData.getLanguageString("Time") + "=" + this.time_left + "\n" + GameData.getLanguageString("Score") + "=" + (int)(time_bonus + level_bonus) + "\n" + additionalInfo);
			else
				this.info.setText(GameData.getLanguageString("Lives")+ "=" + this.lives_used + "\n" + GameData.getLanguageString("Score") + "=" + (int)(time_bonus + level_bonus) + "\n" + additionalInfo);
		}

	}

	public void setWorldInfo(int doneLevels, int levels) {
		this.info.setText(GameData.getLanguageString("Completed") + "=" + doneLevels + "\n" + GameData.getLanguageString("Levels") + "=" + levels);
	}
	public void setLevelname(String levelname) {
		this.levelname = levelname;
	}


	public int getType() {
		return type;
	}




	private final class levelClickListener extends ClickListener {
		private Item item;

		public levelClickListener(Item item) {
			this.item = item;
		}

		public void clicked(InputEvent event, float x, float y) {
			//Gdx.app.log("SELECTED ITEM", "ID: " + item.getId() + " Description: " + item.getDescription().getText());
			GameData.dataSetLocation = item.location;
			callingMenu.saveScrollPosition();
			//LEVEL TYPE or WORLD TYPE
			switch (item.getType()) {
				case Item.LEVEL_TYPE:
					_game.gameData.lastLevel = _game.gameData.level = item.getId();
					_game.setScreen(new GameScreen(_game));
					break;
				case Item.WORLD_TYPE:
					_game.gameData.lastDataSet = item.levelname;
					_game.gameData.lastLevel = _game.gameData.level = 1;
					_game.setScreen(new ScrolledLevelChoiceScreen(_game));
					break;
				case Item.EXT_URL_TYPE:
					Gdx.net.openURI(item.levelname);
					//_game.setOldscreen("DownloadWarningScreen");
					_game.setScreen(new OptionsScreen(_game));
					break;
			}
		}
	}


	public final int getId() {
		return id;
	}

	public final Label getDescription() {
		return description;
	}

	public final Image getImage() {
		return image;
	}
	public final float getWidth() { return image.getWidth();}
	public final float getHeight() { return image.getHeight(); }


	public final Color getColor() {
		return color;
	}


}
