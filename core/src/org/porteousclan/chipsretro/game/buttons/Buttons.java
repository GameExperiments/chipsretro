package org.porteousclan.chipsretro.game.buttons;

import org.porteousclan.chipsretro.DirectedGame;
import org.porteousclan.chipsretro.game.data.GameData;
import org.porteousclan.chipsretro.game.data.ImageCache;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;

public class Buttons extends Actor{

	public TextButton myButton;
	public boolean visible = true;

	public Buttons(DirectedGame game, String type, float x, float y, float width, float height) {
		makeButton(game,type,x,y);
		if (getWidth() < width )
			myButton.setWidth(width);
		if (getHeight() < height )
			myButton.setHeight(height);
//		resize(width, height);
	}

	public Buttons(DirectedGame game, String type, float x, float y) {
		makeButton(game,type,x,y);
	}

	private void makeButton(DirectedGame game, String type, float x, float y){
//		TextButton.TextButtonStyle style = new TextButton.TextButtonStyle();
		TextButton.TextButtonStyle style = ImageCache.uiSkin.get(TextButton.TextButtonStyle.class);
		style.font = game.gameData.scaledfont3;
		style.fontColor = Color.WHITE;
		myButton = new TextButton( GameData.getLanguageString(type),  style);
		myButton.setSkin(ImageCache.uiSkin);
		myButton.setPosition(x,y);
	}

	public void reMidPosition(float x, float y) {
		myButton.setPosition(x-myButton.getWidth()/2,y-myButton.getHeight()/2);
	}

	public void resize(float width, float height) {
		if (height != 0)
			myButton.setHeight(height);
		if (width != 0)
			myButton.setWidth(width);
	}
	public float right () {
		return myButton.getX() + myButton.getWidth();
	}
	public float top () {
		return myButton.getY() + myButton.getHeight();
	}
	public float left () {
		return myButton.getX();
	}
	public float bottom () {
		return myButton.getY();
	}
	public float getWidth(){
		return myButton.getWidth();
	}
	public float getHeight(){
		return myButton.getHeight();
	}
	public Rectangle bounds () {
		return new Rectangle(left(), bottom(), getWidth(), getHeight());
	}

	public Boolean contains(Vector3 tp) {
		if (visible && bounds().contains(tp.x,tp.y))
			return true;
		else
			return false;
	}
	public void draw (SpriteBatch spriteBatch) {
		if (visible)
			myButton.draw(spriteBatch, 1f);

	}

}
