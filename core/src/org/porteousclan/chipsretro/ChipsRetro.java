package org.porteousclan.chipsretro;

import org.porteousclan.chipsretro.screens.ScrolledWorldChoiceScreen;

public class ChipsRetro extends DirectedGame {

    public ChipsRetro() {
        super();
	}

	@Override
	public void create() {	
		super.create();
		setScreen(new ScrolledWorldChoiceScreen(this));
	}

}
