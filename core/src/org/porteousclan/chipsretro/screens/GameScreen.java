package org.porteousclan.chipsretro.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.viewport.ScreenViewport;

import org.porteousclan.chipsretro.DirectedGame;
import org.porteousclan.chipsretro.game.buttons.Buttons;
import org.porteousclan.chipsretro.game.data.DirectionStack;
import org.porteousclan.chipsretro.game.data.GameData;
import org.porteousclan.chipsretro.game.data.ImageCache;
import org.porteousclan.chipsretro.game.data.LevelScores;
import org.porteousclan.chipsretro.game.data.Preferences;
import org.porteousclan.chipsretro.game.data.Sounds;
import org.porteousclan.chipsretro.game.data.Type;
import org.porteousclan.chipsretro.game.elements.TileSprite;
import org.porteousclan.chipsretro.game.elements.tiles.Floor;
import org.porteousclan.chipsretro.game.elements.tiles.Wall;
import org.porteousclan.chipsretro.game.gameLevels.GameLevel;
import org.porteousclan.chipsretro.game.gameLevels.InputStreamFileLevelReader;
import org.porteousclan.chipsretro.game.gameLevels.MicrosoftLevelFactory;
import org.porteousclan.chipsretro.game.gui.ChipsLeft;
import org.porteousclan.chipsretro.game.gui.Controls;
import org.porteousclan.chipsretro.game.gui.Inventory;
import org.porteousclan.chipsretro.game.gui.Level;
import org.porteousclan.chipsretro.game.gui.TimeBar;
import org.porteousclan.chipsretro.util.AutoButtonSpacer;
import org.porteousclan.chipsretro.util.CameraHelper;

public class GameScreen extends AbstractScreen {

	public SpriteBatch spriteBatch;
    public OrthographicCamera cameraHUD;
	public OrthographicCamera cameraGUI;
    public OrthographicCamera cameraGame;
//    public OrthographicCamera cameraBG;
	//public Viewport viewport;
	public Stage hudStage = null;
    public Stage guiStage = null;

	private Sprite interruptionBackground;
    Sprite spriteBG;
    private float bgTextureWidth, bgTextureHeight;

    Buttons levelCompletedButton;
	Buttons back_button;
	Buttons pause_button;
	Buttons play_rommy_button;
    Buttons play_ms_button;
    Buttons play_lynx_button;
	Buttons playnext_button; //?? add in ?
	Buttons repeat_button;
	Buttons save_button;
	Buttons soundsoff_button;
	Buttons soundson_button;

    //private Touchpad touchpad;

	private Controls _controls;
	private Vector3 _touchPoint;
	private Level _level;
	private ChipsLeft _chipsleft;
	CharSequence str[];

//	Boolean draw_hint_text = false;
//    float hint_text_timer = 0;
    Boolean draw_timeOut_text = false;
    float timeOut_text_timer = 0;
	Boolean draw_name_text = false;
	//Boolean draw_death_text = false;
    float level_timer=0f;
    float frame_time=0.05f;
//    float tick_time = 0.1f;
//    float move_time = 0.2f;

    int time_bonus;
	int level_bonus;

	boolean mykeypress = false;
	int mykeydirection = GameData.NONE;

	private GameLevel _gamelevel;
	private MicrosoftLevelFactory mslf = null;

    public Sounds sounds = Sounds.getInstance();

	public GameScreen(DirectedGame game) {
		super(game);
        Gdx.input.setCatchBackKey(game.gameData.catchBackKey);
    }

    @Override
    public void show() {
        spriteBatch = new SpriteBatch();
        showGame();
        showGUI();
        showHUD();
        _game.gameData.lives = 0;
    }

    private void showHUD() {
        cameraHUD = new OrthographicCamera(_game.screenHUDWidth,_game.screenHUDHeight);// _game.screenWidth * aspectRatio);
        cameraHUD.position.set(0,0,0);

        hudStage = new Stage(new ScreenViewport(cameraHUD));
//        Gdx.input.setInputProcessor(hudStage);
        cameraHUD.update();
        createHUDItems(_game.screenHUDWidth,_game.screenHUDHeight);
    }

    private void showGUI() {
        cameraGUI = new OrthographicCamera(_game.screenWidth, _game.screenHeight);// _game.screenWidth * aspectRatio);
        cameraGUI.position.set(0,0,0);

        guiStage = new Stage(new ScreenViewport(cameraGUI));
        Gdx.input.setInputProcessor(guiStage);
        cameraGUI.update();
        createItems(_game.screenWidth,_game.screenHeight);
    }

    private void createHUDItems(float width, float height){

        _level = new Level(_game, width*0.88f,height*0.91f, _game.gameData.numbers_prefix + "number");
        _chipsleft = new ChipsLeft(_game, width*0.88f,height*0.82f, _game.gameData.numbers_prefix + "number");
        if (_game.gameData._timebar == null) {
            _game.gameData._timebar = new TimeBar(_game, width * 0.88f, height * 0.73f, _game.gameData.numbers_prefix + "number", _game.gameData.timeleft);
        }

        if (_game.gameData._inventory == null) {
            _game.gameData._inventory = new Inventory(_game, width * 0.65f, height * 0.91f);
        }

    }

    private void createItems(float width, float height){

        _touchPoint = new Vector3();

        interruptionBackground = new Sprite(ImageCache.getTexture("whiteness"));
        interruptionBackground.setBounds(0, 0,width,height);


        show_controls(width, height);

        levelCompletedButton = new Buttons(_game,"level_select",width*0.5f,height*0.5f);
        levelCompletedButton.resize(width*0.5f,height*0.8f);
        levelCompletedButton.reMidPosition(width*0.5f,height*0.5f);
        float buttonHeightMultiplier = 0.05f;

//        float buttonMultiplyer=1;
//        float buttonOffset=0.05f;
//        float newPosX;
        AutoButtonSpacer aBs = new AutoButtonSpacer(width,6);
        float buttonHeight = height/10;//(width/4) - (width*0.05f);
        float buttonWidth = aBs.getwidth(); //width/(numButtons+1);//(width/4) - (width*0.05f);

//        newPosX = (buttonMultiplyer * width) + (buttonMultiplyer * buttonOffset);

        back_button = new Buttons(_game, "back_button", aBs.getPos(1),height*buttonHeightMultiplier, buttonWidth, buttonHeight );

        play_rommy_button = new Buttons(_game, "play_rommy_button",aBs.getPos(2),height*buttonHeightMultiplier, buttonWidth, buttonHeight);
        play_lynx_button = new Buttons(_game, "play_lynx_button",aBs.getPos(3),height*buttonHeightMultiplier, buttonWidth, buttonHeight);
        play_ms_button = new Buttons(_game, "play_ms_button",aBs.getPos(4),height*buttonHeightMultiplier, buttonWidth, buttonHeight);

        playnext_button = new Buttons(_game, "next_page_button",aBs.getPos(2),height*buttonHeightMultiplier, buttonWidth, buttonHeight);

        repeat_button = new Buttons(_game, "repeat_button",aBs.getPos(5),height*buttonHeightMultiplier, buttonWidth, buttonHeight);
        soundsoff_button = new Buttons(_game,"sound_button_off",aBs.getPos(6),height*buttonHeightMultiplier, buttonWidth, buttonHeight);
        soundson_button = new Buttons(_game, "sound_button_on",aBs.getPos(6),height*buttonHeightMultiplier, buttonWidth, buttonHeight);

        pause_button = new Buttons(_game, "pause_button",aBs.getPos(1),height*0.89f, buttonWidth, buttonHeight);

        save_button = new Buttons(_game, "save_button",aBs.getPos(6),height*buttonHeightMultiplier, buttonWidth, buttonHeight);

        str = new CharSequence[5];

        draw_timeOut_text = false;
        draw_name_text = true;
        levelCompletedButton.visible = false;

        hideSoundButton();
        showBackButton(false);
        showRepeatButton(false);
        showPlayButton(false);
        showPlayNextButton(false);
        showPauseButton(true);

        save_button.visible = false;

    }

    private void show_controls(float screenWidth, float screenHeight) {

        float screenHeightIN = Gdx.graphics.getHeight() / Gdx.graphics.getPpiY();
        float virtualGuiToInch = screenHeight/screenHeightIN; //number of virtual dots per inch

        float maxHeight = Math.min(screenHeight/2f,virtualGuiToInch*2.1f); //no more than two inches or 50%
        float minHeight = virtualGuiToInch*1.3f; // no less than about 1 inch

        float preferredHeight = screenHeight/2;
        if (preferredHeight > maxHeight) preferredHeight = maxHeight;
        if (preferredHeight < minHeight) preferredHeight = minHeight;

        float tmpHeightCalc =  screenHeight*0.6f - preferredHeight;
        if (tmpHeightCalc > virtualGuiToInch/3)
            tmpHeightCalc = virtualGuiToInch/3;
        //adjust position according to size
        _controls = new Controls(_game, screenWidth*0.95f - preferredHeight, tmpHeightCalc);// screenHeight/4f - preferredHeight/2//preferredHeight/2 + screenHeight*0.05f);//screenWidth*0.83f, screenHeight*0.22f);
        _controls.resetSize(preferredHeight,preferredHeight);



        //Create a touchpad skin

        Skin touchpadSkin = new Skin();
        touchpadSkin.add("touchBackground", new Texture("data/skins/touchBackground.png"));
        touchpadSkin.add("touchKnob", new Texture("data/skins/touchKnob.png"));

//        Touchpad.TouchpadStyle touchpadStyle = new Touchpad.TouchpadStyle();
//        Drawable touchBackground = touchpadSkin.getDrawable("touchBackground");
//        Drawable touchKnob = touchpadSkin.getDrawable("touchKnob");
//        touchpadStyle.background = touchBackground;
//        touchpadStyle.knob = touchKnob;
//        touchpad = new Touchpad(20, touchpadStyle);
//        touchpad.setBounds(0, 0, preferredHeight,preferredHeight);
//        touchpad.setX(stage.getWidth() - touchpad.getWidth() - stage.getWidth()*0.1f);
//        touchpad.setY(stage.getHeight()*0.1f);
//        stage.addActor(touchpad);


        _controls.visible = true;

    }

    private void showGame() {

        cameraGame = new OrthographicCamera(TileSprite.VIEWPORT_WIDTH, TileSprite.VIEWPORT_HEIGHT);
        cameraGame.position.set(0, 0, 0);
        cameraGame.setToOrtho(true);
        cameraGame.update();
        CameraHelper.getInstance().applyTo(cameraGame);

        if (_game.gameData._elements.size() != 0) {
            _game.gameData._elements.clear();
            _game.gameData._creatureMoveOrderList.clear();

            if (_level != null)
                _level.reset();
        }

        TileSprite _sprite;

        // create the bedrock
        _game.gameData._floors.clear();
        for (int x1 = -4; x1 <= GameData.totaltilesX + 4; x1++) {
            for (int y1 = -4; y1 <= GameData.totaltilesY + 4; y1++) {
                if (x1==0 || y1==0 || x1 ==GameData.totaltilesX+1 || y1 == GameData.totaltilesY+1 )
                    _sprite = new Wall(_game, x1, y1, Type.WALL, GameData.BASE_LAYER, GameData.NONE);
                else
                    _sprite = new Floor(_game, x1, y1, Type.FLOOR, GameData.BASE_LAYER, GameData.NONE);
                _game.gameData._floors.add(_sprite);
            }
        }

        String filename = "";
        if (GameData.dataSetLocation == InputStreamFileLevelReader.INTERNAL) {
            filename = _game.gameData.gamefolder + _game.gameData.lastDataSet + ".dat";
        } else {
            filename = _game.gameData.dataFolder + "games/" + _game.gameData.lastDataSet + ".dat";
        }

        try {
            mslf = new MicrosoftLevelFactory(
                    InputStreamFileLevelReader.create(filename));
            _game.gameData.gameMode = DirectedGame.GAME_STATE_NEWLEVEL;

        } catch (Exception e) {
            mslf = null;
            toWorldMenu();
            return;
        }

        _game.gameData.gameMode = DirectedGame.GAME_STATE_NEWLEVEL;

    }

    @Override
    public void hide() {
        dispose();
    }

    @Override
    public void render(float dt) {

        update(dt);
        CameraHelper.getInstance().setTarget(_game.gameData._player);
        CameraHelper.getInstance().update(dt);
        CameraHelper.getInstance().applyTo(cameraGame);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT | GL20.GL_DEPTH_BUFFER_BIT);
        Gdx.gl.glClearColor(0.4f, 0.4f, 0.4f, 1f);
        renderGame();
        renderHUD();
        renderGUI();
    }

    public void renderHUD() {
        hudStage.draw();
        spriteBatch.setProjectionMatrix(cameraHUD.combined);
        spriteBatch.begin();
        _game.gameData._inventory.draw(spriteBatch);
        _level.draw(spriteBatch);
        _game.gameData._timebar.draw(spriteBatch);
        _chipsleft.draw(spriteBatch);
        spriteBatch.end();
    }

    public void renderGUI() {

        guiStage.draw();
        spriteBatch.setProjectionMatrix(cameraGUI.combined);
        spriteBatch.begin();

        _controls.draw(spriteBatch);

        pause_button.draw(spriteBatch);
        if (back_button.visible || levelCompletedButton.visible) //==PAUSE
            interruptionBackground.draw(spriteBatch);
        back_button.draw(spriteBatch);
        play_rommy_button.draw(spriteBatch);
        play_lynx_button.draw(spriteBatch);
        play_ms_button.draw(spriteBatch);
        repeat_button.draw(spriteBatch);
        soundsoff_button.draw(spriteBatch);
        soundson_button.draw(spriteBatch);
        playnext_button.draw(spriteBatch);
        save_button.draw(spriteBatch);
        levelCompletedButton.draw(spriteBatch);

        if (draw_name_text == true) {
            _game.gameData.font4.setColor(0.1f, 0.1f, 0.1f, 1.0f);
            _game.gameData.font4.draw(spriteBatch, str[0] + "\n" + str[1], GameData.tileWidth, _game.screenHeight - GameData.tileWidth*3f, GameData.tileWidth * 8f, Align.center,true);//GameData.tileHeight * 7, GameData.tileWidth * 9, Align.center,true);
        }

        if (levelCompletedButton.visible) {
            _game.gameData.font1.setColor(0.1f, 0.1f, 0.1f, 1.0f);
            _game.gameData.font1.draw(spriteBatch, str[2], levelCompletedButton.left()+2, levelCompletedButton.top(), levelCompletedButton.getWidth() - 5, Align.center, true);
        }

        _game.gameData._player.draw_death_text(spriteBatch);

        if (draw_timeOut_text == true) {
            _game.gameData.font3.setColor(0.1f, 0.1f, 0.1f, 1.0f);
            _game.gameData.font3.draw(spriteBatch, str[4], GameData.tileWidth,  _game.screenHeight - GameData.tileWidth*2f, GameData.tileWidth * 8f, Align.center, true);
        }

        if (_game.gameData.hint != null)
            _game.gameData.hint.drawText(spriteBatch);

        spriteBatch.end();

    }

    public void renderGame() {
        TileSprite element;
        int len = _game.gameData._elements.size();

        spriteBatch.setProjectionMatrix(cameraGame.combined);
        spriteBatch.begin();

        //BASE_LAYER
        int len_floor = _game.gameData._floors.size();
        for (int i = 0; i < len_floor; i++) {
            _game.gameData._floors.get(i).draw(spriteBatch);
        }

        for (int i = len - 1; i >= 0; i--) {
            element = DirectionStack.getElement(_game,i);
            if (element._layer != GameData.LOWER_LAYER)
                continue;
            if (!element.active)
                continue;
            element.draw(spriteBatch);
        }
        for (int i = len - 1; i >= 0; i--) {
            element = DirectionStack.getElement(_game,i);
            if (element._layer != GameData.UPPER_LAYER)
                continue;
            if (!element.active)
                continue;
            element.draw(spriteBatch);
        }
        for (int i = len - 1; i >= 0; i--) {
            element = DirectionStack.getElement(_game,i);
            if (element._layer != GameData.VISITOR_LAYER)
                continue;
            if (!element.active)
                continue;
            element.draw(spriteBatch);
        }
//        for (int i = len - 1; i >= 0; i--) {
//            element = _game.gameData._elements.get(i);
//            if (element._layer != GameData.VISITOR_UPPER_LAYER)
//                continue;
//            if (!element.active)
//                continue;
//            element.draw(spriteBatch);
//        }
        spriteBatch.end();
    }

    public void update(float dt) {

        if (_game.gameData.gameMode == DirectedGame.GAME_STATE_NEWLEVEL) {
            newLevelUpdate();
        }

        if (!handleGameControlInput(dt)) return;
        //handleGameControlInput(dt);

        if(_game.gameData.gsEvent == GameData.GameScreenEventMessage.TIMEOUTMESSAGE) {
            showTimeOut(true);
            hideHintText();
            _game.gameData.gsEvent = GameData.GameScreenEventMessage.NORMAL;
        }
        if(_game.gameData.gsEvent == GameData.GameScreenEventMessage.HINTMESSAGE) {
            showTimeOut(false);
            _game.gameData.gsEvent = GameData.GameScreenEventMessage.NORMAL;
        }

        if(_game.gameData.gsEvent == GameData.GameScreenEventMessage.PLAYERDIED) {
            playerDied();
            hideHintText();
            showTimeOut(false);
           _game.gameData.gsEvent = GameData.GameScreenEventMessage.NORMAL;
        }

        if(_game.gameData.gsEvent == GameData.GameScreenEventMessage.LEVELDONE) {
            levelcomplete();
            _game.gameData.gsEvent = GameData.GameScreenEventMessage.NORMAL;
            hideHintText();
            showTimeOut(false);
        }

        if (_game.gameData._inventory != null)
            _game.gameData._inventory.update(dt);

        if (_game.gameData._timebar != null)
            _game.gameData._timebar.update(dt);

        if (_gamelevel != null) {
            level_timer += dt;
            if (level_timer >= frame_time) {
                handlePlayerInput ();
                _gamelevel.update(frame_time);
                level_timer = level_timer - frame_time;
            }
        }

        if (timeOut_text_timer > 0) {
            timeOut_text_timer -= dt;
        } else {
            draw_timeOut_text = false;
            timeOut_text_timer =0;
        }
        sounds.update(dt);
    }

    private void newLevelUpdate() {
        _game.gameData.needToSave = true;
        //interruptionBackground.visible = false;

        hideSoundButton();
        showBackButton(false);
        showRepeatButton(false);
        showPlayNextButton(false);

        _game.gameData._elements.clear();
        _game.gameData._creatureMoveOrderList.clear();

        try {
            if (_game.gameData.level > _game.gameData.lastLevel) {
                _game.gameData.lastLevel = _game.gameData.level;
            }
            _level.currentLevel = _game.gameData.level;
            _game.gameData.levelhint = "";
            _game.gameData.levelname = "";
            _gamelevel = mslf.getLevel(_game, _game.gameData.level);
            str[0] = _game.gameData.levelname;
            str[1] = _game.gameData.levelPassword;
        } catch (Exception e) {
            //_game.setOldscreen("ScrolledLevelChoiceScreen");
            toLevelMenu();
            return;
        }

        _game.gameData._inventory.reset();
        _level.reset();
        _game.gameData._timebar.reset();
        _chipsleft.reset();

        // connects remaining buttons and unjoined tiles etc.
        _gamelevel.connectItems();
        _game.gameData._player.dead = false; //connected now
        _game.gameData._player.completed_level = false;

        _game.gameData.gameMode = DirectedGame.GAME_STATE_PLAY;
        return;
    }
    private void toLevelMenu() {
//        Gdx.input.setInputProcessor(null);
        _game.setScreen(new ScrolledLevelChoiceScreen(_game)); //set position to return to correct place
    }
    private void toWorldMenu() {
//        Gdx.input.setInputProcessor(null);
        _game.setScreen(new ScrolledWorldChoiceScreen(_game));
    }
    private void toOptionsMenu() {
//        Gdx.input.setInputProcessor(null);
        _game.setScreen(new OptionsScreen(_game));
    }
    private boolean handleGameControlInput(float dt) {
        // check for input
        if(Gdx.input.isKeyJustPressed(Input.Keys.BACK) || Gdx.input.isKeyJustPressed(Input.Keys.ESCAPE)){
            show_pause(true);
            return false;
        }

        if (Gdx.input.justTouched() || Gdx.input.isKeyPressed(Input.Keys.ANY_KEY) ) {
            cameraGUI.unproject(_touchPoint.set(Gdx.input.getX(), Gdx.input.getY(), 0));

            //End of Level Results Button touched
            if (levelCompletedButton.visible == true) {
                levelCompletedButton.visible = false;
                newLevel();
                return false;
            }

            //BACK BUTTON TOUCHED
            if(back_button.contains(_touchPoint)) {
                toLevelMenu();
                return false;
            }

            //SOUND BUTTON TOUCHED
            if (soundson_button.contains(_touchPoint)) {
                soundToggleButtonOn(false);
                Sounds.soundOff();
                return false;
            } else {
                if (soundsoff_button.contains(_touchPoint)) {
                    soundToggleButtonOn(true);
                    Sounds.soundOn();
                    return false;
                }
            }

            //PLAY BUTTON TOUCHED
            if (play_rommy_button.contains(_touchPoint) || (play_rommy_button.isVisible() && (Gdx.input.isKeyPressed(Input.Keys.P) || Gdx.input.isKeyPressed(Input.Keys.BUTTON_START))) ) {
                playStylePrefs('r');
                show_pause(false);
                return false;
            }
            //PLAY BUTTON TOUCHED
            if (play_ms_button.contains(_touchPoint)) //|| (play_rommy_button.isVisible() && (Gdx.input.isKeyPressed(Input.Keys.P) || Gdx.input.isKeyPressed(Input.Keys.BUTTON_START))) )
            {
                playStylePrefs('m');
                show_pause(false);
                return false;
            }
            //PLAY BUTTON TOUCHED
            if (play_lynx_button.contains(_touchPoint) ) //|| (play_lynx_button.isVisible() && (Gdx.input.isKeyPressed(Input.Keys.P) || Gdx.input.isKeyPressed(Input.Keys.BUTTON_START))) )
            {
                playStylePrefs('l');
                show_pause(false);
                return false;
            }

            //PLAY NEXT TOUCHED
            if (playnext_button.contains(_touchPoint) || (playnext_button.isVisible()  && (Gdx.input.isKeyPressed(Input.Keys.P) || Gdx.input.isKeyPressed(Input.Keys.BUTTON_START))) ) {
                //interruptionBackground.visible = false;

                hideSoundButton();
                showBackButton(false);
                showRepeatButton(false);
                showPlayButton(false);
                showPlayNextButton(false);
                showPauseButton(true);

                newLevel();
                return false;
            }


            //restart level
            if (repeat_button.contains(_touchPoint) || (repeat_button.isVisible() && Gdx.input.isKeyPressed(Input.Keys.R))) {
                _game.gameData._player.hideText();
                //interruptionBackground.visible = false;

                hideSoundButton();
                showBackButton(false);
                showRepeatButton(false);
                showPlayButton(false);
                showPauseButton(true);

                // only needed if we want to try again
                _game.gameData.lives++;
                _game.gameData.gameMode = DirectedGame.GAME_STATE_NEWLEVEL;
                return false;
            }
        }

        if (Gdx.input.justTouched() || Gdx.input.isKeyPressed(Input.Keys.ANY_KEY) ) {
            _game.gameData._player.hadFirstMove = true; //here?
            draw_name_text = false;
            _game.gameData._player.hideText();
            if (timeOut_text_timer <= 0)
                draw_timeOut_text = false;
        }

        if (_game.gameData.gameMode == DirectedGame.GAME_STATE_PLAY) {
            pause_button.visible = true;
            hideSoundButton();

            //PAUSE button
            if (pause_button.contains(_touchPoint)) {
                show_pause(true);
                //or here // draw_name_text = true;
                return false;
            }

        }
        return true;
    }
    public void playStylePrefs(char type) {
        Preferences.loadSettings(_game.gameData);
        switch (type) {
            case 'm':
                _game.gameData.enableTransparencyGlitch = true; //glitchCheckBox.isChecked();
                _game.gameData.msStyleMovement = true; //msMoveCheckBox.isChecked();
                _game.gameData.msStyleMoveConditions = true; //msConditionsCheckBox.isChecked();
                _game.gameData.lynxStyleMoveConditions = false; //lynxConditionsCheckBox.isChecked();
                _game.gameData.rommysStyleMoveConditions = false; //rommysConditionsCheckBox.isChecked();
                break;
            case 'l':
                _game.gameData.enableTransparencyGlitch = false; //glitchCheckBox.isChecked();
                _game.gameData.msStyleMovement = false; //msMoveCheckBox.isChecked();
                _game.gameData.msStyleMoveConditions = false; //msConditionsCheckBox.isChecked();
                _game.gameData.lynxStyleMoveConditions = true; //lynxConditionsCheckBox.isChecked();
                _game.gameData.rommysStyleMoveConditions = false; //rommysConditionsCheckBox.isChecked();
                break;
            default:
                _game.gameData.enableTransparencyGlitch = true; //glitchCheckBox.isChecked();
                //_game.gameData.msStyleMovement = true; //msMoveCheckBox.isChecked();
                _game.gameData.msStyleMoveConditions = false; //msConditionsCheckBox.isChecked();
                _game.gameData.lynxStyleMoveConditions = false; //lynxConditionsCheckBox.isChecked();
                _game.gameData.rommysStyleMoveConditions = true; //rommysConditionsCheckBox.isChecked();
        }
//        Preferences.saveSettings(_game.gameData);
    }

    public void handlePlayerInput () {
        if (_game.gameData._player.hadFirstMove) {
            if(!Gdx.input.isKeyPressed(Input.Keys.ANY_KEY) && !Gdx.input.isTouched()) {
                if (_game.gameData.touchAndHold) {
                    _game.gameData._player.clearMove();
                }
            }
            if(_controls.getDirection(_touchPoint) == GameData.NONE || _controls.getDirection(_touchPoint) != _game.gameData._player.queryNextMove() ) {
                _game.gameData._player.clearMove();
            }
        }

        boolean checkControllers = false;
        if (!_game.gameData.touchAndHold) {
            checkControllers = Gdx.input.justTouched();
        }
        else {
            checkControllers = Gdx.input.isTouched();
        }

        if (checkControllers) {
            cameraGUI.unproject(_touchPoint.set(Gdx.input.getX(), Gdx.input.getY(), 0));
            // CONTROLS
            if (_game.gameData.gameMode == DirectedGame.GAME_STATE_PLAY) {

                if (_controls.contains(_touchPoint))
                {

                    if (_controls.visible) {
                        if (_controls.contains(_touchPoint))
                            _game.gameData._player.moveDir(_controls.getDirection(_touchPoint));
                    }

                }
            }
        }
        if (Gdx.input.isKeyPressed(Input.Keys.Y)|| Gdx.input.isKeyPressed(Input.Keys.BUTTON_Y)) {
            if (!_game.gameData.msStyleMoveConditions)
                _game.gameData.slapOnly = true;
        }
        if (Gdx.input.isKeyJustPressed(Input.Keys.Z)|| Gdx.input.isKeyJustPressed(Input.Keys.BUTTON_Z)) {
            CameraHelper.getInstance().addZoom(0.5f);
            if (CameraHelper.getInstance().getZoom() >= 3f) CameraHelper.getInstance().setZoom(1f);
        }
//        if (Gdx.input.isKeyPressed(Input.Keys.X)|| Gdx.input.isKeyPressed(Input.Keys.BUTTON_X)) {
//        }

        if (_game.gameData.touchAndHold) {
            if (Gdx.input.isKeyPressed(Input.Keys.A) || Gdx.input.isKeyPressed(Input.Keys.LEFT) || Gdx.input.isKeyPressed(Input.Keys.DPAD_LEFT))
                _game.gameData._player.moveDir(GameData.WEST);
            if (Gdx.input.isKeyPressed(Input.Keys.D) || Gdx.input.isKeyPressed(Input.Keys.RIGHT) || Gdx.input.isKeyPressed(Input.Keys.DPAD_RIGHT))
                _game.gameData._player.moveDir(GameData.EAST);
            if (Gdx.input.isKeyPressed(Input.Keys.W) || Gdx.input.isKeyPressed(Input.Keys.UP) || Gdx.input.isKeyPressed(Input.Keys.DPAD_UP))
                _game.gameData._player.moveDir(GameData.NORTH);
            if (Gdx.input.isKeyPressed(Input.Keys.S) || Gdx.input.isKeyPressed(Input.Keys.DOWN) || Gdx.input.isKeyPressed(Input.Keys.DPAD_DOWN))
                _game.gameData._player.moveDir(GameData.SOUTH);
        } else {
            if (Gdx.input.isKeyJustPressed(Input.Keys.A) || Gdx.input.isKeyJustPressed(Input.Keys.LEFT) || Gdx.input.isKeyJustPressed(Input.Keys.DPAD_LEFT))
                _game.gameData._player.moveDir(GameData.WEST);
            if (Gdx.input.isKeyJustPressed(Input.Keys.D) ||Gdx.input.isKeyJustPressed(Input.Keys.RIGHT) || Gdx.input.isKeyJustPressed(Input.Keys.DPAD_RIGHT))
                _game.gameData._player.moveDir(GameData.EAST);
            if (Gdx.input.isKeyJustPressed(Input.Keys.W) ||Gdx.input.isKeyJustPressed(Input.Keys.UP) || Gdx.input.isKeyJustPressed(Input.Keys.DPAD_UP))
                _game.gameData._player.moveDir(GameData.NORTH);
            if (Gdx.input.isKeyJustPressed(Input.Keys.S) ||Gdx.input.isKeyJustPressed(Input.Keys.DOWN) || Gdx.input.isKeyJustPressed(Input.Keys.DPAD_DOWN))
                _game.gameData._player.moveDir(GameData.SOUTH);
        }

    }


    public void show_pause(boolean tf) {
        if (tf) {
            levelCompletedButton.visible = false;
            _game.gameData.gameMode = DirectedGame.GAME_STATE_PAUSE;
            _game.gameData._player.hadFirstMove = false;
            draw_name_text = true;
            //interruptionBackground.visible = true;
            save_button.visible = false;
            showBackButton(true);
            showRepeatButton(true);
            if (_game.gameData._player.dead == true || _game.gameData._player.completed_level == true) {
                showPlayButton(false);
                showPlayNextButton(true);
            } else {
                showPlayButton(true);
                showPlayNextButton(false);
            }
            showPauseButton(false);
            soundToggleButtonOn(Sounds.SoundOn);
        } else {
            draw_timeOut_text = false;
            hideHintText();
            _game.gameData._player.hideText();
            //interruptionBackground.visible = false;
            hideSoundButton();
            showBackButton(false);
            showRepeatButton(false);
            showPlayButton(false);
            showPlayNextButton(false);
            showPauseButton(true); //crash
            _game.gameData.gameMode = DirectedGame.GAME_STATE_PLAY;
            _game.gameData.needToSave = true;
        }
    }

    @Override
    public void resize(int width, int height) {
//        cameraBG.viewportHeight = bgTextureHeight;
//        cameraBG.viewportWidth = bgTextureWidth;
//        cameraBG.position.set(cameraBG.viewportWidth / 2,cameraBG.viewportHeight / 2, 0);
//        cameraBG.update();
        guiStage.act();
        hudStage.act();

        cameraGame.viewportHeight = TileSprite.VIEWPORT_HEIGHT;
        cameraGame.viewportWidth = (TileSprite.VIEWPORT_HEIGHT / (float)height) * (float)width;
        cameraGame.position.set(cameraGame.viewportWidth / 2, cameraGame.viewportHeight / 2, 0);
        cameraGame.update();

        cameraHUD.viewportHeight = _game.screenHUDHeight;
        cameraHUD.viewportWidth = _game.screenHUDWidth; //(_game.screenHUDHeight / (float)height) * (float)width;
        cameraHUD.position.set(cameraHUD.viewportWidth / 2, cameraHUD.viewportHeight / 2, 0);
        cameraHUD.update();

        cameraGUI.viewportHeight = _game.screenHeight;
        cameraGUI.viewportWidth = _game.screenWidth;
        cameraGUI.position.set(_game.screenWidth / 2, _game.screenHeight / 2, 0);
        cameraGUI.update();
    }


	public void pause() {
        Preferences.saveSettings(_game.gameData);
//        _game.gameData.gameMode = DirectedGame.GAME_STATE_PAUSE;
//		levelCompletedButton.visible = false;
//        hideSoundButton();
//        showBackButton(false);
//        showRepeatButton(false);
//        showPlayButton(false);
//        showPlayNextButton(false);
//        showPauseButton(false);
//		save_button.visible = false;
//		interruptionBackground.visible = true;
//		play_rommy_button.visible = true;

	};


	public void resume() {
        show_pause(true);
        if (_game.gameData._player.dead == true || _game.gameData._player.completed_level == true) {
            showPlayButton(false);
            showPlayNextButton(true);
        }
    };

	public void playerDied() {
		levelCompletedButton.visible = false;
		save_button.visible = false;
		//interruptionBackground.visible = true;

        showBackButton(true);
        showRepeatButton(true);
        showPlayButton(false);
        showPlayNextButton(true);
        showPauseButton(false);
		if (!Sounds.SoundOn) {
            soundToggleButtonOn(false);
		} else {
            soundToggleButtonOn(true);
		}

        //Here we get the message to display after the 'dying' animation
		_game.gameData.gameMode = DirectedGame.GAME_STATE_PAUSE;
		// Player.dead = false;
		_game.gameData._player.textVisible = true; //draw_death_text = true;
//		if (TimeBar._timeleft <= 0 && TimeBar._timeallocated > 0) {
//			str[3] = GameData.getLanguageString("warn time");
//		} else {
//		}
	}

    public void hideHintText() {
        if (_game.gameData.hint != null)
            _game.gameData.hint.hideText();
    }

    public void showTimeOut(boolean tf) {
        if (tf) {
            draw_timeOut_text = true;
            timeOut_text_timer = 5; // display for 5 sec
            //if (TimeBar._timeleft <= 0 && TimeBar._timeallocated > 0) {
            str[4] = GameData.getLanguageString("warn time");
            _game.gameData.lives = 5;
        } else {
            draw_timeOut_text = false;
            timeOut_text_timer = 0; // display for 5 sec
        }
    }

	@Override
	public void dispose() {
		hudStage.dispose();
        guiStage.dispose();
	}

//    @Override
//    public InputMultiplexer setInputProcessor(InputMultiplexer multiplexer) {
//        return null;
//    }


    private void soundToggleButtonOn(Boolean tf) {
        if (tf) {
            soundson_button.visible = true;
            soundsoff_button.visible = false;
        } else {
            soundsoff_button.visible = true;
            soundson_button.visible = false;
        }
    }
    private void hideSoundButton() {
        soundson_button.visible = false;
        soundsoff_button.visible = false;
    }
    private void showBackButton(Boolean tf) {
        back_button.visible = tf;
    }
    private void showRepeatButton(Boolean tf) {
        repeat_button.visible = tf;
    }
    private void showPlayButton(Boolean tf) {
        play_rommy_button.visible = tf;
        play_lynx_button.visible = tf;
        play_ms_button.visible = tf;
        //showPlayNextButton(!tf);
    }
    private void showPlayNextButton(Boolean tf) {
        playnext_button.visible = tf;
    }
    private void showPauseButton(Boolean tf) {
        pause_button.visible = tf;
    }

    public void levelcomplete() {
		int _timeleft = _game.gameData.timeleft;
        _game.gameData._player.completed_level = true;
		str[2] = "";
		if (_timeleft > 999)
			_timeleft = 999;
		boolean newtime = LevelScores.savelevelscores(_game.gameData,
				_game.gameData.level, _timeleft,
				_game.gameData.lives);

        Preferences.saveSettings(_game.gameData);

		levelCompletedButton.visible = true;

        hideSoundButton();
        showBackButton(false);
        showRepeatButton(false);
        showPlayButton(false);
        showPlayNextButton(false);
        showPauseButton(false);

		save_button.visible = false;

		_game.gameData.gameMode = DirectedGame.GAME_STATE_PAUSE;
		//interruptionBackground.visible = true;
		//levelCompletedButton.visible = true;
		time_bonus = LevelScores.get_time_bonus(_timeleft);

		level_bonus = LevelScores.get_level_bonus(_game.gameData.level,
				_game.gameData.lives);

		switch (_game.gameData.lives) {
		case 0:
			str[2] = GameData.getLanguageString("Best");
			break;
		case 1:
		case 2:
			str[2] = GameData.getLanguageString("Better");
			break;
		case 3:
		case 4:
			str[2] = GameData.getLanguageString("Good");
			break;
		default:
			str[2] = GameData.getLanguageString("Ok");
		}
		str[2] = Integer.toString(_game.gameData.level) + "\n" + str[2] + "\n"
				+ GameData.getLanguageString("Time bonus") + " " + time_bonus
				+ "\n" + GameData.getLanguageString("Level bonus") + " " + level_bonus + "\n"
				+ GameData.getLanguageString("Level score") + " "
                + (int) (time_bonus + level_bonus);

		if (newtime && _game.gameData.timeleft > 0) {
			str[2] = str[2] + "\n" + GameData.getLanguageString("Time record")
					+ "\n" + LevelScores.levelSaveMsg;
		} else {
			str[2] = str[2] + "\n" + LevelScores.levelSaveMsg;
		}
	}

	// start new level
	public void newLevel() {
		draw_name_text = true;
		levelCompletedButton.visible = false;
        _game.gameData._player.completed_level = false;

        hideSoundButton();
        showBackButton(false);
        showRepeatButton(false);
        showPlayButton(false);
        showPlayNextButton(false);
        showPauseButton(true);
		save_button.visible = false;

		_game.gameData.lives = 0;

		_game.gameData.level++;
		_game.gameData.gameMode = DirectedGame.GAME_STATE_NEWLEVEL;
        Preferences.saveSettings(_game.gameData);

	}

}
