package org.porteousclan.chipsretro.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.ScrollPane;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextField;
import com.badlogic.gdx.utils.ArrayMap;
import com.badlogic.gdx.utils.viewport.StretchViewport;

import org.porteousclan.chipsretro.DirectedGame;
import org.porteousclan.chipsretro.game.data.GameData;
import org.porteousclan.chipsretro.game.data.ImageCache;
import org.porteousclan.chipsretro.game.buttons.Buttons;
import org.porteousclan.chipsretro.game.buttons.Item;

/**
 * Created by richp on 2016-12-23.
 */

public class ScrollMenu extends AbstractScreen {

    protected SpriteBatch spriteBatch;
    //public OrthographicCamera cameraMenu;
    public OrthographicCamera cameraBG;
    public OrthographicCamera cameraMG;
    public OrthographicCamera cameraFG;
    public Stage stage=null;
    public Stage stage2;
    protected Vector3 _touchPoint;

    /**
     * Distance between items (in pixels)
     */
    protected float textureSizeY;
    protected float textureSizeX;

    Table container, buttonTable;
    protected static final float PAD = 10f;
    Buttons back_button;
    Buttons options_button;
    /**
     * All items available
     */
    protected ArrayMap<Integer, Item> items;
    ScrollPane scroll;
    Sprite spriteMG,spriteBG;
    private float bgTextureWidth, bgTextureHeight;
    private float mgTextureWidth, mgTextureHeight;
    protected ArrayMap<Integer, Item> itemsDisplayed;

    protected Table scrollTable;
    protected TextField textSearch;

    protected String searchLastCriteria;
    public Boolean verticalScroll = false;
    int itemsMaxPerLine;
    int linesTotal;

    InputMultiplexer multiplexer = new InputMultiplexer();

    public ScrollMenu(DirectedGame game) {
        super(game);

        //Gdx.input.setCatchMenuKey(game.gameData.catchMenuKey);
        Gdx.input.setCatchBackKey(game.gameData.catchBackKey);

    }

    @Override
    public void render(float dt) {
        update(dt);
        Gdx.gl.glClearColor(1, 1, 1, 1); //testing //Gdx.gl.glClearColor(254, 254, 254, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        //cameraBG.update();
        renderBG();
        //cameraMG.update();
        renderMG();
        stage.draw();
        //cameraFG.update();
        renderFG();
        if (Gdx.input.getInputProcessor() != multiplexer)
            Gdx.input.setInputProcessor(multiplexer);
    }

    public void update(float dt) {
        cameraFG.unproject(_touchPoint.set(Gdx.input.getX(), Gdx.input.getY(), 0));
        handleInput();
        stage.act(dt);
    }
    public void renderBG() {
        spriteBatch.setProjectionMatrix(cameraBG.combined);
        spriteBatch.begin();
        spriteBatch.disableBlending();
        spriteBG.draw(spriteBatch);
        spriteBatch.enableBlending();
        spriteBatch.end();
    }
    public void renderMG() {
        spriteBatch.setProjectionMatrix(cameraMG.combined);
        spriteBatch.begin();
        spriteMG.draw(spriteBatch);
        spriteBatch.end();
    }
    public void renderFG() {
        spriteBatch.setProjectionMatrix(cameraFG.combined);
        spriteBatch.begin();
        back_button.draw(spriteBatch);
        options_button.draw(spriteBatch);
        spriteBatch.end();
    }

    @Override
    public void resize(int width, int height) {

        cameraBG.viewportHeight = bgTextureHeight;
        cameraBG.viewportWidth = bgTextureWidth; //(_game.screenGUIHeight / (float)height) * (float)width;
        cameraBG.position.set(cameraBG.viewportWidth / 2,cameraBG.viewportHeight / 2, 0);//Gdx.graphics.getHeight() / 2,Gdx.graphics.getHeight() / 2, 0);
        cameraBG.update();

        cameraMG.viewportHeight = mgTextureHeight; //_game.screenGUIHeight;
        cameraMG.viewportWidth = (mgTextureHeight / (float)height) * (float)width;//(_game.screenGUIHeight / (float)height) * (float)width;
        cameraMG.position.set(mgTextureWidth / 2,cameraMG.viewportHeight / 2, 0);//Gdx.graphics.getHeight() / 2,Gdx.graphics.getHeight() / 2, 0);
        cameraMG.update();


        cameraFG.viewportHeight = _game.screenHeight;
        cameraFG.viewportWidth = _game.screenWidth; //(_game.screenWidth / (float)height) * (float)width;//
        cameraFG.position.set(_game.screenWidth / 2, _game.screenHeight / 2, 0);
        cameraFG.update();

        rearrangeTable(scrollTable, verticalScroll);
        scrollTable.pack();

    }

    @Override
    public void show() {
        multiplexer.clear();
        Gdx.input.setInputProcessor(null);
        _touchPoint = new Vector3();
        spriteBG = new Sprite(new Texture(Gdx.files.internal(GameData.skinsfolder + "splash_screen_prt2.png")));
        bgTextureWidth = spriteBG.getWidth();
        bgTextureHeight = spriteBG.getHeight();
        cameraBG = new OrthographicCamera(bgTextureWidth,bgTextureHeight); // _game.screenGUIWidth, _game.screenGUIHeight);//Gdx.graphics.getWidth(),Gdx.graphics.getHeight());
        cameraBG.position.set(0,0,0);//Gdx.graphics.getWidth()/2,Gdx.graphics.getHeight()/2, 0);
        cameraBG.update();

        spriteMG = new Sprite(new Texture(Gdx.files.internal(GameData.skinsfolder + "splash_screen_prt1.png")));
        mgTextureWidth = spriteMG.getWidth();
        mgTextureHeight = spriteMG.getHeight();

        cameraMG = new OrthographicCamera(mgTextureWidth,mgTextureHeight);
        cameraMG.position.set(0,0,0);
        cameraMG.update();

        cameraFG = new OrthographicCamera(_game.screenWidth, _game.screenHeight);
        cameraFG.position.set(0,0,0);
        cameraFG.update();

        stage = new Stage(new StretchViewport(_game.screenWidth,_game.screenHeight,cameraFG));
        spriteBatch = new SpriteBatch(); //(SpriteBatch) stage.getSpriteBatch(); //new SpriteBatch();

        items = new ArrayMap<Integer, Item>();
        itemsDisplayed = new ArrayMap<Integer, Item>();

        container = new Table();

        createItems(); // Creates all item list:
        textSearch = new TextField("", ImageCache.uiSkin);
        scrollTable = new Table(); // Table used to position all the items:
        //if (verticalScroll == null)
        rearrangeTable(scrollTable, verticalScroll);

        scroll = new ScrollPane(scrollTable, ImageCache.uiSkin);			// Prepares the scroll manager:
        scroll.validate();
        scroll.setScrollingDisabled(verticalScroll, !verticalScroll); 			// Only scroll horizontally:
        container.add(scroll).width(stage.getWidth()).height(stage.getHeight());
        container.pack();
        stage.addActor(container);

        stage2 = new Stage(new StretchViewport(_game.screenWidth,_game.screenHeight,cameraFG));
        multiplexer.addProcessor(stage);
        multiplexer.addProcessor(stage2);


        buttonTable = new Table();

        back_button = new Buttons(_game, "back_button", stage2.getWidth()*0.05f ,stage2.getHeight()*0.05f,  stage2.getWidth()/5, stage2.getHeight()/10);

        options_button = new Buttons(_game, "settings_button", stage2.getWidth()*0.75f,stage2.getHeight()*0.05f, stage2.getWidth()/5, stage2.getHeight()/10);
        back_button.visible = true; //!_game.gameData.catchBackKey;
        options_button.visible = true; //!_game.gameData.catchMenuKey;
        stage2.addActor(back_button);
        stage2.addActor(options_button);
        stage2.act();
        resumeScrollPosition();

    }

    protected final void rearrangeTable(Table scrollTable, boolean VerticalScroll) {


        computeDisplayedItems(textSearch.getText());

        //int maxSizeHeightItemIN = _game.gameData.magic_screen_height; //move as same in item
        float itemHeight;
        float itemWidth;
        if(items == null || items.size < 1) {
            //float height;
            float screenheightIN = Gdx.graphics.getHeight() / Gdx.graphics.getPpiY();
            float maxSizeHeightItemIN=1.8f;//GameData.magic_screen_height;
            if (screenheightIN > maxSizeHeightItemIN) {
                itemHeight = maxSizeHeightItemIN*Gdx.graphics.getPpiY();
            }
            else {
                itemHeight = _game.screenHeight * (screenheightIN/maxSizeHeightItemIN);
            }
            //*********//

            itemWidth = itemHeight * 0.7f;

        } else {
            itemHeight = items.firstValue().getHeight();
            itemWidth = items.firstValue().getWidth();

        }

        scrollTable.reset();//clear();
        scrollTable.defaults().uniform().maxHeight( itemHeight * 1.1f ).minSize(itemWidth,itemHeight);

        if (itemsDisplayed.size < 1) {
            linesTotal=1;
            itemsMaxPerLine = 1;
            return;
        }
        if (!VerticalScroll) {
//            float screenheightIN = (Gdx.graphics.getHeight() / Gdx.graphics.getPpiY())/maxSizeHeightItemIN ;
            float maxItems = _game.screenHeight/(itemHeight+PAD);
            linesTotal = (int) (maxItems - maxItems % 1);
            if (linesTotal > itemsDisplayed.size / 3 )
                linesTotal = (itemsDisplayed.size / 3) - ((itemsDisplayed.size / 3)%1) + 1;
//            if (((itemsDisplayed.size / 3)%1) > 0.0f)
//                linesTotal += 1;
            if (linesTotal < 1) linesTotal = 1;

            itemsMaxPerLine = (int) (itemsDisplayed.size / linesTotal);
            if (itemsDisplayed.size % linesTotal > 0)
                itemsMaxPerLine += 1; //left overs need a column
        } else {
            itemsMaxPerLine = 0; //fix later if needed
            if (itemsMaxPerLine < 1) itemsMaxPerLine = 1;
            linesTotal = (int) (itemsDisplayed.size / itemsMaxPerLine)-((itemsDisplayed.size / itemsMaxPerLine)%1);
            if (itemsDisplayed.size % itemsMaxPerLine > 0)
                linesTotal += 1; //left overs need a row

        }

        int itemsCount = 0;
        for (int i = 0; i < linesTotal; i++) { //STEP LINES
            itemsCount = i; //this equals the start number of each row
            while (itemsCount < itemsDisplayed.size) {
                scrollTable.add(itemsDisplayed.getValueAt(itemsCount))
                        .spaceBottom(PAD)            //Padding between images below this one
                        .spaceLeft(PAD)                //Padding between images to the left of this one
                        .spaceRight(PAD)            //Padding between images to the right if this one
                        .center();                    //Centers the image on the cell
                itemsCount += linesTotal; //STEP by NUMBER of LINES to give next in this line
            }
            if (i < linesTotal)
                scrollTable.row();
        }

    }


    private final void computeDisplayedItems(String searchCriteria) {
        if ((searchCriteria == null && searchLastCriteria == null) || searchCriteria.equals(searchLastCriteria)) {
            if (items.size != itemsDisplayed.size) {
                itemsDisplayed.clear();
                itemsDisplayed.putAll(items);
            }
            return;
        }

        itemsDisplayed.clear();

        if (searchCriteria == null || searchCriteria.trim().length() < 1) { //isEmpty()) { //requires sdk 9
            itemsDisplayed.putAll(items);
            return;
        }

        for (int i = 0; i < items.size; i++) {

            if (items.getValueAt(i).getDescription().getText().toString().contains(searchCriteria))
                itemsDisplayed.put(items.getKeyAt(i), items.getValueAt(i));
        }

        searchLastCriteria = searchCriteria;
    }

    public void resumeScrollPosition() {}
    public void saveScrollPosition() {}
    protected void createItems() {}
    public void handleInput() {}

    @Override
    public void hide() {
        dispose();
    }

    @Override
    public void pause() {
    }

    @Override
    public void resume() {
    }

    @Override
    public void dispose() {
        // Don't forget to free unmanaged objects:
        Gdx.input.setInputProcessor(null);
        if (stage != null)
            stage.dispose();
        if (stage2 != null)
            stage2.dispose();
        if (spriteBatch != null)
            spriteBatch.flush();
        //spriteBatch.dispose();

    }


}
