package org.porteousclan.chipsretro.screens;

import com.badlogic.gdx.Screen;
import org.porteousclan.chipsretro.DirectedGame;

/**
 * Created by richard on 02/08/16.
 */

public abstract class AbstractScreen implements Screen {

    protected static DirectedGame _game;
    public AbstractScreen (DirectedGame directedGame) {
        this._game = directedGame;
    }
    public abstract void show ();
    public abstract void render (float deltaTime);
    public abstract void resize (int width, int height);
    public abstract void pause ();
    public abstract void resume ();
    public abstract void hide ();
    public abstract void dispose (); //not called directly from game
}