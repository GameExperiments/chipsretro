package org.porteousclan.chipsretro.screens;

import org.porteousclan.chipsretro.DirectedGame;
import org.porteousclan.chipsretro.game.data.GameData;
import org.porteousclan.chipsretro.game.data.ImageCache;
import org.porteousclan.chipsretro.game.gameLevels.InputStreamFileLevelReader;
import org.porteousclan.chipsretro.game.gameLevels.MicrosoftLevelFactory;
import org.porteousclan.chipsretro.game.data.LevelScores;
import org.porteousclan.chipsretro.game.data.Preferences;
import org.porteousclan.chipsretro.game.buttons.Item;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.MathUtils;


public class ScrolledLevelChoiceScreen extends ScrollMenu {

	public ScrolledLevelChoiceScreen (DirectedGame game) {
		super(game);
	}

	@Override
	public void handleInput() {

		if(Gdx.input.isKeyJustPressed(Input.Keys.BACK) || Gdx.input.isKeyJustPressed(Input.Keys.ESCAPE)){
			_game.setScreen(new ScrolledWorldChoiceScreen(_game));
		}
		if(Gdx.input.isKeyJustPressed(Input.Keys.MENU)){
			_game.setScreen(new OptionsScreen(_game));
		}
		if (Gdx.input.justTouched()) {
			if (back_button.contains(_touchPoint)) {
				_game.setScreen(new ScrolledWorldChoiceScreen(_game));
			}
			if (options_button.contains(_touchPoint)) {
				_game.setScreen(new OptionsScreen(_game));
			}
		}
	}


    protected final void createItems() {
		int total;
		MicrosoftLevelFactory mslf = null;
		Item item =null;
		items.clear();
		itemsDisplayed.clear();
		
		String filename = "";
		if (GameData.dataSetLocation == InputStreamFileLevelReader.INTERNAL) {
			filename = _game.gameData.gamefolder + _game.gameData.lastDataSet + ".dat";
		} else {
			filename = _game.gameData.dataFolder+"games/" + _game.gameData.lastDataSet + ".dat";
		}
		
		try {
			mslf = new MicrosoftLevelFactory(InputStreamFileLevelReader.create(filename));
		} catch (Exception e) {
			mslf = null;
            _game.setScreen(new ScrolledWorldChoiceScreen(_game));
			return;
		}

		total = mslf.getLastLevelNumber();
		int x, oldscores[];
		String levelname;
		for (int i = 0; i < total; i++) {
			Color novaCor = new Color(MathUtils.random(0.5f), MathUtils.random(0.5f), MathUtils.random(0.5f), 1f);
			x=i+1;
			_game.gameData.lives = 0;
			String FILENAME = _game.gameData.lastDataSet + "_levels_" + x;
			oldscores = LevelScores.loadlevelscores(FILENAME);
			levelname = mslf.getLevelName(_game,x);

			item = new Item(_game, x, Item.LEVEL_TYPE, GameData.dataSetLocation, (x+"."+levelname), ImageCache.getTexture("level_select"), novaCor, ImageCache.uiSkin,_game.gameData.font1,_game.gameData.font1, this);

			item.setInfo(oldscores[0], oldscores[1], oldscores[2], LevelScores.levelReadMsg);
			items.put(i, item );
		}
		
		mslf = null;
		
	}
	@Override
	public void resumeScrollPosition() {
		scroll.layout();
		float newpos = Preferences.getLevelpos(_game.gameData.lastDataSet);
		if(newpos != 0) {
			scroll.setScrollX(newpos);
			scroll.updateVisualScroll();
		}
	}
	@Override
	public void saveScrollPosition() {
		scroll.layout();
		Preferences.saveLevelpos(_game.gameData.lastDataSet,scroll.getScrollX());
	}



}