package org.porteousclan.chipsretro.screens.common;

import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.scenes.scene2d.ui.ButtonGroup;
import com.badlogic.gdx.scenes.scene2d.ui.CheckBox;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import org.porteousclan.chipsretro.game.data.GameData;
import org.porteousclan.chipsretro.game.data.Sounds;
import org.porteousclan.chipsretro.util.GuiHelper;

import static org.porteousclan.chipsretro.util.GuiHelper.createCheckBox;

/**
 * Created by Richard on 2017-10-09.
 */

public class MoveStyleOptions {
    public GameData gameData;
    public Table optionsTable;
    public float fontscale;
    public float checkboxSize;
    public CheckBox glitchCheckBox;
    public CheckBox speedCheckBox;
    public CheckBox msMoveCheckBox;
    public ButtonGroup moveConditions;
    public CheckBox msConditionsCheckBox;
    public CheckBox lynxConditionsCheckBox;
    public CheckBox rommysConditionsCheckBox;

    public MoveStyleOptions(GameData gameData, Table optionsTable, float fontscale, float checkboxSize) {
        this.gameData = gameData;
        this.optionsTable = optionsTable;
        this.fontscale = fontscale;
        this.checkboxSize = checkboxSize;
        init();
    }

    private void init() {
        msMoveCheckBox = createCheckBox("", fontscale, checkboxSize, gameData.msStyleMovement);
        optionsTable.add( GameData.getLanguageString("msStyleMove") ).right().colspan(2);
        optionsTable.add( msMoveCheckBox ).colspan( 1 ).left();

        speedCheckBox = createCheckBox("", fontscale, checkboxSize, gameData.slowSpeed);
        optionsTable.add( GameData.getLanguageString("slowSpeed") ).right().colspan(2);
        optionsTable.add( speedCheckBox ).colspan( 1 ).left();

        glitchCheckBox = createCheckBox("", fontscale, checkboxSize, gameData.enableTransparencyGlitch);
        optionsTable.add( GameData.getLanguageString("transparencyGlitch") ).right().colspan(2);
        optionsTable.add( glitchCheckBox ).colspan( 1 ).left();

        optionsTable.row();
        moveConditions = new ButtonGroup();
        msConditionsCheckBox = createCheckBox("", fontscale, checkboxSize, gameData.msStyleMoveConditions);
        lynxConditionsCheckBox = createCheckBox("", fontscale, checkboxSize, gameData.lynxStyleMoveConditions);
        rommysConditionsCheckBox = createCheckBox("", fontscale, checkboxSize, gameData.rommysStyleMoveConditions);

        moveConditions.add(msConditionsCheckBox, lynxConditionsCheckBox, rommysConditionsCheckBox);
        moveConditions.setMaxCheckCount(1);
        moveConditions.setMinCheckCount(1);

        optionsTable.add( GameData.getLanguageString("msStyleCoditions") ).right().colspan(2);
        optionsTable.add( msConditionsCheckBox ).colspan( 1 ).left();
        optionsTable.add( GameData.getLanguageString("lynxStyleCoditions") ).right().colspan(2);
        optionsTable.add( lynxConditionsCheckBox ).colspan( 1 ).left();
        optionsTable.add( GameData.getLanguageString("rommyStyleCoditions") ).right().colspan(2);
        optionsTable.add( rommysConditionsCheckBox ).colspan( 1 ).left();

        optionsTable.row();

    }
    public void showOptions(Boolean tf) {
        optionsTable.setVisible(tf);
    }
    public void checkInput(Vector3 _touchPoint) {
        if (GuiHelper.contains(speedCheckBox, _touchPoint.x, _touchPoint.y)) {
            Sounds.play(Sounds.click, false);
            speedCheckBox.setChecked(!speedCheckBox.isChecked());
            gameData.slowSpeed = speedCheckBox.isChecked();
        }

        if (GuiHelper.contains(glitchCheckBox, _touchPoint.x, _touchPoint.y)) {
            Sounds.play(Sounds.click, false);
            glitchCheckBox.setChecked(!glitchCheckBox.isChecked());
            gameData.enableTransparencyGlitch = glitchCheckBox.isChecked();
            if (!rommysConditionsCheckBox.isChecked()) {
                msMoveCheckBox.setChecked(glitchCheckBox.isChecked());
                msConditionsCheckBox.setChecked(glitchCheckBox.isChecked());
                lynxConditionsCheckBox.setChecked(!glitchCheckBox.isChecked());
                gameData.msStyleMovement = msMoveCheckBox.isChecked();
                gameData.msStyleMoveConditions = msConditionsCheckBox.isChecked();
                gameData.lynxStyleMoveConditions = lynxConditionsCheckBox.isChecked();
                gameData.rommysStyleMoveConditions = rommysConditionsCheckBox.isChecked();
            }
        }

        if (GuiHelper.contains(msMoveCheckBox, _touchPoint.x, _touchPoint.y)) {
            Sounds.play(Sounds.click, false);
            msMoveCheckBox.setChecked(!msMoveCheckBox.isChecked());
            gameData.msStyleMovement = msMoveCheckBox.isChecked();
            if (!rommysConditionsCheckBox.isChecked()) {
                glitchCheckBox.setChecked(msMoveCheckBox.isChecked());
                msConditionsCheckBox.setChecked(msMoveCheckBox.isChecked());
                lynxConditionsCheckBox.setChecked(!msMoveCheckBox.isChecked());
                gameData.enableTransparencyGlitch = glitchCheckBox.isChecked();
                gameData.msStyleMoveConditions = msConditionsCheckBox.isChecked();
                gameData.lynxStyleMoveConditions = lynxConditionsCheckBox.isChecked();
                gameData.rommysStyleMoveConditions = rommysConditionsCheckBox.isChecked();

            }
        }

        if (GuiHelper.contains(msConditionsCheckBox, _touchPoint.x, _touchPoint.y)) {
            Sounds.play(Sounds.click, false);
            msConditionsCheckBox.setChecked(!msConditionsCheckBox.isChecked());
            msMoveCheckBox.setChecked(msConditionsCheckBox.isChecked());
            glitchCheckBox.setChecked(msMoveCheckBox.isChecked());
            gameData.enableTransparencyGlitch = glitchCheckBox.isChecked();
            gameData.msStyleMovement = msMoveCheckBox.isChecked();
            gameData.msStyleMoveConditions = msConditionsCheckBox.isChecked();
            gameData.lynxStyleMoveConditions = lynxConditionsCheckBox.isChecked();
            gameData.rommysStyleMoveConditions = rommysConditionsCheckBox.isChecked();
        }

        if (GuiHelper.contains(lynxConditionsCheckBox, _touchPoint.x, _touchPoint.y)) {
            Sounds.play(Sounds.click, false);
            lynxConditionsCheckBox.setChecked(!lynxConditionsCheckBox.isChecked());
            msMoveCheckBox.setChecked(!lynxConditionsCheckBox.isChecked());
            glitchCheckBox.setChecked(!lynxConditionsCheckBox.isChecked());
            gameData.enableTransparencyGlitch = glitchCheckBox.isChecked();
            gameData.msStyleMovement = msMoveCheckBox.isChecked();
            gameData.msStyleMoveConditions = msConditionsCheckBox.isChecked();
            gameData.lynxStyleMoveConditions = lynxConditionsCheckBox.isChecked();
            gameData.rommysStyleMoveConditions = rommysConditionsCheckBox.isChecked();
        }

        if (GuiHelper.contains(rommysConditionsCheckBox, _touchPoint.x, _touchPoint.y)) {
            Sounds.play(Sounds.click, false);
            rommysConditionsCheckBox.setChecked(!rommysConditionsCheckBox.isChecked());
            gameData.msStyleMoveConditions = msConditionsCheckBox.isChecked();
            gameData.lynxStyleMoveConditions = lynxConditionsCheckBox.isChecked();
            gameData.rommysStyleMoveConditions = rommysConditionsCheckBox.isChecked();
        }

    }
}
