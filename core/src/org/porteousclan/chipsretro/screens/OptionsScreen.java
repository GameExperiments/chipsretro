package org.porteousclan.chipsretro.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.ButtonGroup;
import com.badlogic.gdx.scenes.scene2d.ui.CheckBox;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Slider;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.utils.Scaling;
import com.badlogic.gdx.utils.viewport.StretchViewport;
import com.badlogic.gdx.utils.viewport.Viewport;

import org.porteousclan.chipsretro.DirectedGame;
import org.porteousclan.chipsretro.game.buttons.Buttons;
import org.porteousclan.chipsretro.game.data.GameData;
import org.porteousclan.chipsretro.game.data.ImageCache;
import org.porteousclan.chipsretro.game.data.Preferences;
import org.porteousclan.chipsretro.game.data.Sounds;
import org.porteousclan.chipsretro.screens.common.MoveStyleOptions;
import org.porteousclan.chipsretro.util.GuiHelper;
import org.porteousclan.chipsretro.util.ImportDataSets;

import java.util.Locale;

import static org.porteousclan.chipsretro.util.GuiHelper.createCheckBox;


public class OptionsScreen extends AbstractScreen  {
	public SpriteBatch spriteBatch;
	public OrthographicCamera cameraFG;
    public OrthographicCamera cameraBG;
    public OrthographicCamera cameraMG;
    Sprite spriteMG,spriteBG;
    private float bgTextureWidth, bgTextureHeight;
    private float mgTextureWidth, mgTextureHeight;

    public Viewport viewport;
	public Stage stage,stage2;
	Label volumeValue;
    Table optionsTable;

    private AbstractScreen screenToReturnTo;

    InputMultiplexer multiplexer = new InputMultiplexer();
    protected Vector3 _touchPoint;

    //private TextButton backButton;
    //private TextButton importButton;
    Buttons backButton;
    Buttons importButton;

    private Slider volumeSlider;
//    private ButtonGroup dpadChoice;
//    private CheckBox controlsCheckbox1;
//    private CheckBox controlsCheckbox2;
//    private CheckBox controlsCheckbox3;
    private CheckBox requireTapTapCheckbox;
//    private CheckBox touchPadCheckbox;
    private CheckBox soundCheckbox;
    private CheckBox soundEffectsCheckbox;
    private CheckBox catchBackKeyCheckbox;
    MoveStyleOptions systemOptions;
//    private ButtonGroup msChoice;
//    private CheckBox glitchCheckBox;
//    private CheckBox speedCheckBox;
//    private CheckBox msMoveCheckBox;
//    private ButtonGroup moveConditions;
//    private CheckBox msConditionsCheckBox;
//    private CheckBox lynxConditionsCheckBox;
//    private CheckBox rommysConditionsCheckBox;

	public OptionsScreen (DirectedGame game) {
		super(game);
        screenToReturnTo = new ScrolledWorldChoiceScreen(_game);
        Gdx.input.setCatchBackKey(game.gameData.catchBackKey);

    }

    @Override
    public void render(float dt) {
        update(dt);
        renderBG();
        renderMG();
        stage.draw();
        renderFG();
    }
    public void renderBG() {
        spriteBatch.setProjectionMatrix(cameraBG.combined);
        spriteBatch.begin();
        spriteBatch.disableBlending();
        spriteBG.draw(spriteBatch);
        spriteBatch.enableBlending();
        spriteBatch.end();
    }
    public void renderMG() {
        spriteBatch.setProjectionMatrix(cameraMG.combined);
        spriteBatch.begin();
//        spriteBatch.disableBlending(); //hides previously drawn
        spriteMG.draw(spriteBatch);
//        spriteBatch.enableBlending();
        spriteBatch.end();
    }
    public void renderFG() {
        spriteBatch.setProjectionMatrix(cameraFG.combined);
        spriteBatch.begin();
        backButton.draw(spriteBatch);
        importButton.draw(spriteBatch);
        spriteBatch.end();
    }
    public void update(float dt) {
        handleInput();
        stage.act(dt);
        spriteBatch.setProjectionMatrix(cameraFG.combined);
        Gdx.gl.glClearColor(1, 1, 1, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        cameraFG.update();

    }
    @Override
    public void resize(int width, int height) {
        stage.act();
        optionsTable.act(Gdx.graphics.getDeltaTime());
        optionsTable.pack();
        cameraBG.viewportHeight = bgTextureHeight;
        cameraBG.viewportWidth = bgTextureWidth; //(_game.screenGUIHeight / (float)height) * (float)width;
        cameraBG.position.set(cameraBG.viewportWidth / 2,cameraBG.viewportHeight / 2, 0);//Gdx.graphics.getHeight() / 2,Gdx.graphics.getHeight() / 2, 0);
        cameraBG.update();

        cameraMG.viewportHeight = mgTextureHeight; //_game.screenGUIHeight;
        cameraMG.viewportWidth = (mgTextureHeight / (float)height) * (float)width;//(_game.screenGUIHeight / (float)height) * (float)width;
        cameraMG.position.set(mgTextureWidth / 2,cameraMG.viewportHeight / 2, 0);//Gdx.graphics.getHeight() / 2,Gdx.graphics.getHeight() / 2, 0);
        cameraMG.update();

        cameraFG.viewportHeight = _game.screenHeight;
        cameraFG.viewportWidth = _game.screenWidth; //(_game.screenWidth / (float)height) * (float)width;
//        cameraFG.position.set(optionsTable.getWidth() / 2, optionsTable.getHeight() / 2, 0);
        cameraFG.position.set(cameraFG.viewportWidth / 2, cameraFG.viewportHeight / 2, 0);
        cameraFG.update();
        stage.getViewport().setWorldWidth(cameraFG.viewportWidth);
        stage2.getViewport().setWorldWidth(cameraFG.viewportWidth);
        optionsTable.pack();

        if (Gdx.input.getInputProcessor() != multiplexer)
            Gdx.input.setInputProcessor(multiplexer);
    }

    @Override
    public void show() {

        _touchPoint = new Vector3();
        spriteBatch = new SpriteBatch();
        spriteBG = new Sprite(new Texture(Gdx.files.internal(GameData.skinsfolder + "splash_screen_prt2.png")));
        bgTextureWidth = spriteBG.getWidth();
        bgTextureHeight = spriteBG.getHeight();
        cameraBG = new OrthographicCamera(bgTextureWidth,bgTextureHeight); // _game.screenGUIWidth, _game.screenGUIHeight);//Gdx.graphics.getWidth(),Gdx.graphics.getHeight());
        cameraBG.position.set(0,0,0);//Gdx.graphics.getWidth()/2,Gdx.graphics.getHeight()/2, 0);
        cameraBG.update();

        spriteMG = new Sprite(new Texture(Gdx.files.internal(GameData.skinsfolder + "splash_screen_prt1.png")));
        mgTextureWidth = spriteMG.getWidth();
        mgTextureHeight = spriteMG.getHeight();
        cameraMG = new OrthographicCamera(mgTextureWidth,mgTextureHeight); //new OrthographicCamera( _game.screenGUIWidth, _game.screenGUIHeight);//Gdx.graphics.getWidth(),Gdx.graphics.getHeight());
        cameraMG.position.set(0,0,0);//Gdx.graphics.getWidth()/2,Gdx.graphics.getHeight()/2, 0);
        cameraMG.update();

        cameraFG = new OrthographicCamera(_game.screenWidth, _game.screenHeight);
        cameraFG.position.set(0,0,0);
        cameraFG.update();

        stage = new Stage(new StretchViewport(_game.screenWidth,_game.screenHeight,cameraFG));
        stage2 = new Stage(new StretchViewport(_game.screenWidth,_game.screenHeight,cameraFG));
        multiplexer.addProcessor(stage);
        multiplexer.addProcessor(stage2);

        Preferences.loadSettings(_game.gameData);

        optionsTable = new Table(ImageCache.uiSkin);
        optionsTable.setFillParent(true);
        optionsTable.setWidth(_game.screenWidth);
        optionsTable.defaults().pad(10,10,5,10).expandX();

        float checkboxSize = _game.screenHeight/15f;
        float fontscale = 1f;

        soundCheckbox = createCheckBox("", fontscale, checkboxSize, Sounds.SoundOn);
        optionsTable.add( GameData.getLanguageString("SoundOnOff") ).right().colspan(2);
        optionsTable.add( soundCheckbox ).colspan( 1 ).left();

        soundEffectsCheckbox = createCheckBox("", fontscale, checkboxSize, Sounds.SoundEffectsOn);
        optionsTable.add( GameData.getLanguageString("Sound Effects") ).right().colspan(2);
        optionsTable.add( soundEffectsCheckbox ).colspan( 1 ).left();

//        optionsTable.row();
//
//        // range is [0.0,1.0]; step is 0.1f
//        volumeSlider = new Slider( 0f, 1f, 0.1f, false, ImageCache.uiSkin );
//
//        volumeSlider.setValue( Sounds.volume );
//        volumeValue = new Label( "", ImageCache.uiSkin );
//        updateVolumeLabel();
//
//        volumeSlider.addListener( new ChangeListener() {
//            @Override
//            public void changed(ChangeEvent event, Actor actor )
//            {
//                float value = volumeSlider.getValue();
//                Sounds.volume  = value;
//                //Sounds.musicvolume = value;
//                Sounds.effectsvolume  = value;
//                updateVolumeLabel();
//                //               Sounds.music1.setVolume(value);
//            }
//        } );
//        optionsTable.add( GameData.getLanguageString("Volume") ).right().colspan(2);
//        optionsTable.add( volumeSlider ).colspan( 1 ).left();

        optionsTable.row();

        requireTapTapCheckbox = createCheckBox("", fontscale, checkboxSize, _game.gameData.touchAndHold);
        optionsTable.add( GameData.getLanguageString("touchAndHold") ).right().colspan(2);
        optionsTable.add( requireTapTapCheckbox ).colspan( 1 ).left();

        catchBackKeyCheckbox = createCheckBox("", fontscale, checkboxSize, _game.gameData.catchBackKey);
        optionsTable.add( GameData.getLanguageString("catchBackKey") ).right().colspan(2);
        optionsTable.add( catchBackKeyCheckbox ).colspan( 1 ).left();

        optionsTable.row();

        systemOptions = new MoveStyleOptions(_game.gameData,optionsTable,fontscale, checkboxSize);
        optionsTable.pack();
//        optionsTable.debug();

        stage.addActor(optionsTable);

        stage.act();
        backButton = new Buttons(_game, "back_button", stage2.getWidth()*0.05f ,stage2.getHeight()*0.05f,  stage2.getWidth()/5, stage2.getHeight()/10);
        importButton = new Buttons(_game, "import", stage2.getWidth()*0.7f,stage2.getHeight()*0.05f, stage2.getWidth()/5, stage2.getHeight()/10);
        backButton.visible = true; //!_game.gameData.catchBackKey;
        importButton.visible = true; //!_game.gameData.catchMenuKey;
        stage2.addActor(backButton);
        stage2.addActor(importButton);
        stage2.act();

    }

//    private CheckBox createCheckBox(String label, float fontscale, float size, boolean checked) {
//        CheckBox checkbox = new CheckBox(label,ImageCache.uiSkin);
//        //checkbox.setSkin(ImageCache.uiSkin);
//        checkbox.getLabel().setFontScale(fontscale);
//        checkbox.getImageCell().width(size).height(size);
//        checkbox.getImage().setScaling(Scaling.stretch);
//        checkbox.setChecked( checked );
//        return checkbox;
//    }

//    private TextButton createTextButton(String label) {
//        TextButton textbutton = new TextButton(label, ImageCache.uiSkin);
//        //textbutton.getLabel().setFontScale(fontscale);
//        textbutton.defaults().fill();
//        textbutton.pad(2,3,2,3);
//        return textbutton;
//    }

    @Override
    public void hide() {
        dispose();
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void dispose() {
        Gdx.app.log("dispose", "Options Screen");
        //Gdx.input.setCatchBackKey(false);
        Gdx.input.setInputProcessor(null);
        stage.dispose();
        stage2.dispose();
        spriteBatch.flush();
    }


    public void handleInput() {
        cameraFG.unproject(_touchPoint.set(Gdx.input.getX(), Gdx.input.getY(), 0));

        if(Gdx.input.isKeyJustPressed(Input.Keys.BACK) || Gdx.input.isKeyJustPressed(Input.Keys.ESCAPE)){
            Preferences.saveSettings(_game.gameData);
            _game.setScreen(screenToReturnTo);
        }

        if (Gdx.input.justTouched()) {
            if (backButton.contains(_touchPoint)) {
//            if (contains(backButton, _touchPoint.x, _touchPoint.y)) {
                Preferences.saveSettings(_game.gameData);
                Gdx.app.log("backbutton","");
                Sounds.play( Sounds.click, false );
                _game.setScreen(screenToReturnTo);
            }
            if (importButton.contains(_touchPoint)) {
//            if (contains(importButton, _touchPoint.x, _touchPoint.y)) {
                Gdx.app.log("importbutton","");
                Sounds.play(Sounds.click, false);
                ImportDataSets.searchForDownLoadedDataSets(_game.gameData);
                _game.setScreen(new ScrolledWorldChoiceScreen(_game));
            }


            if (GuiHelper.contains(requireTapTapCheckbox, _touchPoint.x, _touchPoint.y)) {
                Sounds.play(Sounds.click, false);
                requireTapTapCheckbox.setChecked(!requireTapTapCheckbox.isChecked());
                _game.gameData.touchAndHold = requireTapTapCheckbox.isChecked();
            }

            systemOptions.checkInput(_touchPoint);

            if (GuiHelper.contains(soundCheckbox, _touchPoint.x, _touchPoint.y)) {
                Gdx.app.log("sound","");
                soundCheckbox.setChecked(!soundCheckbox.isChecked());
                boolean enabled = soundCheckbox.isChecked();
                if (enabled)
                    Sounds.soundOn();
                else
                    Sounds.soundOff();
                Sounds.play(Sounds.click, false);
            }

            if (GuiHelper.contains(soundEffectsCheckbox, _touchPoint.x, _touchPoint.y)) {
                Gdx.app.log("Effects","");
                soundEffectsCheckbox.setChecked(!soundEffectsCheckbox.isChecked());
                boolean enabled = soundEffectsCheckbox.isChecked();
                if (enabled)
                    Sounds.SoundEffectsOn = true;
                else
                    Sounds.SoundEffectsOn = false;
                Sounds.play(Sounds.click, false);
            }

            if (GuiHelper.contains(catchBackKeyCheckbox, _touchPoint.x, _touchPoint.y)) {
                Gdx.app.log("back key checkbox","");
                catchBackKeyCheckbox.setChecked(!catchBackKeyCheckbox.isChecked());
                _game.gameData.catchBackKey = catchBackKeyCheckbox.isChecked();
                Gdx.input.setCatchBackKey(_game.gameData.catchBackKey);
                Sounds.play(Sounds.click, false);
            }

//            if (contains(volumeSlider,_touchPoint.x, _touchPoint.y)) {
//                //                float value = ( (Slider) actor ).getValue();
////                Sounds.volume  = value;
////                //Sounds.musicvolume = value;
////                Sounds.effectsvolume  = value;
////                updateVolumeLabel();
//
//            }

            Preferences.saveSettings(_game.gameData);

        }
    }

//    private boolean contains(Actor test, float x, float y) {
//        Rectangle bb = new Rectangle(test.getX(), test.getY(), test.getWidth(), test.getHeight());
//        return bb.contains(x,y);
//    }
        /**
          * Updates the volume label next to the slider.
          */
    private void updateVolumeLabel()
    {
        float volume = ( Sounds.volume * 100 );
        volumeValue.setText( String.format( Locale.US, "%1.0f%%", volume ) );
    }

}
