package org.porteousclan.chipsretro.screens;

import org.porteousclan.chipsretro.DirectedGame;
import org.porteousclan.chipsretro.game.data.GameData;
import org.porteousclan.chipsretro.game.data.ImageCache;
import org.porteousclan.chipsretro.game.gameLevels.InputStreamFileLevelReader;
import org.porteousclan.chipsretro.game.gameLevels.MicrosoftLevelFactory;
import org.porteousclan.chipsretro.game.buttons.Item;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.MathUtils;

import java.util.ArrayList;
import static com.badlogic.gdx.Gdx.app;

public class ScrolledWorldChoiceScreen extends ScrollMenu {

	//File Stuff
	private ArrayList<FileHandle> files;

	public ScrolledWorldChoiceScreen (DirectedGame game) {
		super(game);
	}

	@Override
	public void handleInput() {

		if(Gdx.input.isKeyJustPressed(Input.Keys.BACK) || Gdx.input.isKeyJustPressed(Input.Keys.ESCAPE)){
			app.exit();
		}
		if(Gdx.input.isKeyJustPressed(Input.Keys.MENU)){
			_game.setScreen(new OptionsScreen(_game));
		}
		if (Gdx.input.justTouched()) {
			if (back_button.contains(_touchPoint)) {
				app.exit(); //set position to return to correct place
			}
			if (options_button.contains(_touchPoint)) {
				_game.setScreen(new OptionsScreen(_game)); //, this));
				//				_game.setScreen(new OptionsScreen(_game)); //set position to return to correct place
			}
		}
	}


    protected final void createItems() {
		int total,location;
		MicrosoftLevelFactory mslf = null;
		Item item =null;
		items.clear();
		itemsDisplayed.clear();
		
		String levelPackName = "";
		int i=0,x=0;
		Color novaCor;

		//change STARTED
        files = new ArrayList<FileHandle>();
        GameData.dataSetLocation = InputStreamFileLevelReader.INTERNAL;
		for (FileHandle file : Gdx.files.internal("data/games/").list(".dat")) {
            //Gdx.app.log("found", file.path());
            files.add(file);
        }
        if (Gdx.files.isExternalStorageAvailable()) {
            for (FileHandle file : Gdx.files.external("porteousclan/rommysgauntlet/games/").list(".dat")) {
                //Gdx.app.log("found", file.path());
                files.add(file);
            }
        }
		for(FileHandle file: files) {
		   // do something interesting here
            //Gdx.app.log("file ", file.path());
			if (file.path().startsWith("porteousclan/rommysgauntlet/games/")) {
				location = InputStreamFileLevelReader.EXTERNAL;
			} else {
				location = InputStreamFileLevelReader.INTERNAL;
			}
			try {
				mslf = new MicrosoftLevelFactory(InputStreamFileLevelReader.create(file));
			} catch (Exception e) {
				mslf = null;
				continue;
			}
			total = mslf.getLastLevelNumber();
			levelPackName = file.nameWithoutExtension();
			x=i+1;
			novaCor = new Color(MathUtils.random(0.5f), MathUtils.random(0.5f), MathUtils.random(0.5f), 1f);

            item = new Item(_game, x, Item.WORLD_TYPE, location, levelPackName, ImageCache.getTexture("level_select"), novaCor, ImageCache.uiSkin,_game.gameData.font2,_game.gameData.font1,this);
			item.setLevelname(levelPackName);
			item.setWorldInfo(_game.gameData.countDoneLevels(levelPackName), total);
			items.put(i, item );
			mslf = null;
			i++;
		}
		mslf = null;

		novaCor = new Color(MathUtils.random(0.5f), MathUtils.random(0.5f), MathUtils.random(0.5f), 1f);

		item = new Item(_game, x, Item.EXT_URL_TYPE, -1, _game.gameData.getLanguageString("MikeL"), ImageCache.getTexture("level_select"), novaCor, ImageCache.uiSkin,_game.gameData.font2,_game.gameData.font1, this);
		item.setLevelname("http://www.pillowpc2001.net/index.htm#cclp");
		items.put(i, item );
		i++;

	}



}