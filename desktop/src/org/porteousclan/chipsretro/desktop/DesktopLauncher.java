package org.porteousclan.chipsretro.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.badlogic.gdx.tools.texturepacker.TexturePacker;

import org.porteousclan.chipsretro.ChipsRetro;

public class DesktopLauncher {
	private static boolean rebuildAtlas = false;
	private static boolean drawDebugOutline = false;
	public static void main (String[] arg) {

		if (rebuildAtlas) {
			TexturePacker.Settings settings = new TexturePacker.Settings();
			settings.maxWidth = 512;
			settings.maxHeight = 512;
			settings.duplicatePadding = false;
			settings.debug = drawDebugOutline;

//			TexturePacker.process(settings, "../../assets-raw/rommysgauntlet/", "images", "rommysgauntlet" );
//			TexturePacker.process(settings, "../../assets-raw/rommysgauntlet-ui/", "images", "rommysgauntlet-ui" );
			TexturePacker.process(settings, "../../assets-raw/atileset/", "game/tiles", "atileset" );
//			TexturePacker.process(settings, "../../assets-raw/controls/", "images", "controls" );
//			TexturePacker.process(settings, "../../assets-raw/splash_screen/", "images", "splash_screen" );
		}

		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		config.height = 480; //320; // 960; //
		config.width = 800; //640; // 1280; //
		new LwjglApplication(new ChipsRetro(), config);
	}


}
